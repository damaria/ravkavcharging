import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent} from './pages/login/login.component';
import { ChangingComponent } from './pages/main/changing/changing.component';
import { DepositToBankComponent } from './pages/main/deposit-to-bank/deposit-to-bank.component';
import { DepositToChashboxComponent } from './pages/main/deposit-to-chashbox/deposit-to-chashbox.component';
import { HomeComponent } from './pages/main/home/home.component';
import { MainComponent } from './pages/main/main.component';
import { StocktakingComponent } from './pages/main/stocktaking/stocktaking.component';
import { MainResolverService } from './pages/main/main-resolver.service';
import { sessionKeys } from './shared/constants/sessionKey';
import { PayReportComponent } from './pages/main/pay-report/pay-report.component';
import { PayReportSuccessComponent } from './pages/main/pay-report/components/pay-report-success/pay-report-success.component';
import { PayReportFailedComponent } from './pages/main/pay-report/components/pay-report-failed/pay-report-failed.component';
import { PayReportCanceledComponent } from './pages/main/pay-report/components/pay-report-canceled/pay-report-canceled.component';
import { GeneralDepositFromCashboxComponent } from './pages/main/general-deposit-from-cashbox/general-deposit-from-cashbox.component';
import { GeneralDepositFromCashboxGuard } from './pages/main/general-deposit-from-cashbox/general-deposit-from-cashbox.guard';
import { AdminCashboxGuard } from './shared/guards/admin-cashbox.guard';
import { LoginGuard } from './shared/guards/login.guard';
import { DisloginGuard } from './shared/guards/dislogin.guard';

function getOperatorId() {
  const origin = window.location.origin;
  switch(true) {
    case origin.includes('192.168.157.81'): //eged-taabura
      return '4';
    case origin.includes('10.208.7.152') || origin.includes('gbtours.isrcorp.co.il'): //GB-tours
      return '8';
    case origin.includes('10.5.20.34') : //metropolin
      return '15';
    case origin.includes('192.168.159.5') || origin.includes('10.240.52.5'): //tnufa
      return '34';
    case origin.includes('192.168.160.151'): //extra
      return '37';
    case origin.includes('10.208.71.162:8080'): //clalit-golan
      return '24';
    case origin.includes('localhost'): //develop
      return sessionStorage.getItem(sessionKeys.operatorId) || '37';
    default:
      return '37' // test (random operator)
  }
}

const routes: Routes = [
  {
    path: '',
    redirectTo: `login/${getOperatorId()}`,
    pathMatch: 'full'
  },
  {
    path: 'login/:operatorId',
    component: LoginComponent,
    canActivate: [DisloginGuard],
    canLoad: [DisloginGuard]
  },
  {
    path: 'main',
    component: MainComponent,
    resolve: [MainResolverService],
    canActivate: [LoginGuard],
    canLoad: [LoginGuard],
    children: [
      // { path: 'dispalyRavKavDetails', component: DisplayDetailsOfRavKavComponent },
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'depositToCashbox',
        component: DepositToChashboxComponent,
        canActivate: [AdminCashboxGuard],
        canLoad: [AdminCashboxGuard]
      },
      {
        path: 'changing',
        component: ChangingComponent,
        canActivate: [AdminCashboxGuard],
        canLoad: [AdminCashboxGuard]
      },
      {
        path: 'stocktaking',
        component: StocktakingComponent,
        canActivate: [AdminCashboxGuard],
        canLoad: [AdminCashboxGuard]
      },
      {
        path: 'depositToBank',
        component: DepositToBankComponent,
        canActivate: [AdminCashboxGuard],
        canLoad: [AdminCashboxGuard]
      },
      {
        path: 'generalDepositFromCashbox',
        component: GeneralDepositFromCashboxComponent,
        canActivate: [AdminCashboxGuard, GeneralDepositFromCashboxGuard],
        canLoad: [AdminCashboxGuard, GeneralDepositFromCashboxGuard]
      },
      {
        path: 'payReport',
        component: PayReportComponent,
        canActivate: [AdminCashboxGuard],
        canLoad: [AdminCashboxGuard]
      }
    ]
  },
  {
    path: 'payment-success',
    component: PayReportSuccessComponent
  },
  {
    path: 'payment-failed',
    component: PayReportFailedComponent
  },
  {
    path: 'payment-canceled',
    component: PayReportCanceledComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy', useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
