import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { sessionKeys } from './shared/constants/sessionKey';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  translateForCurrentVersionKey = `translateFor${environment.versionNum}Version`;
  translateForCurrentVersionValue = sessionStorage.getItem(this.translateForCurrentVersionKey);

  ngOnInit(): void {
    this.updateTranslate();
  }

  updateTranslate() {
    this.deleteTranslateForLastVersionsFlags();

    if (this.translateForCurrentVersionValue == null) {
      this.deleteLastTranslate();
      this.addTranslateForCurrentVersionFlag();
    }
  }

  deleteTranslateForLastVersionsFlags() {
    const sessionStorageKeys = Object.keys(sessionStorage);
    sessionStorageKeys.forEach(key => {
      if (key.includes("translateFor") && key != this.translateForCurrentVersionKey) {
        sessionStorage.removeItem(key);
      }
    });
  }

  deleteLastTranslate() {
    sessionStorage.removeItem(sessionKeys.i18n);
  }

  addTranslateForCurrentVersionFlag() {
    sessionStorage.setItem(this.translateForCurrentVersionKey, 'Up to date');
  }
}


