import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AmountInputService, subData } from 'src/app/shared/components/amount-input/amount-input.service';
import { getValue, ServerResponse, statusCode } from 'src/app/shared/constants/server';
import { CashboxWithoutPapers } from 'src/app/shared/models/cashboxWithoutPapers.model';
import { StoreService } from 'src/app/shared/services/store.service';
import { getImageUrl } from 'src/app/shared/utils/cashbox';
import { LoginService } from '../../login/login.service';
import { urls } from '../../../shared/constants/URLs';
import * as serverConstant from '../../../shared/constants/server';
import { Router } from '@angular/router';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { CompareResult, StocktakingStatus } from 'src/app/shared/constants/driverBox';
import { LoginResponse, LoginResponseType } from '../../login/loginResponse';
import { StocktakingService } from './stocktaking.service';
import { stocktakingResponse } from './stocktakingResponse';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-stocktaking',
  templateUrl: './stocktaking.component.html',
  styleUrls: ['./stocktaking.component.css']
})
export class StocktakingComponent implements OnInit, OnDestroy {
  stocktakingCashbox = new CashboxWithoutPapers();
  amountInputChangedSub: Subscription;
  responseFromServerSub: Subscription;
  stocktakingModalBackgroundClickFunction = null;

  infoModalBackgroundClickFunction = null;
  infoModalIcon;
  infoModalHr;
  infoModalTitle;
  infoModalSubTitle;
  infoModalBtnTitle;
  infoModalBtnRightText;
  infoModalBtnLeftText;
  infoModalBtnRightClickFun;
  infoModalBtnLeftClickFun;
  infoModalError;

  compareResult;
  lockInterval;
  lockIntervalSec = 10;

  openCantSkipStocktaking = false;
  cantSkipStocktakingPopUpLabel;
  intervalTime = 2500;

  extraBlur;

  StocktakingStatus = StocktakingStatus;

  CANCEL_AND_DISCONNECT;
  CANCEL_BUTTON;
  DISCONNECT_WITHOUT_STOCKTAKING;

  constructor(
    private amountInputService: AmountInputService,
    private http: HttpClient,
    private loginService: LoginService,
    private storeService: StoreService,
    private router: Router,
    private modalService: ModalService,
    private stocktakingService: StocktakingService,
    private translateService: TranslateService
    ) { }

  ngOnInit(): void {
    this.storeService.getTranslation(translation => {
      this.CANCEL_AND_DISCONNECT = translation.PAGES.MAIN.STOCKTAKING.CANCEL_AND_DISCONNECT;
      this.CANCEL_BUTTON = translation.PAGES.MAIN.STOCKTAKING.CANCEL_BUTTON;
      this.DISCONNECT_WITHOUT_STOCKTAKING = translation.PAGES.MAIN.STOCKTAKING.DISCONNECT_WITHOUT_STOCKTAKING;

      if (this.getStocktakingStatus() == StocktakingStatus.START_SHIFT) {
        this.extraBlur = true;
      }
      else {
        this.extraBlur = false;
        if (this.getStocktakingStatus() == StocktakingStatus.MIDDLE_SHIFT) {
          this.stocktakingModalBackgroundClickFunction = this.closeStocktaking;
        }
      }

      this.responseFromServerSub = this.stocktakingService.responseFromServer.subscribe(
        (res: stocktakingResponse) => {
          if (res == stocktakingResponse.cashboxLocked) {
            sessionStorage.setItem(sessionKeys.cashboxLocked, "true");
            this.infoModalTitle = translation.PAGES.MAIN.STOCKTAKING.CASHBOX_LOCKED;
            this.infoModalSubTitle =
            [
              translation.PAGES.MAIN.STOCKTAKING.CASHBOX_LOCKED_REASON,
              `${translation.PAGES.MAIN.STOCKTAKING.WIILBE_DISCONNECT} ${this.lockIntervalSec} ${translation.GENERAL.SEC}`
            ];
            this.infoModalBtnTitle = null;
            this.infoModalHr = false;
            this.infoModalIcon = "assets/icons/infoIcons/error.svg";
            this.infoModalBtnLeftText = null;
            this.infoModalBtnLeftClickFun = null;
            this.infoModalBtnRightText = translation.GENERAL.LOGOUT_NOW;
            this.infoModalBtnRightClickFun = this.disconnect;
            this.infoModalBackgroundClickFunction = this.disconnect;

            this.lockInterval = setInterval(() => {
              this.lockIntervalSec--;
              if (this.lockIntervalSec == 0) {
                this.disconnect();
                clearInterval(this.lockInterval);
              }
              else {
                this.infoModalSubTitle =
                [
                  translation.PAGES.MAIN.STOCKTAKING.CASHBOX_LOCKED_REASON,
                  `${translation.PAGES.MAIN.STOCKTAKING.WIILBE_DISCONNECT} ${this.lockIntervalSec} ${translation.GENERAL.SEC}`
                ];
              }
            }, 1000);
          }
          else {
            this.defineStocktackingSuccessModal();
            this.modalService.open('info-modal-stocktacking');
          }
        }
      );

      this.amountInputChangedSub = this.amountInputService.inputChanged.subscribe(
        (data: subData) => {
          switch(data.operation) {
            case 'increase':
              this.stocktakingCashbox[data.type]++;
              break;
            case 'decrease':
              this.stocktakingCashbox[data.type]--;
              break;
            case 'change':
              this.stocktakingCashbox[data.type] = data.amount;
              break;
          }
        }
      );
    });
  }

  _getImageUrl(type) {
    return getImageUrl(type);
  }

  _getValue(code) {
    return getValue(code);
  }

  resetCashbox() {
    this.stocktakingCashbox = new CashboxWithoutPapers();
  }

  onSubmit() {
    this.storeService.getTranslation(translation => {
      this.loginService.checkLogin().subscribe(
        connectStatus => {
          if (connectStatus == true) {
            this.modalService.close('stocktaking-modal');

            const realCashbox = this.storeService.getCashbox();
            this.compareResult = realCashbox.compare(this.stocktakingCashbox);

            if (this.compareResult == CompareResult.IDENTICAL) {
              this.sendStocktakingToServer();
            }
            else {
              if (this.compareResult == CompareResult.DIFFERENT_SUM) {
                this.infoModalTitle = translation.PAGES.MAIN.STOCKTAKING.DIFFERENT_SUM;
              }
              else {
                this.infoModalTitle = translation.PAGES.MAIN.STOCKTAKING.COMPOSITION_ISSUE;
              }
              this.infoModalHr = false;
              this.infoModalIcon = "assets/icons/infoIcons/warning.svg";
              this.infoModalBtnRightText = translation.PAGES.MAIN.STOCKTAKING.BACK;
              this.infoModalBtnRightClickFun = this.returnToStocktaking;
              this.infoModalBackgroundClickFunction = null;
              this.infoModalBtnLeftText = translation.PAGES.MAIN.STOCKTAKING.FINISH;
              this.infoModalBtnLeftClickFun = this.sendStocktakingToServer;

              this.modalService.open('info-modal-stocktacking');
            }
          }
        }
      );
    });
  }

  sendStocktakingToServer = () => {
    let gapCashbox = this.stocktakingCashbox.getCopy();
    gapCashbox.decrease(this.storeService.getCashbox());
    const gapData = gapCashbox.convertToServerFormat();
    const shift = JSON.parse(sessionStorage.getItem(sessionKeys.shift));

    if (this.compareResult == CompareResult.IDENTICAL) {
      return this.http
      .post<ServerResponse>(
          urls.STOCKTAKING,
          {
            data: {
              device_ID: sessionStorage.getItem(sessionKeys.cashboxId),
              shift_ID: shift.Shift_id || shift.shiftId
            },
            sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
          })
            .subscribe(
              res => {
                if (res.statusCode == statusCode.OK) {
                  this.stocktakingService.responseFromServer
                  .next(stocktakingResponse.stocktackingSuccess);
                }
              }
            )
    }

    else {
      return this.http
        .post<ServerResponse>(
            urls.STOCKTAKING_WITH_LOCK,
            {
              data: {
                actionTypeTransaction: serverConstant.actionTypeTransaction.FIX_CASHBOX_AMOUNT_PROACTIVELY,
                deviceIdCashier: sessionStorage.getItem(sessionKeys.cashboxId),
                idNumberDriver: sessionStorage.getItem(sessionKeys.userId),
                shiftIdCashier:  shift.Shift_id || shift.shiftId,
                comment: "",
                driverCashboxData: gapData
              },
              sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
            })
              .subscribe(
                res => {
                  if (res.statusCode == statusCode.OK) {
                    this.storeService.updateCashBox().subscribe();

                    switch(this.compareResult) {
                      case CompareResult.DIFFERENT_COIN_COMPOSITION:
                        this.stocktakingService.responseFromServer
                        .next(stocktakingResponse.stocktackingSuccessDifferentCoinComposition);
                        break;
                      case CompareResult.DIFFERENT_SUM:
                        this.stocktakingService.responseFromServer
                        .next(stocktakingResponse.stocktackingSuccessDifferentSum);
                        break;
                    }
                  }
                  if (res.statusCode == statusCode.CASHBOX_LOCK) {
                    this.stocktakingService.responseFromServer
                    .next(stocktakingResponse.cashboxLocked);
                  }
                }
              )
    }
  }

  endStocktaking = () => {
    const stocktakingStatus = this.getStocktakingStatus();

    if (stocktakingStatus == StocktakingStatus.END_SHIFT) {
      this.loginService.disconnect()
      .subscribe(
        (res: LoginResponse) => {
          if (res.type == LoginResponseType.logoutFromSessionSuccessfully) {
            const operatorId = sessionStorage.getItem(sessionKeys.operatorId);
            this.router.navigate(['/login', operatorId]);
            sessionStorage.removeItem(sessionKeys.stocktakingStatus);
          }
        }
      );
    }
    else if (stocktakingStatus == StocktakingStatus.END_DAY) {
      this.loginService.closingDay()
      .subscribe(
        (res: LoginResponse) => {
          if (res.type == LoginResponseType.logoutFromSessionSuccessfully) {
            const operatorId = sessionStorage.getItem(sessionKeys.operatorId);
            this.router.navigate(['/login', operatorId]);
            sessionStorage.removeItem(sessionKeys.stocktakingStatus);
          }
        });
    }
    else {
      sessionStorage.setItem(sessionKeys.stocktakingStatus, StocktakingStatus.MIDDLE_SHIFT);
      this.router.navigate(['./main/home']);
    }
  }

  getStocktakingStatus() {
    return JSON.parse(sessionStorage.getItem(sessionKeys.stocktakingStatus));
  }

  getCancelStocktakingBtnText() {
    const stocktakingStatus = this.getStocktakingStatus();
    if (stocktakingStatus == null) {
      return;
    }
    switch(stocktakingStatus.toString()) {
      case StocktakingStatus.START_SHIFT:
        return this.CANCEL_AND_DISCONNECT;
      case StocktakingStatus.MIDDLE_SHIFT:
        return this.CANCEL_BUTTON;
      case StocktakingStatus.END_SHIFT:
      case StocktakingStatus.END_DAY:
        return this.DISCONNECT_WITHOUT_STOCKTAKING;
    }
  }

  closeStocktaking = () => {
    const stocktakingStatus = this.getStocktakingStatus();

    if (stocktakingStatus == StocktakingStatus.MIDDLE_SHIFT) {
      this.router.navigate(['./main/home']);
    }
    else if (stocktakingStatus == StocktakingStatus.END_SHIFT || stocktakingStatus == StocktakingStatus.END_DAY) {
      // sessionStorage.setItem(sessionKeys.stocktakingStatus, StocktakingStatus.MIDDLE_SHIFT);
      // this.router.navigate(['./main/home']);
      this.defineStocktackingWarningModal();
      this.modalService.close('stocktaking-modal');
      this.modalService.open('info-modal-stocktacking');
    }
    else if (stocktakingStatus == StocktakingStatus.START_SHIFT) {
      this.disconnect();
    }
  }

  openCantSkipStocktakingPopUp() {
    this.storeService.getTranslation(translation => {
      const _translation = translation.PAGES.MAIN.STOCKTAKING;
      const stocktakingStatus = this.getStocktakingStatus();
      stocktakingStatus == StocktakingStatus.START_SHIFT ? this.cantSkipStocktakingPopUpLabel = _translation.OPENNING : this.cantSkipStocktakingPopUpLabel = _translation.CLOSING;
      this.openCantSkipStocktaking = true;
      setTimeout(() => {
        this.openCantSkipStocktaking = false;
        this.router.navigate(['./main/home']);
      }, this.intervalTime);
    });
  }

  returnToStocktaking = () => {
    this.modalService.close('info-modal-stocktacking');
    this.modalService.open('stocktaking-modal');
  }

  defineStocktackingSuccessModal() {
    this.storeService.getTranslation(translation => {
      this.infoModalTitle = translation.PAGES.MAIN.STOCKTAKING.STOCKTAKING_SUCCESS;
      this.infoModalBtnTitle = null;
      this.infoModalHr = false;
      this.infoModalIcon = "assets/icons/infoIcons/ok.svg";
      this.infoModalBtnLeftText = translation.GENERAL.OK;
      this.infoModalBtnLeftClickFun = this.endStocktaking;
      this.infoModalBtnRightText = null;
      this.infoModalBtnRightClickFun = null;
      this.infoModalBackgroundClickFunction = null;
    });
  }

  defineStocktackingWarningModal() {
    this.storeService.getTranslation(translation => {
      const stocktakingStatus = this.getStocktakingStatus();
      let operation = translation.PAGES.MAIN.STOCKTAKING.TO_CLOSE + " ";
      operation += stocktakingStatus == StocktakingStatus.END_SHIFT ?
      translation.GENERAL.SHIFT : translation.GENERAL.DAY;
      this.translateService.stream('PAGES.MAIN.STOCKTAKING.ARE_YOU_SURE', {value: operation}).subscribe(translation => {
        this.infoModalTitle = translation;
      })
      this.infoModalBtnTitle = null;
      this.infoModalHr = false;
      this.infoModalIcon = "assets/icons/infoIcons/warning.svg";
      this.infoModalBtnLeftText = translation.GENERAL.YES;
      this.infoModalBtnLeftClickFun = this.endStocktaking;
      this.infoModalBtnRightText = `${translation.GENERAL.NO}, ${translation.PAGES.MAIN.STOCKTAKING.BACK}`;
      this.infoModalBtnRightClickFun = this.returnToStocktaking;
      this.infoModalBackgroundClickFunction = null;
    });
  }

  disconnect = () => {
    this.loginService.checkLogin().subscribe(
      connectStatus => {
        if (connectStatus == true) {
          this.loginService.disconnect()
          .subscribe(
          (res: LoginResponse) => {
              if (res.type == LoginResponseType.logoutFromSessionSuccessfully) {
                const operatorId = sessionStorage.getItem(sessionKeys.operatorId);
                this.router.navigate(['/login', operatorId]);
              }
          });
        }
      }
    );
  }

  ngOnDestroy() {
    this.amountInputChangedSub.unsubscribe();
    this.responseFromServerSub.unsubscribe();
  }
}
