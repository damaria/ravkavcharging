import { Component, OnDestroy, OnInit } from '@angular/core';
import { sendGetReadersCommand, handleReaderStatus, tryConnectToReaders, handleReadRavKav, writeToCard, writeToLogger } from '../../shared/constants/webSocketFunc';
import { MatDialog } from '@angular/material/dialog';
import { StoreService } from 'src/app/shared/services/store.service';
import { DialogRavKacComponent } from './haveRavKav/dialog-rav-kac/dialog-rav-kac.component';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { StocktakingStatus } from 'src/app/shared/constants/driverBox';
import { RefreshDataService, DataMessages } from 'src/app/shared/services/refresh-data.service';
import { ActionCode, CirtusMahar, ContractAction, ContractActionType, ServerResponse, SmartIdType, statusCode } from 'src/app/shared/constants/server';
import * as serverConstant from '../../shared/constants/server';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { Subscription } from 'rxjs';
import { MainService } from './main.service';
import { Commands, ReaderStatus, webSocketUrl } from 'src/app/shared/constants/cardReader';
import { DialogRavKacService } from './haveRavKav/dialog-rav-kac/dialog-rav-kac.service';
import { HttpClient } from '@angular/common/http';
import { urls } from 'src/app/shared/constants/URLs';
import { LocationService } from 'src/app/shared/services/location.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit, OnDestroy {
  dataOfReadRavKav;
  contractPosition;
  counterValueByPosition;
  contractProfile;
  typeTicket;
  dialogRef;
  userName: string;
  contractToWriteOnCard;

  infoModalBackgroundClickFunction;
  infoModalIcon;
  infoModalHr;
  infoModalTitle;
  infoModalSubTitle;
  infoModalBtnTitle;
  infoModalBtnRightText;
  infoModalBtnLeftText;
  infoModalBtnRightClickFun;
  infoModalBtnLeftClickFun;
  infoModalError;

  connection;
  validationNum;
  finishWithPaybackSub: Subscription;
  cashboxDataSub: Subscription;
  enterToHomePageSub: Subscription;
  paymentSum;

  serverData;
  profileType;
  data;

  transactionId;

  constructor(
    public dialog: MatDialog,
    public storeService: StoreService,
    private router: Router,
    private route: ActivatedRoute,
    private dataView: RefreshDataService,
    private modalService: ModalService,
    private mainService: MainService,
    private dialogRavKacService: DialogRavKacService,
    private http: HttpClient,
    private locationService: LocationService
  ) {
    this.userName = sessionStorage.getItem(sessionKeys.username);
  }

  ngOnInit(): void {
    const stocktakingStatus = sessionStorage.getItem(sessionKeys.stocktakingStatus);
    if (stocktakingStatus != StocktakingStatus.MIDDLE_SHIFT && this.router.url != "/main/stocktaking") {
      this.router.navigate(['./main/stocktaking']);
    }
    else {
      if (this.router.url == "/main") {
        this.router.navigate(['./home'], { relativeTo: this.route });
      }
    }

    this.finishWithPaybackSub = this.mainService.finishWithPayback.subscribe(
      (cashboxData) => {
        if (cashboxData) {
          this.cashboxTransaction(this.validationNum, cashboxData);
          //reset finishWithPayback
          this.mainService.finishWithPayback.next(null);
        }
      }
    );
    this.cashboxDataSub = this.mainService.cashboxData.subscribe(
      (data : any) => {
        this.data = data;
      }
    );

    this.enterToHomePageSub = this.mainService.enterToHomePage.subscribe(
      port => {
        this.connect(port);
      }
    );
    this.mainService.getAllContracts();

    this.router.events.subscribe(
      (res: NavigationStart) => {
        if (res.url == "/main" || res.url == "/main/") {
          const stocktakingStatus = sessionStorage.getItem(sessionKeys.stocktakingStatus);
          if (stocktakingStatus == StocktakingStatus.MIDDLE_SHIFT) {
            this.router.navigate(['/main/home']);
          }
          else {
            this.router.navigate(['/main/stocktaking']);
          }
        }
      }
    );
  }

  connect(port) {
    if(!port) {
      this.mainService.portNotDefined();
      return;
    }
    const connection = new WebSocket(`${webSocketUrl}:${port}`);
    this.connection = connection;

    connection.onopen = (evt) => {
      this.mainService.currReaderSuffix = 0;
      sendGetReadersCommand(connection);
    };

    connection.onmessage = (evt) => {
      var msg = JSON.parse(evt.data);
      console.log(msg);

      switch (msg.command) {
        case Commands.GET_READERS:
          tryConnectToReaders(msg, this.mainService);
          break;
        case Commands.READER:
          if (!this.mainService.canCloseDiaolog) {
            break;
          }
          if (this.dialogRef && msg.cardExists == false) {
            this.dialogRef.close();
          }
          else {
            handleReaderStatus(msg, this.mainService);
          }
          break;
        case Commands.READ_RAVKAV:
          this.dataOfReadRavKav = msg;
          if (handleReadRavKav(msg, this.mainService)) {
            if ((!this.dialog.openDialogs || this.dialog.openDialogs.length == 0) && this.router.url == "/main/home") {
              this.openDialog(msg);
            }
          }
          break;
        case Commands.WRITE_RAVKAV:
          this.handleWriteRavKav(msg);
          break;
      }
    };

    connection.onerror = (evt) => {
      this.mainService.disconnectCardReaderProgram();
      console.error(evt);
    };
  }

  handleWriteRavKav(msgWriteRavKav) {
    if (msgWriteRavKav.status == ReaderStatus.FAILED) {
      this.mainService.writeFailed.next(null);
      this.openFailedWriteToCardModal();
      return;
    }
    switch (this.data.actionCode) {
      case ActionCode.ADD_PERSONALIZATION:
        this.sentAddPersonalization();
        break;
      case ActionCode.LOAD_OR_CANCEL_CONTRACT:
        // adding isLast attribute to snapshots array
        if (this.needMoreThanOneWriteOperation()) {
          this.data.serverData.snapshots[0].isLast = false;
          this.data.serverData.snapshots[1].isLast = true;
        }
        else {
          this.data.serverData.snapshots[0].isLast = true;
        }

        this.data.serverData.snapshots.forEach(snapshot => {
          this.sentAddTransaction(snapshot);
        });
        break;
    }

    this.readAgain();
  }

  readAgain() {
    var msg = {
      command: Commands.READ_RAVKAV
    };
    this.connection.send(JSON.stringify(msg));
  }

  openDialog(readRavKav) {
    this.dialogRef = this.dialog.open(DialogRavKacComponent, {
      data: readRavKav,
      closeOnNavigation: false,
      disableClose: true
    });

    this.dialogRef.componentInstance.reayToWriteOnCard.subscribe((data) => {
      if (data.serverData) {
        this.contractToWriteOnCard =  data.serverData;
        this.counterValueByPosition = data.serverData.counterValueByPosition;
        writeToCard(data.serverData);
      }
      else {
        writeToCard(data);
      }
    });

    this.dialogRef.componentInstance.successLoadOnRavKav.subscribe(() => {
      this.dataView.emitChangeSource.next({ type: DataMessages.REFRESH_DATA, payload: this.dataOfReadRavKav })
    });
  }

  initToPurchaseContract(arrayElement, pushField, field) {
    this.dataOfReadRavKav[arrayElement].forEach(element => {
      pushField.push(element !== null ? element[field] : "")
    });
  }

  sentAddTransaction(snapshot, secondTime?) {
    this.storeService.middleOfProcess.next(true);
    const shift = JSON.parse(sessionStorage.getItem(sessionKeys.shift));
    const routeSystem = sessionStorage.getItem(sessionKeys.cashboxClusterId) ? sessionStorage.getItem(sessionKeys.cashboxClusterId) : '0';

    let counter = this.dataOfReadRavKav.counter.counterOriginDump;
    counter = counter.substring(0, counter.length - 4);

    const snapshotCircumstance = snapshot.circumstances ?? snapshot.circumstance;

    let query = {
      actionCode: ActionCode.LOAD_OR_CANCEL_CONTRACT,
      actionType: snapshot.actionType.toString(),
      cancelId: "0",
      cardId: this.dataOfReadRavKav.cardSerialNumber,
      contractPosition: (this.data.serverData.position + 1).toString(),
      counterValue: this.data.serverData.counterValueByPosition.toString(),
      deviceId: sessionStorage.getItem(sessionKeys.cashboxId),
      driverId: sessionStorage.getItem(sessionKeys.userId),
      price: (this.data.paymentSum * 100).toFixed(),
      id: snapshot.deviceValidationId.toString(),
      nEntries: "1",
      operator: sessionStorage.getItem(sessionKeys.operatorId),
      passengerType: this.data.profileType.toString(),
      routeSystem: routeSystem,
      samSerialNumber: sessionStorage.getItem(sessionKeys.cardReaderSamSerialNumber),
      serialNumberDevice: sessionStorage.getItem(sessionKeys.cashboxDeviceNum),
      shiftId: shift.Shift_id || shift.shiftId,
      subContractor: "0",
      ticketType: this.data.typeTicket.toString(),
      timestamp: snapshot.timestamp,
      versionNumber: "1",
      timeType: "1",
      card: snapshot.newCard,
      //new fileds: MAHAR
      cdmVersion: serverConstant.CardDataModelVersion,
      cm: CirtusMahar.YES,
      ettCode: this.data.ettCode.toString(),
      sharingCode: this.data.sharingCode.toString(),
      smartIdNumber: this.dataOfReadRavKav.cardSerialNumber,
      smartIdType: SmartIdType.RAV_KAV,
      lat: this.locationService.latitude.toString(),
      lon: this.locationService.longitude.toString(),
      circumstances: snapshotCircumstance.toString(), //temp until new version
      circumstance: snapshotCircumstance.toString()
    };

    if (this.needToManipulatePrice(snapshot.contractActionType)) {
      query.price = "0";
    }

    //sent to the card reader logger the query of add_load_contract_transaction
    writeToLogger(
      `Send to ${urls.ADD_LOAD_CONTRACT_TRANSACTION} ` +
      `this body: { transaction: ${JSON.stringify(query)}, sessionData: ${JSON.stringify(JSON.parse(sessionStorage.getItem(sessionKeys.sessionData)))} }`
    );

    //sent add load contract transaction to the server
    this.http
    .post<ServerResponse>(
      urls.ADD_LOAD_CONTRACT_TRANSACTION,
      {
        transaction: query,
        sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
      }
    ).subscribe(res => {
      //sent to the card reader logger the response of add_load_contract_transaction
      writeToLogger(
        `Received from ${urls.ADD_LOAD_CONTRACT_TRANSACTION} ` +
        `${JSON.stringify(res)}}`
      );

      // on failure (dosent get Validation_num), try again just one time.
      if (secondTime == undefined && res.data.Validation_num == undefined) {
        this.sentAddTransaction(snapshot, true);
        return;
      }

      if (snapshot.isLast) {
        this.validationNum = res.data.Validation_num?.toString() ?? null;
        if (!this.mainService.paybackNeeded) {
          this.cashboxTransaction(this.validationNum, this.data.cashboxData);
        }
        else {
          this.mainService.NeedToOpenPayback.next(null);
        }
      }
    },
    error => {
      this.storeService.middleOfProcess.next(false);
    });
  }

  sentAddPersonalization() {
    this.http
    .post<ServerResponse>(
      urls.ADD_PERSONALIZATION,
      {
        data: {
          employeeIdNumber: sessionStorage.getItem(sessionKeys.userId),
          deviceId: sessionStorage.getItem(sessionKeys.cashboxId),
          time_stamp: new Date().getTime(),
          personIdNumber: this.dataOfReadRavKav.environment.holdersNationalid.toString(),
          cardSerial: this.dataOfReadRavKav.cardSerialNumber.toString(),
          restoredCardSerial: serverConstant.general.NULL,
          filesPath: serverConstant.general.NULL,
          card: this.data.card,
          actionCode: ActionCode.ADD_PERSONALIZATION
        },
        sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
      }
    )
    .subscribe(
      () => {
        this.mainService.needToUpdateEvents.next(null);
        this.dialogRef.close();

        this.openSuccessToWriteToCardModal();
      }
    );
  }

  cashboxTransaction = (validationNum, cashboxData) => {
    const shift = JSON.parse(sessionStorage.getItem(sessionKeys.shift));

    let actionTypeTransaction;
    if (this.data.contractAction == ContractAction.LOAD) {
      actionTypeTransaction = serverConstant.actionTypeTransaction.LOAD_CONTRACT
    }
    else {
      actionTypeTransaction = serverConstant.actionTypeTransaction.CANCEL_CONTRACT;
    }

    const markTransactions = [{
      operator_id: sessionStorage.getItem(sessionKeys.operatorId),
      Validation_num: validationNum
    }];

    const query = {
      deviceIdCashier: sessionStorage.getItem(sessionKeys.cashboxId),
      idNumberDriver: sessionStorage.getItem(sessionKeys.userId),
      shiftIdCashier: shift.Shift_id || shift.shiftId,
      driverCashboxData: cashboxData,
      shifts: [],
      jsonObjRes: true,
      comment: this.mainService.comment,
      ... validationNum ? { markTransactions: markTransactions } : {},
      actionTypeTransaction: actionTypeTransaction,
      timestamp: new Date().getTime(),
      driverId: this.dataOfReadRavKav.cardSerialNumber.toString()
    }

    //sent add driver cashbox transaction to the card reader logger
    writeToLogger(
      `Send to ${urls.ADD_DRIVER_CASHBOX} ` +
      `this body: { data: ${JSON.stringify(query)}, sessionData: ${JSON.stringify(JSON.parse(sessionStorage.getItem(sessionKeys.sessionData)))} }`
    );

    //sent add driver cashbox transaction to the server
    this.http
    .post<ServerResponse>(
      urls.ADD_DRIVER_CASHBOX,
      {
        data: query,
        sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
      })
      .subscribe(res => {
        if (res.statusCode == statusCode.OK) {
          this.storeService.updateCashBox().subscribe();
          this.mainService.needToUpdateEvents.next(null);
          this.transactionId = res.data[0].TransactionId;
          this.openSuccessToWriteToCardModal();
        }
        this.dialogRef.close();
      },
      error => {
        this.storeService.middleOfProcess.next(false);
      })
  }

  openSuccessToWriteToCardModal = () => {
    this.storeService.getTranslation(translation => {
      this.infoModalSubTitle = null;
      switch(this.data.actionCode) {
        case ActionCode.ADD_PERSONALIZATION:
          this.infoModalTitle = translation.PAGES.MAIN.ADD_PROFILE_SUCCESS;
          break;
        case ActionCode.LOAD_OR_CANCEL_CONTRACT:
          switch(this.data.contractAction) {
            case ContractAction.LOAD:
              this.infoModalTitle = translation.PAGES.MAIN.LOAD_CONTRACT_SUCCESS;
              break;
            case ContractAction.CANCEL:
              this.infoModalTitle = translation.PAGES.MAIN.CANCEL_CONTRACT_SUCCESS;
              break;
          }
          break;
      }
      this.infoModalIcon = "assets/icons/infoIcons/ok.svg";

      if (this.data.actionCode == ActionCode.LOAD_OR_CANCEL_CONTRACT) {
        this.infoModalBtnTitle = translation.PAGES.MAIN.PRINT_REFERENCE;
        this.infoModalHr = true;
        this.infoModalBtnLeftText = translation.GENERAL.YES;
        this.infoModalBtnLeftClickFun = this.printReceipt;
        this.infoModalBtnRightText = translation.GENERAL.NO;
        this.infoModalBtnRightClickFun = this.closeInfoModalAndReadAgain;
        this.infoModalBackgroundClickFunction = null;
      }
      else {
        this.infoModalBtnTitle = null;
        this.infoModalHr = null;
        this.infoModalBtnLeftText = null;
        this.infoModalBtnLeftClickFun = null;
        this.infoModalBtnRightText = translation.GENERAL.OK;
        this.infoModalBtnRightClickFun = this.closeInfoModalAndReadAgain;
        this.infoModalBackgroundClickFunction = this.closeInfoModalAndReadAgain;
      }

      this.modalService.open('info-modal-main');
    });
  }

  openFailedWriteToCardModal = () => {
    this.storeService.getTranslation(translation => {
      this.infoModalIcon = "assets/icons/infoIcons/error.svg";
      this.infoModalTitle =
      this.data.actionCode == ActionCode.ADD_PERSONALIZATION ? translation.PAGES.MAIN.ADD_PROFILE_ERROR :
      this.data.contractAction == ContractAction.LOAD ? translation.PAGES.MAIN.LOAD_CONTRACT_ERROR : translation.PAGES.MAIN.CANCEL_CONTRACT_ERROR;
      this.infoModalSubTitle = [translation.PAGES.MAIN.WRITING_CARD_ERROR, translation.PAGES.MAIN.READER_KIND_OR_SETTING_ERROR];
      this.infoModalBtnTitle = null;
      this.infoModalHr = null;
      this.infoModalBtnLeftText = null;
      this.infoModalBtnLeftClickFun = null;
      this.infoModalBtnRightText = translation.GENERAL.OK;
      this.infoModalBtnRightClickFun = this.closeInfoModalAndGoBack;
      this.infoModalBackgroundClickFunction = this.closeInfoModalAndGoBack;

      this.modalService.open('info-modal-main');
    });
  }

  printReceipt = () => {
    this.storeService.getTranslation(translation => {
      const _translation = translation.PAGES.MAIN;
      const fileName = this.data.contractAction == ContractAction.LOAD ? _translation.LOAD_CONTRACT : _translation.CANCEL_CONTRACT;
      this.storeService.printReceipt(this.transactionId, fileName);
      this.closeInfoModal();
    });
  }

  closeInfoModal = () => {
    this.modalService.close('info-modal-main');
  }

  closeInfoModalAndReadAgain = () => {
    this.storeService.middleOfProcess.next(false);
    this.closeInfoModal();
    this.readAgain();
  }

  closeInfoModalAndGoBack = () => {
    this.closeInfoModal();
    this.goBack();
  }

  goBack = () => {
    this.dialogRavKacService.backToHome.next(null);
  }

  needMoreThanOneWriteOperation(): boolean {
    return this.data.serverData.snapshots.length > 1;
  }

  needToManipulatePrice(contractActionType): boolean {
    // if its a dummy operation
    return contractActionType == ContractActionType.CANCEL_FOR_LOAD || contractActionType == ContractActionType.LOAD_FOR_CANCEL;
  }

  ngOnDestroy(): void {
    this.finishWithPaybackSub.unsubscribe();
    this.cashboxDataSub.unsubscribe();
    this.enterToHomePageSub.unsubscribe();
  }
}
