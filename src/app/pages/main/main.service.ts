import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject, Subject } from "rxjs";
import { filter, tap } from "rxjs/operators";
import { cashboxDataType, ServerResponse, statusCode } from "src/app/shared/constants/server";
import { sessionKeys } from "src/app/shared/constants/sessionKey";
import { urls } from "src/app/shared/constants/URLs";
import { ContractsService } from "src/app/shared/services/contracts.service";
import { StoreService } from "src/app/shared/services/store.service";
import * as serverConstant from '../../shared/constants/server';

@Injectable({providedIn: 'root'})
export class MainService {
    constructor(
        private http: HttpClient,
        private contractsService: ContractsService,
        private storeService: StoreService
    ){}

    NeedToOpenPayback = new Subject();
    finishWithPayback = new BehaviorSubject(null);
    cashboxData = new Subject();
    enterToHomePage = new Subject();
    needToUpdateEvents = new Subject();
    paybackNeeded = false;
    comment;
    ravkavText = 'PAGES.MAIN.CONNECT_TO_CR';
    ravkavImg = "./assets/imgs/putRavKav.png";
    errorText = null;
    currReaderSuffix;
    writeFailed = new Subject();
    canCloseDiaolog = true;

    getAllContracts = (profiles?, isAnonymousCard?) => {
        if (profiles == "") {
            const profilesContractsFromServer = [];
            sessionStorage.setItem(sessionKeys.profilesContractsFromServer, JSON.stringify(profilesContractsFromServer));
            this.contractsService.initDataOfContract(isAnonymousCard);
            return;
        }
        const profileType = profiles ? profiles : serverConstant.general.NULL;
        this.http
        .post<ServerResponse>(
            urls.GET_CONTRACTS,
            {
                data: {
                    clusterId: serverConstant.general.NULL,
                    dt: serverConstant.general.NULL,
                    ettCode: serverConstant.general.NULL,
                    profileType: profileType,
                    shareCode: serverConstant.general.NULL,
                    canLoad : "1"
                },
                sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
            }
        )
        .subscribe(res => {
            if (res.statusCode == statusCode.OK) {
                if (profiles) {
                    const profilesContractsFromServer = JSON.stringify(res.data);
                    sessionStorage.setItem(sessionKeys.profilesContractsFromServer, profilesContractsFromServer);
                    this.contractsService.initDataOfContract(isAnonymousCard);
                }
                else {
                    const contractsFromServer = JSON.stringify(res.data);
                    sessionStorage.setItem(sessionKeys.contractsFromServer, contractsFromServer);
                }
            }
        })
    }

    getGeneralDataFromServer() {
        return this.http
        .post<ServerResponse>(
            urls.DATA_RECORDS,
            {
                data: {
                    ettCodes: {},
                    profiles: {},
                    operators: {},
                    shareCodeReformZones: {}
                },
                sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
            }
        )
        .pipe(
            tap(res => {
                if (res.statusCode == statusCode.OK) {
                    const allData = res.data;
                    sessionStorage.setItem(sessionKeys.serverShareCodeReformZones, JSON.stringify(allData.shareCodeReformZones.data));
                    sessionStorage.setItem(sessionKeys.serverProfiles, JSON.stringify(allData.profiles.data));
                    sessionStorage.setItem(sessionKeys.serverEttCodes, JSON.stringify(allData.ettCodes.data));
                    sessionStorage.setItem(sessionKeys.serverOperators, JSON.stringify(allData.operators.data));
                }
            })
        )
    }

    noCardReader() {
      this.errorText = null;
      this.ravkavText = 'PAGES.MAIN.CR_NEEDED';
      this.ravkavImg = "./assets/imgs/putRavKav.png";
    }

    disconnectCardReaderProgram() {
      this.errorText = null;
      this.ravkavText = 'PAGES.MAIN.CR_SOFTWARE_NEEDED';
      this.ravkavImg = "./assets/imgs/putRavKav.png";
    }

    portNotDefined() {
        this.errorText = null;
        this.ravkavText = 'PAGES.MAIN.PORT_NOT_DEFINED';
        this.ravkavImg = "./assets/imgs/putRavKav.png";
    }

    failedToReadRavkav() {
        this.errorText = null;
        this.ravkavText = 'PAGES.MAIN.ERROR_READ_CARD';
        this.ravkavImg = "./assets/imgs/putRavKav.png";
    }

    cardReaderConnected() {
        this.errorText = null;
        this.ravkavText = "PAGES.MAIN.ATTACH_CARD";
        this.ravkavImg = "./assets/imgs/putRavKav.png";
    }

    cardNoValid() {
        this.errorText = 'PAGES.MAIN.CARD_NO_VALID';
    }

    noIsraeliCard() {
        this.errorText = 'PAGES.MAIN.NO_ISRAEL_ASSOCIATED';
    }

    burnedCard() {
        this.errorText = 'PAGES.MAIN.BURNED_CARD';
    }

    cardVersionsError(errorStatus) {
        if (errorStatus.specialEvents) {
            this.errorText = 'PAGES.MAIN.VERSION_ERROR_SPECIAL_EVENTS';
        }
        if (errorStatus.events) {
          this.errorText = 'PAGES.MAIN.VERSION_ERROR_EVENTS';
        }
        if (errorStatus.contracts) {
          this.errorText = 'PAGES.MAIN.VERSION_ERROR_CONTRACTS';
        }
        if (errorStatus.environment) {
            this.errorText = 'PAGES.MAIN.VERSION_ERROR_ENVIROMENT';
        }
    }

    equalToSuffix(reader) {
        const readerSuffix = reader.name.substr(reader.name.length-1);
        return readerSuffix == this.currReaderSuffix;
    }

    getCreditValue() {
        this.http
        .post<ServerResponse>(
            urls.GET_DRIVER_CASHBOX_DATA_TYPE,
            {
                data: {
                    dataType_ID : cashboxDataType.credit.toString(),
		            jsonObjRes : true
                },
                sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
            }
        )
        .pipe(
          filter(res => res.statusCode == statusCode.OK)
        )
        .subscribe(res => {
            const CreditDataType = res.data.find(dataType => dataType.DataType_ID == cashboxDataType.credit);
            this.storeService.CreditValue = parseInt(CreditDataType.RelationToSmallestPrice);
        })
    }
}
