import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

@Injectable({providedIn: "root"})
export class DialogRavKacService {
    from;
    backToHome = new Subject();
}