import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { From } from 'src/app/shared/constants/driverBox';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';
import { DialogRavKacService } from '../../dialog-rav-kac.service';
import * as serverConstant from '../../../../../../shared/constants/server';
import { EventCircumstances, ServerResponse } from '../../../../../../shared/constants/server';
import { isAfterToday, isFreeMonthly } from 'src/app/shared/utils/date';
import { hasGoldkavFreeCertificateContract, isAnonymousCard, isValidPriority } from 'src/app/shared/utils/ravkav';
// import { getErrorStatus } from 'src/app/shared/utils/server';
import { urls } from 'src/app/shared/constants/URLs';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { SelectMonthFreeMonthlyComponent } from 'src/app/shared/components/dialogs/select-month-free-monthly/select-month-free-monthly.component';
import { GOLD_KAV_PROFILE, TariffContractNameCode } from 'src/app/shared/constants/ravKav';
import { StoreService } from 'src/app/shared/services/store.service';
import { TranslateService } from '@ngx-translate/core';
import { DatePipe } from '@angular/common';
import { getErrorStatusTranslationPath } from 'src/app/shared/utils/server';

@Component({
  selector: 'app-display-details-of-rav-kav',
  templateUrl: './display-details-of-rav-kav.component.html',
  styleUrls: ['./display-details-of-rav-kav.component.css']
})
export class DisplayDetailsOfRavKavComponent {
  cardSerialNumber: string;
  envendDate: string;
  profiles = [];
  contracts = [];
  events = [];
  titleTravelContracts = [];
  titleEvents = [];
  eventCircumstances = [];
  ravKavDetails;

  dataRecords = {
    profilesWS: JSON.parse(sessionStorage.getItem(sessionKeys.serverProfiles)),
    ettCodesWS: JSON.parse(sessionStorage.getItem(sessionKeys.serverEttCodes)),
    operatorsWS: JSON.parse(sessionStorage.getItem(sessionKeys.serverOperators)),
    shareCodeReformZonesWS: JSON.parse(sessionStorage.getItem(sessionKeys.serverShareCodeReformZones))
  }

  contractsFromServer = JSON.parse(sessionStorage.getItem(sessionKeys.contractsFromServer));

  @Output() loadContractClicked = new EventEmitter<any>();
  @Output() editProfileClicked = new EventEmitter<any>();
  @Output() canLoadContract = new EventEmitter<any>();
  @Output() cancelContract = new EventEmitter<any>();

  isAnonymousCard;
  hasGoldkavFreeCertificateContract;
  hasGoldkavProfile;

  @Input('data')
  set getDataRecords(data) {
    this.ravKavDetails = data["ravKavDetails"];
    this.isAnonymousCard = isAnonymousCard(this.ravKavDetails);
    this.hasGoldkavFreeCertificateContract = hasGoldkavFreeCertificateContract(this.ravKavDetails);
    this.hasGoldkavProfile = this.isHasGoldkavProfile();
  }

  infoModalBackgroundClickFunction;
  infoModalIcon;
  infoModalHr;
  infoModalTitle;
  infoModalSubTitle;
  infoModalBtnTitle;
  infoModalBtnRightText;
  infoModalBtnLeftText;
  infoModalBtnRightClickFun;
  infoModalBtnLeftClickFun;
  infoModalError;

  daysUntilcardExpierd = 0;
  daysUntilcardExpierdString;
  adminCashbox;
  haventSuitableProfile;
  adminAndOperatorAndGoldKav;

  constructor(
    private dialogRavKacService: DialogRavKacService,
    private modalService: ModalService,
    private http: HttpClient,
    public dialog: MatDialog,
    public storeService: StoreService,
    private translateService: TranslateService,
    private datePipe: DatePipe
    ) { }

  async initData() {
    if (this.ravKavDetails) {
      this.cardSerialNumber = this.ravKavDetails.cardSerialNumber;
      this.envendDate = this.ravKavDetails.environment.envendDate;
      let i = 1;
      while (this.ravKavDetails.environment["holderProfile" + i + "Code"] != undefined && this.ravKavDetails.environment["holderProfile" + i + "Date"] != undefined) {
        this.getProfile(this.ravKavDetails.environment["holderProfile" + i + "Code"], profileDesc => {
          this.profiles[i - 1] = {
            code: this.ravKavDetails.environment["holderProfile" + i + "Code"],
            profile: profileDesc,
            date: this.ravKavDetails.environment["holderProfile" + i + "Date"]
          }
          i += 1;
        });
      }
      const contracts = this.ravKavDetails.contracts;
      const events = this.ravKavDetails.events;
      for (let index = 0; index < contracts.length; index++) {
        const contractPriority = events[0]["eventBestContractPriority" + (index + 1).toString()];
        if (contracts[index] && isValidPriority(contractPriority)) {
          this.initContract(index, contract => {
            this.contracts.push(contract);
          });
        }
        else {
          this.contracts.push(null);
        }
      }
      for (let index = 0; index < events.length; index++) {
        if (events[index]) {
          this.events[index] = this.initEvent(index);
        }
      }
    }
  }

  ngOnInit() {
    this.storeService.getTranslation(translation => {
      const _translation = translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.COMPONENTS.DISPLAY_DETAILS_OF_RAVKAV;
      this.titleTravelContracts = [
        _translation.TITLES.CONTRACTS_TITLE.CONTRACT_KIND,
        _translation.TITLES.CONTRACTS_TITLE.CONTRACT_NAME,
        _translation.TITLES.CONTRACTS_TITLE.FROM,
        _translation.TITLES.CONTRACTS_TITLE.TO,
        _translation.TITLES.CONTRACTS_TITLE.BALANCE,
        _translation.TITLES.CONTRACTS_TITLE.OPERATOR,
        _translation.TITLES.CONTRACTS_TITLE.PURCHASE_DATE,
        _translation.TITLES.CONTRACTS_TITLE.SHARING_CODE,
        _translation.TITLES.CONTRACTS_TITLE.PROFILE
      ];
      this.titleEvents = [
        _translation.TITLES.EVENTS_TITLE.OPERATION,
        _translation.TITLES.EVENTS_TITLE.RELEVANT_CONTRACT,
        _translation.TITLES.EVENTS_TITLE.PASSENGERS_AMOUNT,
        _translation.TITLES.EVENTS_TITLE.LINE,
        _translation.TITLES.EVENTS_TITLE.OPERATOR,
        _translation.TITLES.EVENTS_TITLE.DATE,
        _translation.TITLES.EVENTS_TITLE.HOUR
      ];
      this.eventCircumstances = [
        _translation.TITLES.EVENTS_CIRCUMSTANCES.UNSPECIFIED,
        _translation.TITLES.EVENTS_CIRCUMSTANCES.ENTRY,
        _translation.TITLES.EVENTS_CIRCUMSTANCES.EXIT,
        _translation.TITLES.EVENTS_CIRCUMSTANCES.PASSAGE,
        _translation.TITLES.EVENTS_CIRCUMSTANCES.ON_BOARD_CONTROL,
        _translation.TITLES.EVENTS_CIRCUMSTANCES.TEST,
        _translation.TITLES.EVENTS_CIRCUMSTANCES.INTERCHANGE_ENTRY,
        _translation.TITLES.EVENTS_CIRCUMSTANCES.INTERCHANGE_EXIT,
        _translation.TITLES.EVENTS_CIRCUMSTANCES.OTHER_USAGE,
        _translation.TITLES.EVENTS_CIRCUMSTANCES.CANCELLETION,
        , // no 10 index
        , // no 11 index
        _translation.TITLES.EVENTS_CIRCUMSTANCES.CONTRACT_LOADING_WITH_IMMEDIATE_FIRST_USE,
        _translation.TITLES.EVENTS_CIRCUMSTANCES.CONTRACT_LOADING,
        _translation.TITLES.EVENTS_CIRCUMSTANCES.APPLICATION_ISSUING,
        _translation.TITLES.EVENTS_CIRCUMSTANCES.INVALIDATION
      ];

      this.initData();

      const today = new Date();
      const endCardDate = new Date(this.envendDate).setHours(23,59,59,999);

      const Difference_In_Time = endCardDate - today.getTime();
      const Difference_In_Days: any = Difference_In_Time / (1000 * 3600 * 24);

      this.daysUntilcardExpierd = Math.ceil(Difference_In_Days);
      if (this.daysUntilcardExpierd == 1) {
        this.daysUntilcardExpierdString = _translation.ONE_DAY_LEFT;
      }
      else {
        this.translateService.stream('PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.COMPONENTS.DISPLAY_DETAILS_OF_RAVKAV.SOME_DAYS_LEFT', { daysAmount: this.daysUntilcardExpierd }).subscribe(translation => {
          this.daysUntilcardExpierdString = translation;
        })
      }
      this.daysUntilcardExpierdString = { leftDays: this.daysUntilcardExpierdString };
    });

    this.adminCashbox = JSON.parse(sessionStorage.getItem(sessionKeys.adminCashbox));
    this.haventSuitableProfile = this.adminCashbox && this.profiles.every(profile => {
      return profile.code != this.storeService.operatorConfiguration.operatorProfile;
    });
    this.adminAndOperatorAndGoldKav = this.adminCashbox && this.profiles.some(profile => {
      return profile.code == this.storeService.operatorConfiguration.operatorProfile;
    }) && this.profiles.some(profile => {
      return profile.code == GOLD_KAV_PROFILE;
    })
  }

  getProfile(profileId, callbackFun) {
    this.storeService.getTranslation(translation => {
      let shouldSkip = false;
      let profileDesc: string;
      this.dataRecords.profilesWS.forEach(element => {
        if (shouldSkip) {
          return;
        }
        if (element.Passenger_Profile_id == profileId) {
          profileDesc = element.Descr_HE;
          shouldSkip = true;
        }
      });
      profileDesc ? callbackFun(profileDesc) : callbackFun(`${translation.GENERAL.UNIDENTIFIED} (${profileId})`);
    });
  }

  getEttCode(currentContract) {
    let shouldSkip = false;
    let ettCodeDesc: string;
    this.dataRecords.ettCodesWS.forEach((ett) => {
      if (shouldSkip) {
        return;
      }
      const contractLocation = currentContract.locations[0];
      if (ett.Contract_Tariff == currentContract.tariffContractNameCode && contractLocation && ett.ETT_Type == contractLocation.SpatialContractETTCode) {
        ettCodeDesc = ett.ETT_Descr;
        shouldSkip = true;
      }
    });
    return ettCodeDesc ? ettCodeDesc : "";
  }

  getOperator(codeOperator) {
    let shouldSkip = false;
    let ettCodeDesc: string;
    this.dataRecords.operatorsWS.forEach((operator) => {
      if (shouldSkip) {
        return;
      }
      if (operator.Operator_id == codeOperator) {
        ettCodeDesc = operator.Descr;
        shouldSkip = true;
      }
    });
    return ettCodeDesc;
  }

  initContract(i: number, callbackFun) {
    const currContract = this.ravKavDetails.contracts[i];
    const contractParams = this.getContractParam(i);
    const location9 = currContract.locationHashMap[9];
    const shareCode = location9 ? location9.predefinedSharingCode : null;
    const location0 = this.ravKavDetails.contracts[i].locations[0];
    const ettCode = location0 ? "" + this.ravKavDetails.contracts[i].tariffContractNameCode + this.ravKavDetails.contracts[i].locations[0].SpatialContractETTCode : null;

    this.getProfile(currContract.contractCustomerProfile, profile => {
      const contract = {
        contractType: this.getEttCode(currContract),
        ettCode: ettCode,
        contractName: contractParams.contractName,
        from: this.datePipe.transform(currContract.contractValidityStartDate, 'shortDate'),
        to: this.datePipe.transform(contractParams.endDate, 'shortDate'),
        amount: contractParams.amount,
        provider: this.getOperator(currContract.contractProvider),
        saleDate: this.datePipe.transform(currContract.contractSaleDate, 'shortDate'),
        shareCode: shareCode,
        profile: profile,
        profileType: currContract.contractCustomerProfile,
        contractPriceId: contractParams.contractPriceId,
        price: contractParams.price,
        exactPrice: contractParams.exactPrice,
        originalContractValidityStartDate: contractParams.originalContractValidityStartDate
      };

      callbackFun(contract);
    })
  }

  getContractParam(i: number) {
    let contractName, endDate, amount;
    const currContract = this.ravKavDetails.contracts[i];

    if (currContract.contractValidityInfoBitMap[4] == "1") {
      var mydate = new Date(currContract.contractValidityStartDate + " UTC");
      switch (currContract.contractValidityDurationUnit) {
        case 0:
          mydate.setMonth(mydate.getMonth() + currContract.contractValidityDurationNumber);
          break;
        case 1:
          mydate.setDate(mydate.getDate() + (currContract.contractValidityDurationNumber * 7));
          break;
        case 2:
          mydate.setDate(mydate.getDate() + currContract.contractValidityDurationNumber);
          break;
        case 3:
          mydate.setMinutes(mydate.getMinutes() + (currContract.contractValidityDurationNumber * 30));
          break;
      }

      endDate = mydate.getFullYear() + "-" + ((mydate.getMonth() + 1) < 10 ? ("0" + (mydate.getMonth() + 1)) : (mydate.getMonth() + 1)) + "-" +
        (mydate.getDate() < 10 ? ("0" + mydate.getDate()) : mydate.getDate());
    }

    else if (currContract.contractValidityInfoBitMap[5] == "1") {
      endDate = currContract.contractValidityEndDate;
    }

    else if (currContract.tariffContractNameCode !== 4) {
      switch (currContract.tariffCounterTypeCode) {
        case 0:
          amount = "";
          break;
        case 2:
          this.storeService.getTranslation(translation => {
            const _translation = translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.COMPONENTS.DISPLAY_DETAILS_OF_RAVKAV;
            amount = `${currContract.counterValue} ${_translation.RIDES}`;
          })
          break;
        case 3:
          amount = parseInt(currContract.counterValue) / 100;
          break;
      }
    }

    let contractPriceId = null;
    let price = null;
    let exactPrice = null;
    let originalContractValidityStartDate = null;

    const location9 = currContract.locationHashMap[9];
    if (location9) {
      const shareCode = location9.predefinedSharingCode;
      const profileType  = currContract.contractCustomerProfile;
      const ettCodeTens = currContract.tariffContractNameCode;
      const ettCodeUnits = location9.SpatialContractETTCode;
      const ettCode = parseInt(ettCodeTens.toString() + ettCodeUnits.toString());

      let relevanteContract = this.contractsFromServer ? this.contractsFromServer.filter(contract => contract.shareCode == shareCode)[0] : null;
      if (relevanteContract) {
        relevanteContract = relevanteContract.details.filter(contract => contract.ettCode == ettCode && contract.profileType == profileType)[0];
        if (relevanteContract) {
          contractName = relevanteContract.descrA;
          contractPriceId = relevanteContract.contractPriceId;
          originalContractValidityStartDate = relevanteContract.ValidityStartDate;

          price = relevanteContract.price;
          exactPrice = relevanteContract.exactPrice;

          if (!price) { //relevanteContract.price is empty
            price = 0;
          }
          if (!exactPrice) { //relevanteContract.exactPrice is empty
            exactPrice = 0;
          }
          price = parseFloat(price);
          exactPrice = parseFloat(exactPrice);
        }
      }
    }

    return {
      contractName: contractName,
      endDate: endDate,
      amount: amount,
      contractPriceId: contractPriceId,
      price: price,
      exactPrice: exactPrice,
      originalContractValidityStartDate: originalContractValidityStartDate
    }
  }

  initEvent(i: number) {
    let event = [];
    const currEvent = this.ravKavDetails.events[i];

    event.push(this.eventCircumstances[currEvent.eventCircumstances]);
    const eventContractPointer = currEvent.eventContractPointer - 1;
    const relavantContract = this.ravKavDetails.contracts[eventContractPointer];
    if (eventContractPointer != -1 && relavantContract) {
      event.push(this.getEttCode(relavantContract));
    }
    else {
      event.push(" ");
    }
    event.push(currEvent.eventPassengersNumber);
    event.push((currEvent.eventLine).toString().slice(-3));
    event.push(this.getOperator(currEvent.eventServiceProvider));
    event.push(this.datePipe.transform(currEvent.eventDateTimeStamp.split("T")[0], 'shortDate'));
    event.push(currEvent.eventDateTimeStamp.split("T")[1].slice(0, 5));

    return event;
  }

  isCancelable(i: number): boolean {
    if (!this.isContractProvideByThisOperator(i)) {
      return false;
    }
    else {
      if (this.isStoredValueContract(i)) {
        const lastEvent = this.ravKavDetails.events[0];
        const twoPreviousEvent = this.ravKavDetails.events[2];

        // if the last operation was loading this contract but no canceling this contract
        return this.eventIsLoadingSpecificContract(lastEvent, i) && !this.eventIsLoadingSpecificContract(twoPreviousEvent, i);
      }
      else {
        const lastEvent = this.ravKavDetails.events[0];
        return this.eventIsLoadingSpecificContract(lastEvent, i);
      }
    }
  }

  isRepurchasble(i: number): boolean {
    //if contract no recognized by operator - for example havn't price
    if (this.contracts[i]?.price == null) {
      return false;
    }
    // if adminCashbox can repurchase just operator contracts
    return !this.adminCashbox || (this.adminCashbox && this.isOperatorContract(i));
  }

  isOperatorContract(i: number): boolean {
    return this.contracts[i].profileType == this.storeService.operatorConfiguration.operatorProfile;
  }

  async repurchase(contract) {
    this.storeService.getTranslation(translation => {
      let loadContractStartDate;
      const freeMonthly = isFreeMonthly(contract);
      //after refoem
      if (!freeMonthly) {
        loadContractStartDate = this.getLoadContractStartDate(contract);
        // loadContractStartDate = serverConstant.general.NULL;
        this.sendCanLoadContractOnCard(contract, loadContractStartDate);
      }
      else { //freeMonthly
        const dialogRef = this.dialog.open(SelectMonthFreeMonthlyComponent,
          { direction: translation.DIRECTION });

        dialogRef.afterClosed().subscribe(selectedMonth => {
          if (selectedMonth) {
            loadContractStartDate = selectedMonth.valueOf().toString();
            this.sendCanLoadContractOnCard(contract, loadContractStartDate);
          }
        });
      }
    });
  }

  getLoadContractStartDate = (contract) => {
    const today = new Date().getTime();
    const contractStartDate = new Date(contract.originalContractValidityStartDate + " UTC").getTime();

    if (!contractStartDate || (today > contractStartDate)) {
      return today;
    }
    return contractStartDate;
  }

  sendCanLoadContractOnCard(contract, loadContractStartDate) {
    const data = {
      card: {
        contracts: [],
        counter: this.ravKavDetails.counter.counterOriginDump,
        environment: this.ravKavDetails.environment.originDump,
        events: [],
        specialEvents: []
      },
      contractPriceId: contract.contractPriceId,
      saleDevice: sessionStorage.getItem(sessionKeys.cashboxId),
      storedValueCount: (contract.ettCode == "65" ? "0" : serverConstant.general.NULL),
      loadContractStartDate: loadContractStartDate,
      isFreeMonthlySalesRestrictionStartEnd: "false"
    }
    this.initToPurchaseContract("contracts", data.card.contracts, "originDump");
    this.initToPurchaseContract("events", data.card.events, "eventOriginDump");
    this.initToPurchaseContract("specialEvents", data.card.specialEvents, "eventOriginDump");

    this.http
    .post<ServerResponse>(
     urls.CAN_LOAD_CONTRACT_ON_CARD,
    {
      data: data,
      sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
    })
    .subscribe(
      res => {
        switch(res.statusCode) {
          case 1:
            let contractData = {
              contract: contract,
              contractPriceId: contract.contractPriceId,
              ettCode: contract.ettCode,
              profileType: contract.profileType,
              price: contract.price,
              loadContractStartDate: loadContractStartDate
            }

            this.dialogRavKacService.from = From.DISPLAY_DETAILS;

            this.canLoadContract.emit(contractData);
            break;
          default:
            this.openCantLoadContractModal(res);
            break;
        }
      }
    )
  }

  cancelationContract(contract, i): void {
    const detailsContract = {
      ettCode: "" + this.ravKavDetails.contracts[i].tariffContractNameCode + this.ravKavDetails.contracts[i].locations[0].SpatialContractETTCode,
      shareCode: this.ravKavDetails.contracts[i].locations[0].predefinedSharingCode.toString(),
      profileType: this.ravKavDetails.contracts[i].contractCustomerProfile.toString(),
      contract: contract,
      saleDevice: this.ravKavDetails.contracts[i].contractSaleDevice
    }
    this.cancelContract.emit(detailsContract);
  }

  onClickLoadAnotherContract() {
    if (this.hasGoldkavFreeCertificateContract || this.haventSuitableProfile || this.adminAndOperatorAndGoldKav) {
      this.openCantLoadAnotherContractModal();
      return;
    }
    const confirmRoundingRules = sessionStorage.getItem(sessionKeys.confirmRoundingRules);
    if (!confirmRoundingRules) {
      this.showRoundingRulesMessage();
      return;
    }
    this.loadContractClicked.emit();
  }

  showRoundingRulesMessage = () => {
    this.storeService.getTranslation(translation => {
      this.infoModalIcon = "assets/icons/infoIcons/error.svg";
      this.infoModalTitle = translation.GENERAL.NOTE;
      this.infoModalSubTitle = [
        translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.COMPONENTS.DISPLAY_DETAILS_OF_RAVKAV.PAYMENT_DISCLAMER,
        "بالدفع النقدي، يعدل المبلغ بحسب قوانين بنك إسرائيل"
      ]
      this.infoModalHr = null;
      this.infoModalBtnTitle = null;
      this.infoModalBtnLeftText = translation.GENERAL.CONTINUE;
      this.infoModalBtnLeftClickFun = this.confirmationRoundingRules;
      this.infoModalBtnRightText = null;
      this.infoModalBtnRightClickFun = this.closeInfoModal;
      this.infoModalBackgroundClickFunction = this.closeInfoModal;

      this.modalService.open('info-modal-display-details');
    });
  }

  confirmationRoundingRules = () => {
    sessionStorage.setItem(sessionKeys.confirmRoundingRules, "true");
    this.loadContractClicked.emit();
  }

  onClickAddProfile() {
    if (this.isAnonymousCard) {
      this.openCantAddProfileToAnonymousModal();
    }
    else if (this.hasGoldkavProfile) {
      this.openCantAddProfileToGoldkavModal();
    }
    else {
      this.editProfileClicked.emit();
    }
  }

  openCantLoadContractModal(info) {
    this.storeService.getTranslation(translation => {
      const _translation = translation.GENERAL;
      this.infoModalIcon = "assets/icons/infoIcons/error.svg";
      this.infoModalTitle = _translation.CANT_LOAD_CONTRACT;
      if (info.data.errorStatus) {
        const translationPath = getErrorStatusTranslationPath(info.data.errorStatus);
        let errorStatusTranslation = translation;
        translationPath.forEach(key => {
          errorStatusTranslation = errorStatusTranslation[key];
        });
        this.infoModalSubTitle = [errorStatusTranslation];
      }
      else {
        this.infoModalSubTitle = [_translation.UNIDENTIFIED_ERROR];
      }
      if (info.description.includes("loading on this date does not valid")) {
        this.infoModalSubTitle = [_translation.INVALID_LOAD_DATE];
      }
      this.infoModalHr = false;
      this.infoModalBtnLeftText = _translation.BACK;
      this.infoModalBtnLeftClickFun = this.closeInfoModal;
      this.infoModalBackgroundClickFunction = this.closeInfoModal;

      this.modalService.open('info-modal-display-details');
    });
  }

  openCantAddProfileToAnonymousModal() {
    this.storeService.getTranslation(translation => {
      this.infoModalIcon = "assets/icons/infoIcons/error.svg";
      this.infoModalTitle = translation.GENERAL.CANT_ADD_PROFILE;

      this.infoModalSubTitle = [translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.COMPONENTS.DISPLAY_DETAILS_OF_RAVKAV.IS_ANONYMOUS_CARD];
      this.infoModalHr = false;
      this.infoModalBtnLeftText = translation.GENERAL.BACK;
      this.infoModalBtnLeftClickFun = this.closeInfoModal;
      this.infoModalBackgroundClickFunction = this.closeInfoModal;

      this.modalService.open('info-modal-display-details');
    });
  }

  openCantAddProfileToGoldkavModal() {
    this.storeService.getTranslation(translation => {
      this.infoModalIcon = "assets/icons/infoIcons/error.svg";
      this.infoModalTitle = translation.GENERAL.CANT_ADD_PROFILE;

      this.infoModalSubTitle = [translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.COMPONENTS.DISPLAY_DETAILS_OF_RAVKAV.HAS_ZAHAVKAV];
      this.infoModalHr = false;
      this.infoModalBtnLeftText = translation.GENERAL.BACK;
      this.infoModalBtnLeftClickFun = this.closeInfoModal;
      this.infoModalBackgroundClickFunction = this.closeInfoModal;

      this.modalService.open('info-modal-display-details');
    });
  }

  openCantLoadAnotherContractModal() {
    this.storeService.getTranslation(translation => {
      this.infoModalIcon = "assets/icons/infoIcons/error.svg";
      this.infoModalTitle = translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.COMPONENTS.DISPLAY_DETAILS_OF_RAVKAV.CANT_LOAD_ANOTHER_CONTRACT;

      if (this.hasGoldkavFreeCertificateContract) {
        this.infoModalSubTitle = [translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.COMPONENTS.DISPLAY_DETAILS_OF_RAVKAV.HAS_FC];
      } else if (this.haventSuitableProfile) {
        this.infoModalSubTitle = [
          translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.COMPONENTS.DISPLAY_DETAILS_OF_RAVKAV.ON_ADMIN_JUST_OPERATOR,
          translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.COMPONENTS.DISPLAY_DETAILS_OF_RAVKAV.HASNT_OPERATOR_PROFILE
        ];
      } else if (this.adminAndOperatorAndGoldKav) {
        this.infoModalSubTitle = [
          translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.COMPONENTS.DISPLAY_DETAILS_OF_RAVKAV.ON_ADMIN_JUST_OPERATOR,
          translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.COMPONENTS.DISPLAY_DETAILS_OF_RAVKAV.HAS_GOLDKAV_PROFILE
        ];
      }
      this.infoModalHr = false;
      this.infoModalBtnLeftText = translation.GENERAL.BACK;
      this.infoModalBtnLeftClickFun = this.closeInfoModal;
      this.infoModalBackgroundClickFunction = this.closeInfoModal;

      this.modalService.open('info-modal-display-details');
    });
  }

  closeInfoModal = () => {
    this.modalService.close('info-modal-display-details');
  }

  initToPurchaseContract(arrayElement, pushField, field) {
    this.ravKavDetails[arrayElement].forEach(element => {
      pushField.push(element !== null ? element[field] : "");
    });
  }

  noValid(date) {
    return new Date(date).setUTCHours(23,59,59,999) < new Date().getTime();
  }

  isHasGoldkavProfile() {
    return (this.ravKavDetails.environment.holderProfile1Code == GOLD_KAV_PROFILE && isAfterToday(this.ravKavDetails.environment.holderProfile1Date))
    || (this.ravKavDetails.environment.holderProfile2Code == GOLD_KAV_PROFILE && isAfterToday(this.ravKavDetails.environment.holderProfile2Date));
  }

  valueIsMoney(item) {
    if (item.key == 'amount' && !isNaN(item.value)) {
      return true;
    }
  }

  eventIsLoadingSpecificContract(event, contractIndex): boolean {
    return event.eventCircumstances == EventCircumstances.CONTRACT_LOADING &&
          event.eventContractPointer == contractIndex + 1;
  }

  isStoredValueContract(i: number): boolean {
    return this.ravKavDetails.contracts[i].tariffContractNameCode == TariffContractNameCode.STORED_VALUE;
  }

  isContractProvideByThisOperator(i: number): boolean {
    const contractProvider = this.ravKavDetails.contracts[i].contractProvider;
    return contractProvider == sessionStorage.getItem(sessionKeys.operatorId);
  }
}
