import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MainService } from 'src/app/pages/main/main.service';
import { AmountInputService, subData } from 'src/app/shared/components/amount-input/amount-input.service';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { ActionCode, cashboxDataType, ContractAction, getValue, ServerResponse, statusCode } from 'src/app/shared/constants/server';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';
import { urls } from 'src/app/shared/constants/URLs';
import { Cashbox } from 'src/app/shared/models/cashbox.model';
import { CashboxWithoutPapers } from 'src/app/shared/models/cashboxWithoutPapers.model';
import { StoreService } from 'src/app/shared/services/store.service';
import { getImageUrl } from 'src/app/shared/utils/cashbox';
import { DialogRavKacService } from '../../dialog-rav-kac.service';
import { PaymentWay } from '../payment-to-buy-contract/paymentWay';
import * as serverConstant from '../../../../../../shared/constants/server';

@Component({
  selector: 'app-cancelation-contract',
  templateUrl: './cancelation-contract.component.html',
  styleUrls: ['./cancelation-contract.component.css']
})
export class CancelationContractComponent implements OnInit, AfterViewInit {
  @Input() contractToCancelation;
  @Input() dataFromCard;

  moreThanMax = false;
  lessThanZero = false;
  intervalTime = 2500;

  currCashbox = this.storeService.getCashbox();
  paybackCashbox = new CashboxWithoutPapers();

  @ViewChild('cancelButton') cancelButton;

  getValue = getValue;
  getImageUrl = getImageUrl;

  infoModalBackgroundClickFunction;
  infoModalIcon;
  infoModalHr;
  infoModalTitle;
  infoModalSubTitle;
  infoModalBtnTitle;
  infoModalBtnRightText;
  infoModalBtnRightDisabled;
  infoModalBtnRightClickFun;
  infoModalBtnLeftText;
  infoModalBtnLeftDisabled;
  infoModalBtnLeftClickFun;
  infoModalError;

  NeedToOpenPaybackSub: Subscription;
  amountInputChangedSub: Subscription;
  writeFailedSub: Subscription;

  @Output() readyToCancelContract = new EventEmitter<any>();

  @ViewChild('paymentForm') paymentForm: NgForm;
  paymentWay = PaymentWay.CASH;
  PaymentWay = PaymentWay;

  constructor(
    public storeService: StoreService,
    private dialogRavKacService: DialogRavKacService,
    private mainService: MainService,
    private modalService: ModalService,
    private amountInputService: AmountInputService,
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    this.infoModalIcon = "assets/icons/infoIcons/warning.svg";

    this.writeFailedSub = this.mainService.writeFailed.subscribe(
      () => {
        this.closeInfoModal();
      }
    );

    this.NeedToOpenPaybackSub = this.mainService.NeedToOpenPayback.subscribe(
      () => {
        this.mainService.canCloseDiaolog = false;
        this.openPaybackModal();
        this.closeInfoModal();
      }
    );

    this.amountInputChangedSub = this.amountInputService.inputChanged.subscribe(
      (data: subData) => {
        if (data.info == 'payback') {
          switch(data.operation) {
            case 'increase':
              this.paybackCashbox[data.type]++;
              break;
            case 'decrease':
              this.paybackCashbox[data.type]--;
              break;
            case 'change':
              this.paybackCashbox[data.type] = data.amount;
              break;
          }
        }
        switch(data.operation) {
          case 'moreThanMax':
            this.paybackCashbox[data.type] = data.amount;;
            this.openPopUp('moreThanMax');
            break;
          case 'lessThanZero':
            this.openPopUp('lessThanZero');
            break;
        }
      }
    );
  }

  ngAfterViewInit (): void {
    this.openMainPaybackInfoModal();
  }

  openPopUp = (error) => {
    switch(error) {
      case 'moreThanMax':
        this.moreThanMax = true;

        setTimeout(() => {
        this.moreThanMax = false;
        }, this.intervalTime);
        break;
      case 'lessThanZero':
        this.lessThanZero = true;

        setTimeout(() => {
        this.lessThanZero = false;
        }, this.intervalTime);
        break;
    }
  }

  confirmCancelation = () => {
    this.storeService.getTranslation(translation => {
      this.infoModalBtnLeftText = translation.GENERAL.PLEASE_WAIT;
      this.infoModalBtnLeftDisabled = true;

      this.mainService.paybackNeeded = false;
      const price = this.getPrice();
      const data = this.initDataToSendForWS();

      this.http
      .post<ServerResponse>(
       urls.CAN_CANCEL_CONTRACT_ON_CARD,
      {
        data: data,
        sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
      })
      .subscribe(
        res => {
          if (res.statusCode == statusCode.OK) {
            if (price > 0) {
              this.mainService.paybackNeeded = true;
              // on credit or voucher cashbox now no relavent because can return credit or voucher
              if (!this.storeService.operatorConfiguration.supportCredit && !this.storeService.operatorConfiguration.supportVouchers && !this.hasMoneyForPayback(price)) {
                this.openNoPaybackInfoModal();
                return;
              }
            }
            this.getCancellationLoadContractDump();
          }
        }
      );
    });
  }

  getCancellationLoadContractDump() {
    const data = this.initDataToSendForWS();

    this.http
      .post<ServerResponse>(
       urls.GET_CANCELLATION_LOAD_CONTRACT,
      {
        data: data,
        sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
      })
      .subscribe(
        res => {
          if (res.statusCode == statusCode.OK) {
            const ettCode = parseInt(this.contractToCancelation.contract.ettCode);
            const typeTicket = Math.floor(ettCode/10);
            const cashboxData = new CashboxWithoutPapers().convertToServerFormat();
            const sharingCode = this.contractToCancelation.contract.shareCode;

            const data = {
              profileType: this.contractToCancelation.contract.profileType,
              typeTicket: typeTicket,
              cashboxes: null,
              cashboxData: cashboxData,
              paymentSum : this.getPrice(),
              serverData: res.data,
              actionCode: ActionCode.LOAD_OR_CANCEL_CONTRACT,
              contractAction: ContractAction.CANCEL,
              //new fileds : MAHAR
              ettCode: ettCode,
              sharingCode: sharingCode
            };

            this.mainService.cashboxData.next(data);

            this.readyToCancelContract.emit(data);
          }
        }
      );

  }

  initDataToSendForWS() {
    const startDateParts = this.contractToCancelation.contract.from.split("-");
    // month is 0-based, that's why need dataParts[1] - 1
    const loadContractStartDate = new Date(+startDateParts[2], startDateParts[1] - 1, +startDateParts[0]).getTime().toString();
    var query = {
      ettCode: this.contractToCancelation.ettCode,
      shareCode: this.contractToCancelation.shareCode,
      profileType: this.contractToCancelation.profileType,
      saleDevice: this.contractToCancelation.saleDevice.toString(),
      loadContractStartDate: loadContractStartDate,
      storedValueCount: (this.contractToCancelation.ettCode === "65" ? (parseFloat(this.contractToCancelation.contract.price)).toString() : serverConstant.general.NULL),
      card: {
        contracts: [],
        counter: this.dataFromCard.counter.counterOriginDump,
        environment: this.dataFromCard.environment.originDump,
        events: [],
        specialEvents: []
      }
    }

    this.initToPurchaseContract("contracts", query.card.contracts, "originDump")
    this.initToPurchaseContract("events", query.card.events, "eventOriginDump")
    this.initToPurchaseContract("specialEvents", query.card.specialEvents, "eventOriginDump")

    return query;
  }

  initToPurchaseContract(arrayElement, pushField, field) {
    this.dataFromCard[arrayElement].forEach(element => {
      pushField.push(element !== null ? element[field] : "")
    });
  }

  goBack = () => {
    this.dialogRavKacService.backToHome.next(null);
  }

  getPayback = () => {
    const price = this.getPrice();
    if (this.paymentWay == PaymentWay.CREDIT_OR_VOUCHER) {
      return price.toFixed(2);
    }
    else { //cash
      const givenPayback = this.paybackCashbox.getSum();
      const payback = price - givenPayback;
      return payback.toFixed(2);
    }
  }

  finishPayback = () => {
    if (this.paymentWay == PaymentWay.CASH) {
      this.mainService.comment = '';
    }
    else {
      this.mainService.comment = this.paymentForm.value.cardNum.toString();
    }

    this.mainService.finishWithPayback.next(
      this.getCashboxData()
    );
    this.mainService.canCloseDiaolog = true;
    this.modalService.close('payback-modal-cancel');
  }

  getCashboxData = () => {
    let cashboxData = new Cashbox();

    switch(this.paymentWay) {
      case PaymentWay.CASH:
        cashboxData.decrease(this.paybackCashbox);
        break;
      case PaymentWay.CREDIT_OR_VOUCHER:
        switch(this.paymentForm.value.paymentWay) {
          case 'credit':
            cashboxData[cashboxDataType.credit] = -1 * this.getPrice() * (100 / this.storeService.CreditValue);
            break;
          case 'voucher':
            cashboxData[cashboxDataType.studentVoucher] = -1;
            break;
        }
        break;
    }

    return cashboxData.convertToServerFormat();
  }

  hasMoneyForPayback = (remaindPayback) => {
    let currCashboxCopy = this.storeService.getCashbox();

    let coinType = 10; //200 nis
    while (coinType > 0) {
      const coinValue = getValue(coinType)/100;
      while (remaindPayback >= coinValue && currCashboxCopy[coinType] > 0) {
        remaindPayback -= coinValue;
        remaindPayback = parseFloat(remaindPayback.toFixed(2));
        currCashboxCopy[coinType]--;
      }
      if (remaindPayback == 0) {
        return true;
      }
      coinType--;
    }
    return false;
  }

  openPaybackModal = () => {
    this.modalService.open('payback-modal-cancel');
  }

  openMainPaybackInfoModal = () => {
    this.storeService.getTranslation(translation => {
      this.infoModalTitle = translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.COMPONENTS.CANCELATION_CONTRACT.SURE_CANCEL;
      this.infoModalSubTitle = null;
      this.infoModalBtnTitle = null;
      this.infoModalHr = null;
      this.infoModalBtnLeftText = translation.GENERAL.YES;
      this.infoModalBtnLeftClickFun = this.confirmCancelation;
      this.infoModalBtnRightText = translation.GENERAL.NO;
      this.infoModalBtnRightClickFun = this.goBack;
      this.infoModalBackgroundClickFunction = this.goBack;

      this.modalService.open('info-modal-payback-cancel');
    });
  }

  openNoPaybackInfoModal = () => {
    this.storeService.getTranslation(translation => {
      this.infoModalTitle = translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.COMPONENTS.CANCELATION_CONTRACT.NO_MONEY_TO_RETURN;
      this.infoModalSubTitle = null;
      this.infoModalBtnTitle = null;
      this.infoModalHr = null;
      this.infoModalBtnRightText = null;
      this.infoModalBtnRightClickFun = null;
      this.infoModalBtnLeftText = translation.GENERAL.OK;
      this.infoModalBtnLeftDisabled = null;
      this.infoModalBtnLeftClickFun = this.closeInfoModalAndGoBack;
      this.infoModalBackgroundClickFunction = this.closeInfoModalAndGoBack;

      this.modalService.open('info-modal-payback-cancel');
    });
  }

  closeInfoModal = () => {
    this.modalService.close('info-modal-payback-cancel');
  }

  closeInfoModalAndGoBack = () => {
    this.closeInfoModal();
    this.goBack();
  }

  getPrice = () => {
    if (this.paymentWay == PaymentWay.CASH) {
      const price = this.contractToCancelation.contract.price;
      return parseFloat(price);
    }
    else { //credit
      const exactPrice = this.contractToCancelation.contract.exactPrice;
      return parseFloat(exactPrice);
    }
  }

  selectedTabChange(event) {
    if (event.index == 0) {
      this.paymentWay = PaymentWay.CASH;
    }
    else {
      this.paymentWay = PaymentWay.CREDIT_OR_VOUCHER;
      this.paybackCashbox = new CashboxWithoutPapers();
    }
  }

  disabledFinishPayback = () => {
    const flag = (this.paymentWay == PaymentWay.CASH && parseFloat(this.getPayback()) != 0)  ||
    (this.paymentWay == PaymentWay.CREDIT_OR_VOUCHER && !this.paymentForm.valid);

    return flag;
  }

  ngOnDestroy() {
    this.amountInputChangedSub.unsubscribe();
    this.NeedToOpenPaybackSub.unsubscribe();
  }
}
