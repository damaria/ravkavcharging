import { Component, OnInit, Input, Output, EventEmitter, OnDestroy, ViewChild } from '@angular/core';
import { StoreService } from 'src/app/shared/services/store.service';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { LoginService } from 'src/app/pages/login/login.service';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { ActionCode, cashboxDataType, ContractAction, getValue, ServerResponse, statusCode } from 'src/app/shared/constants/server';
import { getImageUrl } from 'src/app/shared/utils/cashbox';
import { CashboxWithoutPapers } from 'src/app/shared/models/cashboxWithoutPapers.model';
import { AmountInputService, subData } from 'src/app/shared/components/amount-input/amount-input.service';
import { Subscription } from 'rxjs';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';
import { MainService } from 'src/app/pages/main/main.service';
import * as serverConstant from '../../../../../../shared/constants/server';
import { DisplayMoneyService, TextChangedStatus } from 'src/app/shared/components/display-money/display-money.service';
import { isFreeMonthly } from 'src/app/shared/utils/date';
import { MatButton } from '@angular/material/button';
import { PaymentWay } from './paymentWay';
import { Cashbox } from 'src/app/shared/models/cashbox.model';
import { MatDialog } from '@angular/material/dialog';
import * as moment from 'moment';
import { PaymentToBuyContractService } from './payment-to-buy-contract.service';
import { urls } from 'src/app/shared/constants/URLs';
import { HttpClient } from '@angular/common/http';
import { writeToLogger } from 'src/app/shared/constants/webSocketFunc';
import { ErrorModalService } from 'src/app/shared/components/modal/error-modal/error-modal-service.service';

@Component({
  selector: 'app-payment-to-buy-contract',
  templateUrl: './payment-to-buy-contract.component.html',
  styleUrls: ['./payment-to-buy-contract.component.css']
})
export class PaymentToBuyContractComponent implements OnInit, OnDestroy {
  onAdd = new EventEmitter();
  minDate = new Date(new Date().setHours(0, 0, 0, 0));
  maxDate =  new Date(new Date("12/31/2040").setHours(0, 0, 0, 0));

  @Output() contractDumpArrived = new EventEmitter<any>();

  @Input() contractDetails;
  @Input() dataFromCard;

  infoModalBackgroundClickFunction;
  infoModalIcon;
  infoModalHr;
  infoModalTitle;
  infoModalSubTitle;
  infoModalBtnTitle;
  infoModalBtnRightText;
  infoModalBtnLeftText;
  infoModalBtnRightClickFun;
  infoModalBtnLeftClickFun;
  infoModalError;

  amountInputChangedSub: Subscription;

  currCashbox = new CashboxWithoutPapers();
  paymentCashbox = new CashboxWithoutPapers();
  paybackCashbox = new CashboxWithoutPapers();

  moreThanMax = false;
  lessThanZero = false;
  intervalTime = 2500;

  dataToBuyContract: FormGroup = new FormGroup({});

  getValue = getValue;
  getImageUrl = getImageUrl;
  isFreeMonthly = isFreeMonthly;
  parseFloat = parseFloat;
  NeedToOpenPaybackSub: Subscription;
  paymentWay: PaymentWay;

  @ViewChild('paymentForm') paymentForm: NgForm;
  @ViewChild('submitButton') submitButton: MatButton;

  changeableLabel: string;

  constructor(
    public storeService: StoreService,
    private loginService: LoginService,
    private mainService: MainService,
    private modalService: ModalService,
    private amountInputService: AmountInputService,
    private displayMoneyService: DisplayMoneyService,
    public dialog: MatDialog,
    private paymentToBuyContractService: PaymentToBuyContractService,
    private http: HttpClient,
    private errorModalService: ErrorModalService
    ) { }

  get f() { return this.dataToBuyContract.controls; }

  async ngOnInit() {
    this.storeService.getTranslation(translation => {
      this.changeableLabel = translation.GENERAL.CHANGEABLE;

      if (this.contractDetails.canSkip) {
        this.mainService.paybackNeeded = false;
        this.purchaseContractWithCash(new CashboxWithoutPapers());
      }

      if (this.isStoredValue() && !this.isVariableStoredValue()) {
        this.getStoredValueByDiscount();
      }
      if (this.isVariableStoredValue()) {
        this.dataToBuyContract.addControl('variableStoredValue', new FormControl('', Validators.required));
      }
      if (this.needToSelectDate()) {
        this.dataToBuyContract.addControl('freedom', new FormControl(moment(), Validators.required));
      }

      this.amountInputChangedSub = this.amountInputService.inputChanged.subscribe(
        (data: subData) => {
          if (data.info == 'payback') {
            switch(data.operation) {
              case 'increase':
                this.paybackCashbox[data.type]++;
                break;
              case 'decrease':
                this.paybackCashbox[data.type]--;
                break;
              case 'change':
                this.paybackCashbox[data.type] = data.amount;
                break;
            }
          }
          switch(data.operation) {
            case 'moreThanMax':
              this.paybackCashbox[data.type] = data.amount;
              this.openPopUp('moreThanMax');
              break;
            case 'lessThanZero':
              this.openPopUp('lessThanZero');
              break;
          }
        }
      );

      this.NeedToOpenPaybackSub = this.mainService.NeedToOpenPayback.subscribe(
        () => {
          this.mainService.canCloseDiaolog = false;
          this.openPaybackModal();
        }
      );
    });
  }

  openPopUp(error) {
    switch(error) {
      case 'moreThanMax':
        this.moreThanMax = true;

        setTimeout(() => {
        this.moreThanMax = false;
        }, this.intervalTime);
        break;
      case 'lessThanZero':
        this.lessThanZero = true;

        setTimeout(() => {
        this.lessThanZero = false;
        }, this.intervalTime);
        break;
    }
  }

  isStoredValue(): boolean {
    return Number(this.contractDetails.ettCode) >= 60 && Number(this.contractDetails.ettCode) <= 67;
  }

  isVariableStoredValue(): boolean {
    return Number(this.contractDetails.ettCode) == 65;
  }

  needToSelectDate(): boolean {
    return (Number(this.contractDetails.ettCode) > 20 && Number(this.contractDetails.ettCode) <= 22);
  }

  isFreeWeekly() {
    return Number(this.contractDetails.ettCode) == 21;
  }

  getStoredValueByDiscount(isVariableStoredValue = false) {
    const query = {
      ettCode: this.contractDetails.ettCode,
      profileType: this.contractDetails.profileType,
      storedValueCount: isVariableStoredValue ? this.f.variableStoredValue.value.toString() : serverConstant.general.NULL
    }
    this.http.post<ServerResponse>(
      urls.GET_STORED_VALUE_BY_DISCOUNT,
      {
        data: query,
        sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
      })
      .subscribe(res => {
        const storedValue = (res.data.storedValueByDiscount) / 100;
        if (isVariableStoredValue) {
          const contractPrice = parseFloat(this.f.variableStoredValue.value);
          this.contractDetails.contract.price = contractPrice;
          this.contractDetails.price = contractPrice;
        }
        this.contractDetails.contract.loadedSum = storedValue;
      })
  }

  initToPurchaseContract(arrayElement, pushField, field) {
    this.dataFromCard[arrayElement].forEach(element => {
      pushField.push(element !== null ? element[field] : "");
    });
  }

  purchaseContract = (payment) => {
    this.mainService.paybackNeeded = false;

    if (payment instanceof CashboxWithoutPapers) {
      this.paymentWay = PaymentWay.CASH;
      this.purchaseContractWithCash(payment);
    }
    else {
      this.paymentWay = PaymentWay.CREDIT_OR_VOUCHER;
      this.purchaseContractWithCreditOrVoucher();
    }
  }

  purchaseContractWithCash(paymentCashbox) {
    this.mainService.comment = '';

    this.paymentCashbox = paymentCashbox;
    const payback = this.getPayback();

    if (parseFloat(payback) < 0) {
      this.openNoEnoughMoneyModal();
      return;
    }
    else if (parseFloat(payback) > 0) {
      this.mainService.paybackNeeded = true;
      this.currCashbox = this.storeService.getCashbox();
      this.currCashbox.increase(paymentCashbox);
      if (!this.hasMoneyForPayback(payback)) {
        this.openNoPaybackModal();
        return;
      }
    }

    this.displayMoneyService.textChanged.next(TextChangedStatus.CONTINUE);

    this.loginService.checkLogin().subscribe(
      connectStatus => {
        if (connectStatus == true) {
          this.getLoadContractDump();
        }
      }
    );

    this.closeInfoModal();
  }

  purchaseContractWithCreditOrVoucher = () => {
    this.mainService.comment = this.paymentForm.value.cardNum.toString();

    this.storeService.getTranslation(translation => {
      this.submitButton._elementRef.nativeElement.textContent = translation.GENERAL.PLEASE_WAIT;
      this.submitButton._elementRef.nativeElement.disabled = true;
      this.submitButton._elementRef.nativeElement.style.cursor = "not-allowed";

      this.loginService.checkLogin().subscribe(
        connectStatus => {
          if (connectStatus == true) {
            this.getLoadContractDump();
          }
        }
      );
    });
  }

  getLoadContractDump = ()=> {
    this.storeService.getTranslation(async translation => {
      const query = {
        card: {
          environment: this.dataFromCard.environment.originDump,
          specialEvents: [],
          counter: this.dataFromCard.counter.counterOriginDump,
          contracts: [],
          events: []
        },
        saleDevice: sessionStorage.getItem(sessionKeys.cashboxId),
        storedValueCount: (this.contractDetails.ettCode === "65" ? (parseFloat(this.contractDetails.price)).toString() : serverConstant.general.NULL),
        loadContractStartDate: this.getLoadContractStartDate(),
        isFreeMonthlySalesRestrictionStartEnd: "false",
      }

      if (typeof this.contractDetails.contractPriceId === 'string') {
        query["contractPriceId"] = this.contractDetails.contractPriceId;
      }
      else {
        query["ettCode"] = this.contractDetails.ettCode;
        query["shareCode"] = this.contractDetails.shareCode;
        query["profileType"] = this.contractDetails.profileType;
      }
      this.initToPurchaseContract("contracts", query.card.contracts, "originDump");
      this.initToPurchaseContract("events", query.card.events, "eventOriginDump");
      this.initToPurchaseContract("specialEvents", query.card.specialEvents, "eventOriginDump");

      this.http.post<ServerResponse>(
      urls.GET_LOAD_CONTRACT_DUMP,
      {
        data: query,
        sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
      })
      .subscribe(res => {
        //sent to the card reader logger the response of get_load_contract_dump
        writeToLogger(
          `Received from ${urls.GET_LOAD_CONTRACT_DUMP} ` +
          `${JSON.stringify(res)}}`
        );

        if (res.statusCode == statusCode.OK) {
          const ettCode = parseInt(this.contractDetails.ettCode);
          const typeTicket = Math.floor(ettCode/10);
          const paymentSum = this.getPrice();
          const cashboxData = this.getCashboxData();
          const sharingCode = this.contractDetails.contract.shareCode;

          const data = {
            profileType: this.contractDetails.profileType,
            typeTicket: typeTicket,
            cashboxData: cashboxData,
            paymentSum: paymentSum,
            serverData: res.data,
            actionCode: ActionCode.LOAD_OR_CANCEL_CONTRACT,
            contractAction: ContractAction.LOAD,
            //new fileds : MAHAR
            ettCode: ettCode,
            sharingCode: sharingCode
          };

          this.mainService.cashboxData.next(data);

          this.contractDumpArrived.emit(data);
        }
        else {
          this.submitButton._elementRef.nativeElement.textContent = translation.GENERAL.PURCHASE;
          this.submitButton._elementRef.nativeElement.disabled = false;
          this.submitButton._elementRef.nativeElement.style.cursor = "pointer";

          this.displayMoneyService.textChanged.next(TextChangedStatus.ERROR);
        }
      })
    })
  }

  getLoadContractStartDate = () => {
    if (isFreeMonthly(this.contractDetails)) {
      return this.contractDetails.loadContractStartDate; // need to understand, on stored value is "-999"
    }
    else {
      return this.f.freedom && this.f.freedom.value ? this.f.freedom.value.valueOf().toString() : this.contractDetails.loadContractStartDate;
    }
  }

  selectedTabChange(event) {
    if (event.index == 0) {
      this.paymentWay = PaymentWay.CASH;
    }
    else {
      this.paymentWay = PaymentWay.CREDIT_OR_VOUCHER;
    }
    this.paymentToBuyContractService.paymentWayChanged.next(this.paymentWay);
  }

  getCashboxData() {
    switch(this.paymentWay) {
      case PaymentWay.CASH:
        let helperCashbox = new CashboxWithoutPapers();
        helperCashbox.increase(this.paymentCashbox);
        helperCashbox.decrease(this.paybackCashbox);
        return helperCashbox.convertToServerFormat();

      case PaymentWay.CREDIT_OR_VOUCHER:
        let helperCashbox1 = new Cashbox();
        switch(this.paymentForm.value.paymentWay) {
          case 'credit':
            helperCashbox1[cashboxDataType.credit] = this.getPrice() * (100 / this.storeService.CreditValue);
            return helperCashbox1.convertToServerFormat();
          case 'voucher':
            helperCashbox1[cashboxDataType.studentVoucher] = 1;
            return helperCashbox1.convertToServerFormat();
        }
      default: //no need to pay - free contract
        return [{cashboxDataTypeID: "1", cashboxCountCount: "0"}];
    }
  }

  openNoEnoughMoneyModal() {
    this.storeService.getTranslation(translation => {
      const _translation = translation.GENERAL;
      this.infoModalIcon = "assets/icons/infoIcons/error.svg";
      this.infoModalTitle = _translation.NOT_ALL_BROUGHT;
      this.infoModalSubTitle = null;
      this.infoModalHr = false;
      this.infoModalBtnLeftText = _translation.RETURN_TO_COMPLETE;
      this.infoModalBtnLeftClickFun = this.closeInfoModal;
      this.infoModalBackgroundClickFunction = this.closeInfoModal;

      this.modalService.open('info-modal-payback');
    });
  }

  closeInfoModal = () => {
    this.modalService.close('info-modal-payback');
  }

  hasMoneyForPayback = (remaindPayback) => {
    let currCashboxCopy = this.storeService.getCashbox();

    let coinType = 10; //200 nis
    while (coinType > 0) {
      const coinValue = getValue(coinType)/100;
      while (remaindPayback >= coinValue && currCashboxCopy[coinType] > 0) {
        remaindPayback -= coinValue;
        remaindPayback = parseFloat(remaindPayback.toFixed(2));
        currCashboxCopy[coinType]--;
      }
      if (remaindPayback == 0) {
        return true;
      }
      coinType--;
    }
    return false;
  }

  openNoPaybackModal() {
    this.storeService.getTranslation(translation => {
      const _translation = translation.GENERAL;
      this.infoModalIcon = "assets/icons/infoIcons/error.svg";
      this.infoModalTitle = _translation.NO_EXCESS;
      this.infoModalSubTitle = null;
      this.infoModalHr = null;
      this.infoModalBtnTitle = null;
      this.infoModalBtnLeftText = _translation.OK;
      this.infoModalBtnLeftClickFun = this.closeInfoModal;
      this.infoModalBtnRightText = null;
      this.infoModalBtnRightClickFun = null;
      this.infoModalBackgroundClickFunction = this.closeInfoModal;

      this.modalService.open('info-modal-payback');
    });
  }

  openPaybackModal() {
    this.modalService.open('payback-modal-purchase');
  }

  closePaybackModal() {
    this.modalService.close('payback-modal-purchase');
  }

  getPayback() {
    const alreadyPayed = this.paymentCashbox.getSum();
    const needToPay = parseFloat(this.contractDetails.price);
    const givenPayback = this.paybackCashbox.getSum();
    const payback = alreadyPayed - needToPay - givenPayback;
    return payback.toFixed(2);
  }

  finishPayback() {
    this.mainService.finishWithPayback.next(
      this.getCashboxData()
    );
    this.mainService.canCloseDiaolog = true;
    this.modalService.close('payback-modal-purchase');
  }

  getPrice() {
    if (this.paymentWay == PaymentWay.CASH) {
      return parseFloat(this.contractDetails.contract.price);
    }
    else { //credit
      return parseFloat(this.contractDetails.contract.exactPrice);
    }
  }

  isCantPurchaseContract() {
    return this.needToSelectDate() && (this.f.freedom.value.valueOf() > this.maxDate.getTime() || this.f.freedom.value.valueOf() < this.minDate.getTime());
  }

  ngOnDestroy() {
    this.amountInputChangedSub.unsubscribe();
    this.NeedToOpenPaybackSub.unsubscribe();
  }
}
