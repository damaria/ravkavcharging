export enum PaymentWay {
    CASH,
    CREDIT_OR_VOUCHER
}

export enum FinePaymentWay {
    CREDIT = "1",
    CASH = "2"
}