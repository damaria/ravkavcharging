import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { PaymentWay } from "./paymentWay";

@Injectable({providedIn: "root"})
export class PaymentToBuyContractService {
    paymentWayChanged = new BehaviorSubject<PaymentWay>(PaymentWay.CASH);
}