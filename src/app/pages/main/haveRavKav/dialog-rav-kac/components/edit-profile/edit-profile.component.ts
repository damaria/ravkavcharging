import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { MainService } from 'src/app/pages/main/main.service';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { ActionCode, ServerResponse, statusCode } from 'src/app/shared/constants/server';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';
import { urls } from 'src/app/shared/constants/URLs';
import { ExperationDateOfOperatorProfile } from 'src/app/shared/fromServer/operatorsConfiguration';
import { StoreService } from 'src/app/shared/services/store.service';
import { getProfileToOverride } from 'src/app/shared/utils/ravkav';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
  @Input() dataFromCard;
  @Output() readyToEditProfile = new EventEmitter<any>();

  minDate = new Date();
  maxDate;
  profiles = [];
  operatorProfile;
  operatorProfileDescr;
  dataToServer;
  profileToOverride;

  infoModalBackgroundClickFunction;
  infoModalIcon;
  infoModalHr;
  infoModalSubTitle;
  infoModalBtnTitle;
  infoModalBtnRightText;
  infoModalBtnRightClickFun;
  infoModalBtnLeftClickFun;
  infoModalError;

  addProfileForm: FormGroup = new FormGroup({
    profileToLoad: new FormControl(null, Validators.required),
    endDateProfile: new FormControl(null, Validators.required)
  });

  needSendToServerAddingProfile;

  UNIDENTIFIED;

  constructor(
    private storeService: StoreService,
    private http: HttpClient,
    private mainService: MainService,
    private modalService: ModalService
    ) { }

  ngOnInit(): void {
    this.storeService.getTranslation(translation => {
      this.UNIDENTIFIED = translation.GENERAL.UNIDENTIFIED;
      this.needSendToServerAddingProfile = this.storeService.operatorConfiguration.needSendToServerAddingProfile;

      // this.maxDate = new Date(this.dataFromCard.environment.envendDate); // OLD
      const experationDateOfOperatorProfile: ExperationDateOfOperatorProfile = this.storeService.operatorConfiguration.experationDateOfOperatorProfile;
      if (experationDateOfOperatorProfile == ExperationDateOfOperatorProfile.ONE_YEAR_FROM_NOW) {
        this.maxDate = moment().add(1, 'year');
      }
      else { //ExperationDateOfOperatorProfile.END_OF_YEAR
        this.maxDate = moment().endOf('year');

        const currYear = moment().year();
        if (moment().isAfter(`${currYear}-12-10`)) {
          this.maxDate.add(1, 'year');
        }
      }

      this.operatorProfile = this.storeService.operatorConfiguration.operatorProfile.toString();
      this.operatorProfileDescr = this.getProfileDescr(this.operatorProfile);
      this.initAutoComplite();

      this.infoModalIcon = "assets/icons/infoIcons/warning.svg";
      this.infoModalHr = false;
      this.infoModalBtnLeftClickFun = this.closeInfoModal;
      this.infoModalBackgroundClickFunction = this.closeInfoModal;

      if (this.needSendToServerAddingProfile) {
        //add relevant controls
        this.addProfileForm.addControl('idNumber', new FormControl(null, Validators.required));
        this.addProfileForm.addControl('pinCode', new FormControl(null, Validators.required));
      }
    });
  }

  initData() {
    const environment = this.dataFromCard.environment;

    const data = {
      saleDevice: sessionStorage.getItem(sessionKeys.cashboxId),
      envCountryID: environment.envCountryID.toString(),
      envIssuerID: environment.envIssuerID.toString(),
      envApplicationNumber: environment.envApplicationNumber.toString(),
      envIssuingDate: new Date(environment.envIssuingDate).getTime().toString(),
      envendDate: new Date(environment.envendDate).getTime().toString(),
      paymentMethod: environment.paymentMethod.toString(),
      holderBirthDate: new Date(environment.holderBirthDate).getTime().toString(),
      passportForeign: environment.passportForeign ? environment.passportForeign : "0", // from enviroment?
      holderCompanyidCode: environment.holderCompanyidCode.toString(),
      holdersNationalid: environment.holdersNationalid.toString(),
      holderProfile1Code: environment.holderProfile1Code.toString(),
      holderProfile1Date: new Date(environment.holderProfile1Date).getTime().toString(),
      holderProfile2Code: environment.holderProfile2Code.toString(),
      holderProfile2Date: new Date(environment.holderProfile2Date).getTime().toString(),
      actionCode: ActionCode.ADD_PERSONALIZATION,
      card: {
        contracts: [],
        counter: this.dataFromCard.counter.counterOriginDump,
        environment: this.dataFromCard.environment.originDump,
        events: [],
        specialEvents: []
      }
    }

    const values = this.addProfileForm.value;

    data["holderProfile" + this.profileToOverride + "Code"] = values.profileToLoad;
    data["holderProfile" + this.profileToOverride + "Date"] = values.endDateProfile.endOf('day').valueOf().toString();

    this.initToPurchaseContract("contracts", data.card.contracts, "originDump")
    this.initToPurchaseContract("events", data.card.events, "eventOriginDump")
    this.initToPurchaseContract("specialEvents", data.card.specialEvents, "eventOriginDump")

    this.dataToServer = data;
  }

  initToPurchaseContract(arrayElement, pushField, field) {
    this.dataFromCard[arrayElement].forEach(element => {
      pushField.push(element !== null ? element[field] : "")
    });
  }

  initAutoComplite() {
    this.profiles = [
      { id: this.operatorProfile, name:  this.operatorProfileDescr }
    ]
  }

  getProfileDescr(profileId): string {
    let shouldSkip = false;
    let profileDesc: string;
    const profilesList = JSON.parse(sessionStorage.getItem(sessionKeys.serverProfiles));
    profilesList.forEach(element => {
      if (shouldSkip) {
        return;
      }
      if (element.Passenger_Profile_id == profileId) {
        profileDesc = element.Descr_HE;
        shouldSkip = true;
      }
    });
    return profileDesc ? profileDesc : `${this.UNIDENTIFIED} (${profileId})`;
  }

  getLoadPersonalizationDump = () => {
    this.initData();
    this.storeService.getTranslation(translation => {
      this.http
      .post<ServerResponse>(
        urls.GET_LOAD_PERSONALIZATION_DUMP,
        {
          data: this.dataToServer,
          sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
        }
      ).subscribe(
        res => {
          if (res.statusCode == statusCode.OK) {
            const data = res.data;
            this.mainService.cashboxData.next(this.dataToServer);
            this.readyToEditProfile.emit(data);
          }
        }
      );
    });
  }

  sendAddFreeEmployeeCard = () => {
    const formValues = this.addProfileForm.value;
    return this.http
    .post<ServerResponse>(
      urls.ADD_FREE_EMPLOYEE_CARD,
      {
        data: {
          idNumber: formValues.idNumber.toString(),
          pinCode: formValues.pinCode.toString(),
          cardSerial: this.dataFromCard.cardSerialNumber,
          withDentification: true
        },
        sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
      }
    );
  }

  onAddProfile() {
    this.storeService.getTranslation(translation => {
      const profileToLoad = this.addProfileForm.value.profileToLoad;
      const environment = this.dataFromCard.environment;
      this.profileToOverride = getProfileToOverride(environment, profileToLoad);

      if (this.profileToOverride) {
        if (this.needSendToServerAddingProfile) {
          this.sendAddFreeEmployeeCard()
          .subscribe(res => {
            if (res.statusCode == statusCode.OK) {
              this.getLoadPersonalizationDump();
            }
          })
        }
        else {
          this.getLoadPersonalizationDump();
        }
      }
      else {
        this.openCantAddProfileNoServerDataModal(translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.COMPONENTS.EDIT_PROFILE.BOTH_PROFILES_VALID);
      }
    });
  }

  openCantAddProfileNoServerDataModal(error) {
    this.infoModalIcon = "assets/icons/infoIcons/error.svg";
    this.infoModalSubTitle = [error];
    this.modalService.open('info-modal-edit-profile');
  }

  closeInfoModal = () => {
    this.modalService.close('info-modal-edit-profile');
  }
}
