import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { StoreService } from 'src/app/shared/services/store.service';
import { FormControl, FormGroup } from '@angular/forms';
import { ContractsService } from 'src/app/shared/services/contracts.service';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';
import * as serverConstant from '../../../../../../shared/constants/server';
import { From } from 'src/app/shared/constants/driverBox';
import { DialogRavKacService } from '../../dialog-rav-kac.service';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { isFreeMonthly, isFreeRide } from 'src/app/shared/utils/date';
import { isAnonymousCard } from 'src/app/shared/utils/ravkav';
import { getErrorStatusTranslationPath } from 'src/app/shared/utils/server';
import { HttpClient } from '@angular/common/http';
import { ServerResponse } from '../../../../../../shared/constants/server';
import { urls } from 'src/app/shared/constants/URLs';
import { MatDialog } from '@angular/material/dialog';
import { SelectMonthFreeMonthlyComponent } from 'src/app/shared/components/dialogs/select-month-free-monthly/select-month-free-monthly.component';
import { GOLD_KAV_PROFILE, STUDENT_PROFILE } from 'src/app/shared/constants/ravKav';
import { TranslateService } from '@ngx-translate/core';
import { SelectExpirationFreeRideComponent } from 'src/app/shared/components/dialogs/select-experation-free-ride/select-expiration-free-ride.component';
// import { MapsAPILoader } from '@agm/core';

@Component({
  selector: 'app-display-contract',
  templateUrl: './display-contract.component.html',
  styleUrls: ['./display-contract.component.css']
})
export class DisplayContractComponent implements OnInit, OnDestroy {
  LAT_LOCATION = 31.771959;
  LNG_LOCATION = 35.217018;
  ZOOM = 8;

  contractsToLoading = [];
  titleContracts;
  dataRecords = {
    profilesWS: JSON.parse(sessionStorage.getItem(sessionKeys.serverProfiles)),
    ettCodesWS: JSON.parse(sessionStorage.getItem(sessionKeys.serverEttCodes))
  };
  cntOfContract: number = 0;
  profiles = [];
  contracts = [];
  areas = [];
  profilesOnCard = [];
  dataOfContract;
  dataFromCard;
  MOTZone;
  currentReformZones = [];
  isChooseContract: string;
  profilesOnCardDesc = [];
  nameOfSelectedArea: string = "";
  searchText: string = '';
  // private geoCoder;
  divisionByRegions = {};
  haveGoldKavProfile: boolean;

  filtering: FormGroup = new FormGroup({
    'profile': new FormControl(""),
    'contract': new FormControl(""),
    'area': new FormControl(""),
    'search': new FormControl(""),
  });

  @Input('dataFromCard')
  set getDataRecords(dataFromCard) {
    this.dataFromCard = dataFromCard;
    let i = 1;
    while (this.dataFromCard.environment["holderProfile" + i + "Code"] != undefined && this.dataFromCard.environment["holderProfile" + i + "Date"] != undefined) {
      const profileDate = new Date(this.dataFromCard.environment["holderProfile" + i + "Date"]).setHours(23,59,59,999);
      if (profileDate > new Date().getTime()) {
        this.profilesOnCard.push(this.dataFromCard.environment["holderProfile" + i + "Code"].toString());
      }
      i += 1;
    }
    if (this.profilesOnCard[0] == this.profilesOnCard[1]) {
      this.profilesOnCard.pop();
    }
    if (!this.profilesOnCard.includes("0")) {
      this.profilesOnCard.push("0");
    }
    if (this.profilesOnCard.includes("1")) {
      this.profilesOnCard = this.profilesOnCard.filter(profile => profile != 0);
    }

    this.haveGoldKavProfile = this.profilesOnCard.includes(GOLD_KAV_PROFILE);
  }

  @Output() canLoadContract = new EventEmitter<any>();

  infoModalBackgroundClickFunction;
  infoModalIcon;
  infoModalHr;
  infoModalTitle;
  infoModalSubTitle;
  infoModalBtnTitle;
  infoModalBtnRightText;
  infoModalBtnLeftText;
  infoModalBtnRightClickFun;
  infoModalBtnLeftClickFun;
  infoModalError;

  contractData;
  @ViewChild('contractsTable') contractsTable: ElementRef;
  isLoading = new Array();

  PRICE;
  SHARING_CODE;
  OPERATOR_PROFILE;
  adminCashbox;

  constructor(
    private storeService: StoreService,
    private contractsService: ContractsService,
    private dialogRavKacService: DialogRavKacService,
    private modalService: ModalService,
    private http: HttpClient,
    public dialog: MatDialog,
    private translateService: TranslateService
  ) { }

  ngOnInit() {
    this.OPERATOR_PROFILE = this.storeService.operatorConfiguration.operatorProfile.toString();
    this.adminCashbox = JSON.parse(sessionStorage.getItem(sessionKeys.adminCashbox));
    this.storeService.getTranslation(translation => {
      const _translation = translation.GENERAL;
      this.divisionByRegions[_translation.AREAS.HAIFA] = {
        areas: [
          334, 322, 310, 333, 601, 701,
          602, 502, 332, 321, 331, 501
        ],
        location: {
          lat: 32.68,
          lng: 35.4
        }
      }
      this.divisionByRegions[_translation.AREAS.JERUSALEM] = {
        areas: [
          134, 131, 123, 121,
          110, 122, 135, 234, 232
        ],
        location: {
          lat: 31.75,
          lng: 35.2
        }
      };
      this.divisionByRegions[_translation.AREAS.BEER_SHEVA] = {
        areas: [
          901, 423, 410, 902, 903, 432, 433,
          422, 431, 421, 434, 1333, 801, 802
        ],
        location: {
          lat: 30.7,
          lng: 34.88
        }
      };
      this.divisionByRegions[_translation.AREAS.TLV_AREA] = {
        areas: [
          233, 223, 210, 222,
          221, 702, 231
        ],
        location: {
          lat: 32.165,
          lng: 34.87
        }
      };

      this.titleContracts =
      [
        _translation.CONTRACT_KIND,
        _translation.DESCR,
        _translation.PROFILE,
        _translation.SHARING_CODE,
        _translation.PRICE
      ];

      this.PRICE = _translation.PRICE;
      this.SHARING_CODE = _translation.SHARING_CODE;

      for (let index = 0; index < this.titleContracts.length; index++) {
        this.isLoading[index] = false;
      }

      this.filterContract();

      const notSpecialProfiles = ["0", "1"];
      let favoriteProfile;

      if (this.haveGoldKavProfile) {
        favoriteProfile = GOLD_KAV_PROFILE;
      }
      else if (this.profilesOnCard.includes(this.OPERATOR_PROFILE)) {
        favoriteProfile = this.OPERATOR_PROFILE;
      }
      else {
        if (this.profilesOnCard.length == 1) {
          favoriteProfile = this.profilesOnCard[0];
        }
        else {
          //take the first profile, if it not special profile take the second profile
          favoriteProfile = notSpecialProfiles.includes(this.profilesOnCard[0]) ? this.profilesOnCard[1] : this.profilesOnCard[0];
        }
      }
      this.getProfile(favoriteProfile, profile => {
        this.filtering.controls['profile'].setValue(profile);
        this.filterContractAccordingProfile(favoriteProfile);

        // this.loadAndSetMOTZone();

        // this.today = new Date();
        // this.sixMonthsAgo = new Date();
        // this.sixMonthsAgo.setMonth(this.today.getMonth() - 6);
      })
    });
  }

  get f() {
    return this.filtering.controls;
  }

  checkMOTZone(poly: object) {
    this.storeService.getTranslation(translation => {
      const _translation = translation.GENERAL.AREAS;
      if (this.nameOfSelectedArea.length > 0) {
        if (this.divisionByRegions[this.nameOfSelectedArea] && this.divisionByRegions[this.nameOfSelectedArea].areas.find(region => region == poly["tazEzor"]) != undefined) {
          this.LAT_LOCATION = this.divisionByRegions[this.nameOfSelectedArea].location.lat;
          this.LNG_LOCATION = this.divisionByRegions[this.nameOfSelectedArea].location.lng;
          this.ZOOM = this.nameOfSelectedArea == _translation.BEER_SHEVA ? 8 : 9;
          return "#0bd637b9";
        }
        else if (this.nameOfSelectedArea == _translation.ALL) {
          this.LAT_LOCATION = 31.771959;
          this.LNG_LOCATION = 35.217018;
          this.ZOOM = 8;
        }
      }

      if (this.currentReformZones.find(name => name["zName"] == poly["zName"])) {
        return "#007D78";
      }
      return "transparent";
    });
  }

  loadAndSetMOTZone() {
    this.http.post<ServerResponse>(
      urls.GET_MOT_ZONE,
      {
        data: {},
        sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
      })
      .subscribe(res => {
        this.MOTZone = res?.data;
      })
  }

  chooseContract(i: number, j: number): void {
    this.isChooseContract = this.dataOfContract[i].details[j].contractPriceId;
    this.currentReformZones = this.dataOfContract[i].reformZones;
  }

  initProfilesOnCardDesc() {
    this.profilesOnCardDesc = [];
    const profile1 = this.profilesOnCard[0];
    const profile2 = this.profilesOnCard[1];
    if (profile1 == '0' && profile2 == undefined) {
      return;
    }
    else {
      if (profile1 != '0' && profile1 != undefined) {
        this.getProfile(profile1, profile => {
          this.profilesOnCardDesc.push(profile);
        })
      }
      if (profile2 != '0'  && profile2 != undefined) {
        if (profile1 != '0' && profile1 != undefined) {
          this.profilesOnCardDesc.push(", ");
        }
        this.getProfile(profile2, profile => {
          this.profilesOnCardDesc.push(profile);
        })
      }
    }
  }

  initAutocomplete() {
    this.storeService.getTranslation(translation => {
      this.profiles = [];
      this.profiles.push({ name: translation.GENERAL.ALL_PROFILES, id: "-999" });
      this.profilesOnCard.forEach((profile) => {
        this.getProfile(profile, res => {
          this.profiles.push({ name: res, id: profile.toString() });
        })
      });
      this.contracts = [
        { name: translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.COMPONENTS.DISPLAY_CONTRACT.ALL_CONTRACTS_KINDS, id: ["-999"] },
        { name: translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.COMPONENTS.DISPLAY_CONTRACT.STORED_VALUE, id: ["60", "61", "62", "63", "64", "65", "66", "67"] },
        { name: translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.COMPONENTS.DISPLAY_CONTRACT.ONE_TIME, id: ["10", "12", "13", "14", "15", "16", "17", "70", "71", "72", "73", "75", "91", "92"] },
        { name: translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.COMPONENTS.DISPLAY_CONTRACT.DAILY_FREE, id: ["22"] },
        { name: translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.COMPONENTS.DISPLAY_CONTRACT.WEEKLY_FREE, id: ["21"] },
        { name: translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.COMPONENTS.DISPLAY_CONTRACT.MONTHLY_FREE, id: ["20"] },
        { name: translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.COMPONENTS.DISPLAY_CONTRACT.YEARLY_FREE, id: ["25"] },
        ... this.profilesOnCard.includes(STUDENT_PROFILE) ? [{ name: translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.COMPONENTS.DISPLAY_CONTRACT.STUDENT, id: ["23", "24"] }] : []
      ]
      this.areas = [
        { name: translation.GENERAL.AREAS.ALL, id: translation.GENERAL.AREAS.ALL },
        { name: translation.GENERAL.AREAS.NORTH, id: translation.GENERAL.AREAS.HAIFA },
        { name: translation.GENERAL.AREAS.CENTER, id: translation.GENERAL.AREAS.TLV_AREA },
        { name: translation.GENERAL.AREAS.JERUSALEM, id: translation.GENERAL.AREAS.JERUSALEM },
        { name: translation.GENERAL.AREAS.SOUTH, id: translation.GENERAL.AREAS.BEER_SHEVA },
      ]
    });
  }

  getProfile(profileId, callbackFun) {
    this.storeService.getTranslation(translation => {
      let profileDesc: string;
      this.dataRecords.profilesWS.forEach(element => {
        if (element.Passenger_Profile_id == profileId) {
          profileDesc = element.Descr_HE;
        }
      });
      profileDesc ? callbackFun(profileDesc) : callbackFun(`${translation.GENERAL.UNIDENTIFIED} (${profileId})`);
    });
  }

  getEttCode(currentContract): string {
    let ettCodeDesc: string;
    this.dataRecords.ettCodesWS.forEach((ett) => {
      if (ett.ETT_Code_id == currentContract) {
        ettCodeDesc = ett.ETT_Descr;
      }
    });
    return ettCodeDesc
  }

  filterContractAccordingProfile(event) {
    this.contractsService.setProfile(event);
    this.filterContract();
  }

  filterContractAccordingContract(event) {
    this.contractsService.setEttCode(event);
    this.filterContract();
  }

  filterContractAccordingArea(event) {
    this.nameOfSelectedArea = event;
    this.contractsService.setArea(event);
    this.filterContract();
  }

  filterContract() {
    if (!this.contractsService.getContractAccordingFilter()) {
      this.contractsService.getContrct(isAnonymousCard(this.dataFromCard));
    }
    this.dataOfContract = this.contractsService.getContractAccordingFilter();
    this.initContractToLoading(this.dataOfContract);
  }

  choosePoly(poly) {
    for (let region in this.divisionByRegions) {
      if ((this.divisionByRegions[region].areas.find(area => area == poly["tazEzor"])) != undefined) {
        this.nameOfSelectedArea = region
        this.f.area.setValue(region)
        this.filterContractAccordingArea(region)
      }
    }
  }

  initContractToLoading(data) {
    this.contractsToLoading = [];
    this.initProfilesOnCardDesc();
    this.initAutocomplete();
    for (let i = 0; i < data.length; i++) {
      this.contractsToLoading[i] = [];
      for (let j = 0; j < data[i].details.length; j++) {
        this.cntOfContract++;
        this.initContract(data[i], j, contract => {
          this.contractsToLoading[i][j] = contract;
        });
      }
    }
  }

  initContract(cont, j: number, callbackFun) {
    this.storeService.getTranslation(translation => {
      let price, exactPrice;
      if (cont.details[j].ettCode == "65") {
        price = translation.GENERAL.CHANGEABLE;
        exactPrice = translation.GENERAL.CHANGEABLE;
      }
      else {
        //price
        price = cont.details[j].price.slice(0, -2);
        if (price.length <= 0) {
          price = "0";
        }
        //exact price
        exactPrice = cont.details[j].exactPrice;
        if (exactPrice.length <= 0) {
          exactPrice = "0";
        }
      }

      this.getProfile(cont.details[j].profileType, profile => {
        const contract = {
          index: j.toString(),
          contractType: this.getEttCode(cont.details[j].ettCode),
          contractDescr: cont.details[j].descrA,
          profile: profile,
          shareCode: cont.shareCode,
          price: price,
          exactPrice: exactPrice
        }

        callbackFun(contract);
      })
    });
  }

  initToPurchaseContract(arrayElement, pushField, field) {
    this.dataFromCard[arrayElement].forEach(element => {
      pushField.push(element !== null ? element[field] : "");
    });
  }

  async selectContract(indexOfContract: number, contract) {
    this.storeService.getTranslation(translation => {
      const fullContract = this.dataOfContract[indexOfContract].details[contract.index];
      let loadContractStartDate;
      const freeMonthly = isFreeMonthly(fullContract);
      // const freeRide = isFreeRide(fullContract);
      const freeRide = false;

      if (!freeMonthly && !freeRide) {
        loadContractStartDate = this.getLoadContractStartDate(fullContract);
        this.sendCanLoadContractOnCard(indexOfContract, contract, loadContractStartDate);
      }
      else {
        if (freeMonthly) {
          const dialogRef = this.dialog.open(SelectMonthFreeMonthlyComponent,
            { direction: translation.DIRECTION }
          );

          dialogRef.afterClosed().subscribe(selectedDate => {
            if (selectedDate) {
              loadContractStartDate = selectedDate.valueOf().toString();
              this.sendCanLoadContractOnCard(indexOfContract, contract, loadContractStartDate);
            }
          });
        }
        else { //freeRide
          const dialogRef = this.dialog.open(SelectExpirationFreeRideComponent,
            { direction: translation.DIRECTION }
          );

          dialogRef.afterClosed().subscribe(selectedDate => {
            if (selectedDate) {
              const loadContractEndDate = selectedDate.valueOf().toString();
              //TODO: start-end issue (server side)
              this.sendCanLoadContractOnCard(indexOfContract, contract, loadContractEndDate);
            }
          });
        }
      }
    });
  }

  getLoadContractStartDate = (contract) => {
    const today = new Date().getTime();
    const contractStartDate = new Date(contract.ValidityStartDate + " UTC").getTime();

    if (today > contractStartDate) {
      return today;
    }
    return contractStartDate;
  }

  sendCanLoadContractOnCard(indexOfContract, contract, loadContractStartDate) {
    const data = {
      card: {
        contracts: [],
        counter: this.dataFromCard.counter.counterOriginDump,
        environment: this.dataFromCard.environment.originDump,
        events: [],
        specialEvents: []
      },
      contractPriceId: this.dataOfContract[indexOfContract].details[contract.index].contractPriceId,
      saleDevice: sessionStorage.getItem(sessionKeys.cashboxId),
      storedValueCount: (this.dataOfContract[indexOfContract].details[contract.index].ettCode == "65" ? "0" : serverConstant.general.NULL),
      loadContractStartDate: loadContractStartDate,
      isFreeMonthlySalesRestrictionStartEnd: "false"
    }
    this.initToPurchaseContract("contracts", data.card.contracts, "originDump");
    this.initToPurchaseContract("events", data.card.events, "eventOriginDump");
    this.initToPurchaseContract("specialEvents", data.card.specialEvents, "eventOriginDump");

    this.http
    .post<ServerResponse>(
     urls.CAN_LOAD_CONTRACT_ON_CARD,
    {
      data: data,
      sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
    })
    .subscribe(
      res => {
        switch(res.statusCode) {
          case 1:
            this.contractData = {
              contract: contract,
              contractPriceId: this.dataOfContract[indexOfContract].details[contract.index].contractPriceId,
              ettCode: this.dataOfContract[indexOfContract].details[contract.index].ettCode,
              profileType: this.dataOfContract[indexOfContract].details[contract.index].profileType,
              price: this.dataOfContract[indexOfContract].details[contract.index].price,
              loadContractStartDate: loadContractStartDate
            }

            this.dialogRavKacService.from = From.DISPLAY_CONTRACT;

            const contractPrice = Number(contract.price.slice(0,-1));
            if (contractPrice == 0) {
              this.contractData.canSkip = true;
              this.openConfirmPurchase();
            }
            else {
              this.contractData.canSkip = false;
              this.canLoadContractEmit();
            }
            break;
          default:
            this.openCantLoadContractModal(res);
            break;
        }
      }
    )
  }

  canLoadContractEmit = () => {
    this.canLoadContract.emit(this.contractData);
  }

  openCantLoadContractModal(info) {
    this.storeService.getTranslation(translation => {
      const _translation = translation.GENERAL;
      this.infoModalIcon = "assets/icons/infoIcons/error.svg";
      this.infoModalTitle = _translation.CANT_LOAD_CONTRACT;
      if (info.data.errorStatus) {
        const translationPath = getErrorStatusTranslationPath(info.data.errorStatus);
        let errorStatusTranslation = translation;
        translationPath.forEach(key => {
          errorStatusTranslation = errorStatusTranslation[key];
        });
        this.infoModalSubTitle = [errorStatusTranslation];
      }
      else {
        this.infoModalSubTitle = [_translation.UNIDENTIFIED_ERROR];
      }
      if (info.description.includes("loading on this date does not valid")) {
        this.infoModalSubTitle = [_translation.INVALID_LOAD_DATE];
      }
      this.infoModalHr = false;
      this.infoModalBtnLeftText = _translation.BACK;
      this.infoModalBtnLeftClickFun = this.closeInfoModal;
      this.infoModalBackgroundClickFunction = this.closeInfoModal;

      this.modalService.open('info-modal-display-contract');
    });
  }

  openConfirmPurchase = () => {
    this.storeService.getTranslation(translation => {
      this.infoModalIcon = "assets/icons/infoIcons/warning.svg";
      this.infoModalTitle = translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.COMPONENTS.DISPLAY_CONTRACT.SURE_LOAD_CONTRACT;
      this.infoModalSubTitle = null;
      this.infoModalHr = false;
      this.infoModalBtnTitle = null;
      this.infoModalBtnLeftText = translation.GENERAL.OK;
      this.infoModalBtnLeftClickFun = this.canLoadContractEmit;
      this.infoModalBtnRightText = translation.GENERAL.CANCEL;
      this.infoModalBtnRightClickFun = this.closeInfoModal;
      this.infoModalBackgroundClickFunction = this.closeInfoModal;

      this.modalService.open('info-modal-display-contract');
    });
  }

  closeInfoModal = () => {
    this.modalService.close('info-modal-display-contract');
  }

  sort = (index) => {
    let rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    switching = true;
    // Set the sorting direction to ascending:
    dir = "asc";
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
      // Start by saying: no switching is done:
      switching = false;
      rows = this.contractsTable.nativeElement.rows;
      /* Loop through all table rows (except the
      first, which contains table headers): */
      for (i = 1; i < (rows.length - 1); i++) {
        // Start by saying there should be no switching:
        shouldSwitch = false;
        /* Get the two elements you want to compare,
        one from current row and one from the next: */
        x = rows[i].getElementsByTagName("TD")[index];
        y = rows[i + 1].getElementsByTagName("TD")[index];
        /* Check if the two rows should switch place,
        based on the direction, asc or desc: */
        let xContent = x.innerText.toLowerCase();
        let yContent = y.innerText.toLowerCase();

        if (this.titleContracts[index] == this.PRICE || this.titleContracts[index] == this.SHARING_CODE) {
          if (this.titleContracts[index] == this.PRICE) {
            xContent = xContent.slice(1);
            yContent = yContent.slice(1);
          }
          xContent = parseFloat(xContent.replaceAll(',','').replace(this.storeService.currencySymbol,''));
          yContent = parseFloat(yContent.replaceAll(',','').replace(this.storeService.currencySymbol,''));

          xContent = isNaN(xContent) ? -1 : xContent;
          yContent = isNaN(yContent) ? -1 : yContent;
        }
        if (dir == "asc") {
          if (xContent > yContent ) {
            // If so, mark as a switch and break the loop:
            shouldSwitch = true;
            break;
          }
        } else if (dir == "desc") {
          if (xContent < yContent ) {
            // If so, mark as a switch and break the loop:
            shouldSwitch = true;
            break;
          }
        }
      }
      if (shouldSwitch) {
        /* If a switch has been marked, make the switch
        and mark that a switch has been done: */
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
        // Each time a switch is done, increase this count by 1:
        switchcount ++;
      } else {
        /* If no switching has been done AND the direction is "asc",
        set the direction to "desc" and run the while loop again. */
        if (switchcount == 0 && dir == "asc") {
          dir = "desc";
          switching = true;
        }
      }
    }
  };

  onClickSort = (i) => {
    this.isLoading[i] = true;
    setTimeout(
      () => {
        this.sort(i);
        this.isLoading[i] = false;
      }, 1);
  }

  showChooseProfile(): boolean {
    return this.profilesOnCard.length > 1 && !this.haveGoldKavProfile && !this.adminCashbox;
  }

  ngOnDestroy(): void {
    this.contractsService.initParams();
  }
}
