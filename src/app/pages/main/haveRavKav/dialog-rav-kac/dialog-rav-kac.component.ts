import { Component, OnInit, Inject, EventEmitter, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ContractsService } from 'src/app/shared/services/contracts.service';
import { RefreshDataService, DataMessages } from 'src/app/shared/services/refresh-data.service';
import { Subscription } from 'rxjs';
import { DialogRavKacService } from './dialog-rav-kac.service';
import { From } from 'src/app/shared/constants/driverBox';
import { MainService } from '../../main.service';
import { isAnonymousCard } from 'src/app/shared/utils/ravkav';
import { GOLD_KAV_PROFILE } from 'src/app/shared/constants/ravKav';
import { Direction } from '@angular/cdk/bidi';
import { StoreService } from 'src/app/shared/services/store.service';

@Component({
  selector: 'app-dialog-rav-kac',
  templateUrl: './dialog-rav-kac.component.html',
  styleUrls: ['./dialog-rav-kac.component.css']
})
export class DialogRavKacComponent implements OnInit, OnDestroy {
  homePage: boolean = true;
  editProfile: boolean = false;
  isContractLoading: boolean = false;
  canLoading: boolean = false;
  confirmCancelation: boolean = false;
  contractToBuy = [];
  data = {}
  contractToCancelation;
  $dataView: Subscription;
  reayToWriteOnCard = new EventEmitter();
  successLoadOnRavKav = new EventEmitter();
  backToHomeSub: Subscription;
  title;

  DIRECTION: Direction = "ltr";
  CONTRACT_LIST;
  PAYMENT;
  CANCEL_PURCHASE;
  ADD_PROFILE;
  RAVKAV_DETAILS;

  constructor(
    public dialogRef: MatDialogRef<DialogRavKacComponent>,
    @Inject(MAT_DIALOG_DATA) public ravKavDetails: any,
    private contractsService: ContractsService,
    private dataView: RefreshDataService,
    private dialogRavKacService: DialogRavKacService,
    private mainService: MainService,
    private storeService: StoreService
  ) {
    this.data["ravKavDetails"] = ravKavDetails;
  }

  ngOnInit() {
    this.storeService.getTranslation(translation => {
      this.CONTRACT_LIST  = translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.CONTRACT_LIST;
      this.PAYMENT  = translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.PAYMENT;
      this.CANCEL_PURCHASE  = translation.GENERAL.CANCEL_PURCHASE;
      this.ADD_PROFILE  = translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.ADD_PROFILE;
      this.RAVKAV_DETAILS  = translation.PAGES.MAIN.HAVE_RAVKAV.DIALOG_RAVKAV.RAVKAV_DETAILS;
      this.DIRECTION = translation.DIRECTION;

      let i: number = 1;
      let profilesForContracts:number[] = [];

      while (this.ravKavDetails.environment["holderProfile" + i + "Code"] != undefined && this.ravKavDetails.environment["holderProfile" + i + "Date"] != undefined) {
        const profileDate = new Date(this.ravKavDetails.environment["holderProfile" + i + "Date"]).setHours(23,59,59,999);
        if (profileDate > new Date().getTime()) {
          profilesForContracts.push(this.ravKavDetails.environment["holderProfile" + i + "Code"]);
        }
        i++;
      }

      if(this.hasProfile1(profilesForContracts)) {
        //remove 0 profile
        profilesForContracts = profilesForContracts.filter(profile => profile != 0);
      }
      //not has 1 profile
      else {
        //add 0 profile
        profilesForContracts.push(0);
      }

      //if have gold-kav (75+) profile get just these contracts
      if (profilesForContracts.includes(+GOLD_KAV_PROFILE)) {
        profilesForContracts = [+GOLD_KAV_PROFILE];
      }
      this.mainService.getAllContracts(profilesForContracts.toString(), isAnonymousCard(this.ravKavDetails));
      this.contractsService.getContrct(isAnonymousCard(this.ravKavDetails));

      this.initRefreshDataViewObserver();

      this.backToHomeSub = this.dialogRavKacService.backToHome.subscribe(
        () => {
          this.backToHomePage();
        }
      );
    });
  }

  initRefreshDataViewObserver() {
    this.$dataView = this.dataView.changeEmitted$.subscribe(message => {
      switch (message.type) {
        case DataMessages.REFRESH_DATA:
          this.data["ravKavDetails"] = message.payload
          break;
      }
    })
  }

  isValidationNum(ravKavDetails: any): boolean {
    return typeof ravKavDetails === 'number';
  }

  writeContractOnCard(data) {
    this.reayToWriteOnCard.emit(data);
  }

  goToHomePage(isSuccessToLoadContract) {
    if (isSuccessToLoadContract) {
      this.successLoadOnRavKav.emit();
      this.ravKavDetails = "";
      this.canLoading = false;
      this.isContractLoading = false;
      this.confirmCancelation = false;
      this.homePage = true;
    }
  }

  canLoadContract(contract) {
    if (contract) {
      this.contractToBuy = contract;
      this.homePage = false;
      this.isContractLoading = false;
      this.confirmCancelation = false;
      this.canLoading = true;
    }
  }

  backToHomePage(): void {
    this.isContractLoading = false;
    this.canLoading = false;
    this.confirmCancelation = false;
    this.editProfile = false;
    this.homePage = true;
  }

  backToContracts(): void {
    this.canLoading = false;
    this.homePage = false;
    this.confirmCancelation = false;
    this.isContractLoading = true;
  }

  backFromPayment(): void {
    if (this.dialogRavKacService.from == From.DISPLAY_CONTRACT) {
      this.backToContracts();
    }
    else {
      this.backToHomePage();
    }
  }

  goToContractsPage() {
    this.homePage = false;
    this.canLoading = false;
    this.confirmCancelation = false;
    this.isContractLoading = true;
  }

  goToEditProfilePage() {
    this.homePage = false;
    this.canLoading = false;
    this.confirmCancelation = false;
    this.isContractLoading = false;
    this.editProfile = true;
  }

  goToConfirmCancelation(contract) {
    if (contract) {
      this.contractToCancelation = contract;
      this.homePage = false;
      this.isContractLoading = false;
      this.canLoading = false;
      this.confirmCancelation = true;
    }
  }

  getTitle() {
    switch(true) {
      case (this.isContractLoading):
        return this.CONTRACT_LIST;
      case (this.canLoading):
        return this.PAYMENT;
      case (this.confirmCancelation):
        return this.CANCEL_PURCHASE;
      case (this.editProfile):
        return this.ADD_PROFILE;
      case (this.homePage):
        return this.RAVKAV_DETAILS;
    }
  }

  hasProfile1(profilesForContracts): boolean {
    return profilesForContracts.includes(1);
  }

  ngOnDestroy(): void {
    this.$dataView.unsubscribe();
    this.backToHomeSub.unsubscribe();
  }
}
