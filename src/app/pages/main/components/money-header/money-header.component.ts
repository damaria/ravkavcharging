import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';
import { StoreService } from 'src/app/shared/services/store.service';

@Component({
  selector: 'app-money-header',
  templateUrl: './money-header.component.html',
  styleUrls: ['./money-header.component.css']
})
export class MoneyHeaderComponent implements OnInit, OnDestroy{
  cashbox;
  cashBoxName = sessionStorage.getItem(sessionKeys.cashboxName);
  cashBoxSum;
  cashboxUpdateSubscription: Subscription;
  Number = Number;
  adminCashbox: boolean;

  constructor(public storeService: StoreService) { }

  ngOnInit(): void {
    this.cashbox = this.storeService.getCashbox();
    this.cashBoxSum = this.storeService.getCashBoxSum();
    this.adminCashbox = JSON.parse(sessionStorage.getItem(sessionKeys.adminCashbox));

    this.cashboxUpdateSubscription = this.storeService.cashBoxChanged.subscribe(
      (res: any) => {
        this.cashbox = { ...res.cashBox };
        this.cashBoxSum = res.cashBoxSum;
      }
    )
  }

  getSumCashbox() {
    if (typeof(this.cashBoxSum) == 'undefined') {
      return 0;
    }
    return this.cashBoxSum;
  }

  ngOnDestroy() {
    this.cashboxUpdateSubscription.unsubscribe();
  }
}
