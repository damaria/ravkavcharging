import { Component, Input, OnInit } from '@angular/core';
import { getValue } from 'src/app/shared/constants/server';
import { getImageUrl } from 'src/app/shared/utils/cashbox';
import { cashboxDataType } from '../../../../../shared/constants/server'

@Component({
  selector: 'app-money-header-component',
  templateUrl: './money-header-component.component.html',
  styleUrls: ['./money-header-component.component.css']
})
export class MoneyHeaderComponentComponent implements OnInit {
  @Input() type;
  @Input() amount;

  imageUrl;
  sum;
  cashboxDataType = cashboxDataType;

  ngOnInit(): void {
    this.imageUrl = getImageUrl(this.type);
    this.sum = this.amount * getValue(this.type) / 100;
    if (this.amount >= 1000) {
      this.amount = this.amount.toLocaleString('en-US');
    }
  }
}
