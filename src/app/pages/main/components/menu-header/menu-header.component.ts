import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { LoginService } from 'src/app/pages/login/login.service';
import { LoginResponse, LoginResponseType } from 'src/app/pages/login/loginResponse';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { StocktakingStatus } from 'src/app/shared/constants/driverBox';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';
import { StoreService } from 'src/app/shared/services/store.service';

@Component({
  selector: 'app-menu-header',
  templateUrl: './menu-header.component.html',
  styleUrls: ['./menu-header.component.css']
})
export class MenuHeaderComponent implements OnInit ,OnDestroy {
  userName;
  cashboxName;
  cashboxSum;
  cashboxUpdateSubscription: Subscription;

  infoModalBackgroundClickFunction;
  infoModalIcon;
  infoModalHr;
  infoModalTitle;
  infoModalSubTitle;
  infoModalBtnTitle;
  infoModalBtnRightText;
  infoModalBtnLeftText;
  infoModalBtnRightClickFun;
  infoModalBtnLeftClickFun;
  infoModalWarning;
  adminCashbox: boolean;

  constructor(
    public storeService: StoreService,
    private router: Router,
    private loginService: LoginService,
    private modalService: ModalService
    ) { }


  ngOnInit(): void {
    this.storeService.getTranslation(translation => {
      if (sessionStorage.getItem(sessionKeys.username)) {
        this.userName = sessionStorage.getItem(sessionKeys.username);
        this.cashboxName = sessionStorage.getItem(sessionKeys.cashboxName);
        this.cashboxSum = this.storeService.getCashBoxSum();
      }

      else {
        this.userName = translation.PAGES.MAIN.COMPONENTS.MENU_HEADER.ANONYMOUS;
        this.cashboxName = translation.PAGES.MAIN.COMPONENTS.MENU_HEADER.NO_NAME;
        this.cashboxSum = 0;
      }

      this.cashboxUpdateSubscription = this.storeService.cashBoxChanged.subscribe(
        (res: any) => {
          this.cashboxSum = res.cashBoxSum;
        }
      );

      this.infoModalBackgroundClickFunction = null;
      this.infoModalIcon = "assets/icons/infoIcons/warning.svg";
      this.infoModalBtnRightClickFun = this.closeInfoModal;
    });

    this.adminCashbox = JSON.parse(sessionStorage.getItem(sessionKeys.adminCashbox));
  }

  goToBreak = () => {
    this.closeInfoModal();
    this.loginService.checkLogin().subscribe(
      connectStatus => {
        if (connectStatus == true) {
          this.loginService.goToBreak()
            .subscribe(
              (res: LoginResponse) => {
                if (res.type == LoginResponseType.goToBreakSuccessfully) {
                  const operatorId = sessionStorage.getItem(sessionKeys.operatorId);
                  this.router.navigate(['/login', operatorId]);
                }
              });
        }
      }
    );
  }

  closeShift = () => {
    this.closeInfoModal();
    this.loginService.checkLogin().subscribe(
      connectStatus => {
        if (connectStatus == true) {
          sessionStorage.removeItem(sessionKeys.adminCashbox);
          const cashboxSum = this.storeService.getCashBoxSum();
          if (!this.adminCashbox && cashboxSum != 0) {
            sessionStorage.setItem(sessionKeys.stocktakingStatus, StocktakingStatus.END_SHIFT);
            this.router.navigate(['./main/stocktaking']);
          }
          else {
            this.loginService.disconnect()
            .subscribe(
              (res: LoginResponse) => {
                if (res.type == LoginResponseType.logoutFromSessionSuccessfully) {
                  const operatorId = sessionStorage.getItem(sessionKeys.operatorId);
                  this.router.navigate(['/login', operatorId]);
                }
              }
            );
          }
        }
      }
    );
  }

  closingDay = () => {
    this.closeInfoModal();
    this.loginService.checkLogin().subscribe(
      connectStatus => {
        if (connectStatus == true) {
          const cashboxSum = this.storeService.getCashBoxSum();
          if (cashboxSum != 0) {
            sessionStorage.setItem(sessionKeys.stocktakingStatus, StocktakingStatus.END_DAY);
            this.router.navigate(['./main/stocktaking']);
          }
          else {
            this.loginService.closingDay()
            .subscribe(
              (res: LoginResponse) => {
                if (res.type == LoginResponseType.logoutFromSessionSuccessfully) {
                  const operatorId = sessionStorage.getItem(sessionKeys.operatorId);
                  this.router.navigate(['/login', operatorId]);
                }
              }
            );
          }
        }
      }
    );
  }

  openInfoModalGoToBreak = () => {
    this.storeService.getTranslation(translation => {
      this.infoModalTitle = translation.PAGES.MAIN.COMPONENTS.MENU_HEADER.SURE_TAKE_BREAK;
      this.infoModalWarning = null;
      this.infoModalBtnRightText = translation.GENERAL.NO;
      this.infoModalBtnLeftText = translation.GENERAL.YES;
      this.infoModalBtnLeftClickFun = this.goToBreak;

      this.modalService.open('info-modal-menu-header');
    });
  }

  openInfoModalClosingDay = () => {
    this.storeService.getTranslation(translation => {
      this.infoModalTitle = translation.PAGES.MAIN.COMPONENTS.MENU_HEADER.SURE_CLOSE_DAY;
      this.infoModalWarning = translation.PAGES.MAIN.COMPONENTS.MENU_HEADER.CLOSE_DAY_NOTE;
      this.infoModalBtnRightText = translation.GENERAL.NO;
      this.infoModalBtnLeftText = translation.GENERAL.YES;
      this.infoModalBtnLeftClickFun = this.closingDay;

      this.modalService.open('info-modal-menu-header');
    });
  }

  openInfoModalClosingShift = () => {
    this.storeService.getTranslation(translation => {
      this.infoModalTitle = translation.PAGES.MAIN.COMPONENTS.MENU_HEADER.SURE_CLOSE_SHIFT;
      this.infoModalWarning = null;
      this.infoModalBtnRightText = translation.GENERAL.NO;
      this.infoModalBtnLeftText = translation.GENERAL.YES;
      this.infoModalBtnLeftClickFun = this.closeShift;

      this.modalService.open('info-modal-menu-header');
    });
  }

  openInfoModalExit = () => {
    this.storeService.getTranslation(translation => {
      this.infoModalTitle = translation.PAGES.MAIN.COMPONENTS.MENU_HEADER.SURE_EXIT;
      this.infoModalWarning = null;
      this.infoModalBtnRightText = translation.GENERAL.NO;
      this.infoModalBtnLeftText = translation.GENERAL.YES;
      this.infoModalBtnLeftClickFun = this.closeShift;

      this.modalService.open('info-modal-menu-header');
    });
  }

  closeInfoModal = () => {
    this.modalService.close('info-modal-menu-header');
  }

  ngOnDestroy() {
    this.cashboxUpdateSubscription.unsubscribe();
  }
}
