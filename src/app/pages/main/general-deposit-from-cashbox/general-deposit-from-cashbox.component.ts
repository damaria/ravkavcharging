import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { CashboxWithoutPapers } from 'src/app/shared/models/cashboxWithoutPapers.model';
import { getValue, ServerResponse, statusCode } from '../../../shared/constants/server';
import { AmountInputService, subData } from 'src/app/shared/components/amount-input/amount-input.service';
import { HttpClient } from '@angular/common/http';
import { LoginService } from '../../login/login.service';
import { urls } from '../../../shared/constants/URLs';
import * as serverConstant from '../../../shared/constants/server';
import { StoreService } from 'src/app/shared/services/store.service';
import { getImageUrl } from 'src/app/shared/utils/cashbox';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Cashbox } from 'src/app/shared/models/cashbox.model';

@Component({
  selector: 'app-general-deposit-from-cashbox',
  templateUrl: './general-deposit-from-cashbox.component.html',
  styleUrls: ['./general-deposit-from-cashbox.component.css']
})
export class GeneralDepositFromCashboxComponent implements OnInit, OnDestroy {
  depositCashbox: CashboxWithoutPapers;
  amountInputChangedSub: Subscription;
  depositSum;

  infoModalBackgroundClickFunction;
  infoModalIcon;
  infoModalHr;
  infoModalTitle;
  infoModalSubTitle;
  infoModalBtnTitle;
  infoModalBtnRightText;
  infoModalBtnLeftText;
  infoModalBtnRightClickFun;
  infoModalBtnLeftClickFun;
  infoModalError;

  transactionId;

  emptyCheckoutReason = false;
  moreThanMax = false;
  lessThanZero = false;
  intervalTime = 2500;

  checkoutReasonError = false;

  currCashbox: Cashbox = new Cashbox();

  @ViewChild('checkoutReason') checkoutReason: ElementRef;
  @ViewChild('f') f: NgForm;

  constructor(
    private amountInputService: AmountInputService,
    private http: HttpClient,
    private loginService: LoginService,
    private storeService: StoreService,
    private modalService: ModalService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.currCashbox = this.storeService.getCashbox();
    if (!this.currCashbox) {
      this.loginService.checkLogin().subscribe(
        connectStatus => {
          if (connectStatus == true) {
            this.storeService.updateCashBox().subscribe(
              () => {
                this.currCashbox = this.storeService.getCashbox();
              }
            );
          }
        }
      );
    }
    this.depositCashbox = new CashboxWithoutPapers();

    this.amountInputChangedSub = this.amountInputService.inputChanged.subscribe(
      (data: subData) => {
        switch(data.operation) {
          case 'increase':
            this.depositCashbox[data.type]++;
            break;
          case 'decrease':
            this.depositCashbox[data.type]--;
            break;
          case 'change':
            this.depositCashbox[data.type] = data.amount;
            break;
          case 'moreThanMax':
            this.depositCashbox[data.type] = data.amount;
            this.openPopUp('moreThanMax');
            break;
          case 'lessThanZero':
            this.openPopUp('lessThanZero');
            break;
        }
      }
    );
  }

  _getImageUrl(type) {
    return getImageUrl(type);
  }

  _getValue(code) {
    return getValue(code);
  }

  resetCashbox() {
    this.depositCashbox = new CashboxWithoutPapers();
  }

  openPopUp(error) {
    switch(error) {
      case 'emptyCheckoutReason':
        this.emptyCheckoutReason = true;

        setTimeout(() => {
        this.emptyCheckoutReason = false;
        }, this.intervalTime);
        break;
      case 'moreThanMax':
        this.moreThanMax = true;

        setTimeout(() => {
        this.moreThanMax = false;
        }, this.intervalTime);
        break;
      case 'lessThanZero':
        this.lessThanZero = true;

        setTimeout(() => {
        this.lessThanZero = false;
        }, this.intervalTime);
        break;
    }
  }

  onSubmit() {
    if (this.depositCashbox.getSum() == 0 || !this.f.form.value.checkoutReason) {
      if (this.depositCashbox.getSum() != 0 && !this.f.form.value.checkoutReason) {
        this.checkoutReasonError = true;
        this.openPopUp('emptyCheckoutReason');
      }
      return;
    }

    this.depositSum = this.depositCashbox.getSum();
    this.loginService.checkLogin().subscribe(
      connectStatus => {
        if (connectStatus == true) {
          const shift = JSON.parse(sessionStorage.getItem(sessionKeys.shift));
          let minusDepositCashbox = new Cashbox();
          minusDepositCashbox.decrease(this.depositCashbox);
          const depositData = minusDepositCashbox.convertToServerFormat();
          this.http
              .post<ServerResponse>(
                  urls.ADD_DRIVER_CASHBOX,
                  {
                    data: {
                      deviceIdCashier: sessionStorage.getItem(sessionKeys.cashboxId),
                      idNumberDriver: serverConstant.general.NULL,
                      shiftIdCashier: shift.Shift_id || shift.shiftId,
                      driverCashboxData: depositData,
                      shifts: [],
                      jsonObjRes: true,
                      comment: this.f.form.value.checkoutReason,
                      actionTypeTransaction: serverConstant.actionTypeTransaction.TAKE_OUT_MONEY_PROACTIVELY,
                      timestamp: new Date().getTime()
                    },
                    sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
                  })
                  .subscribe(res => {
                    if (res.statusCode == statusCode.OK) {
                      this.storeService.updateCashBox().subscribe(
                        () => {
                          this.currCashbox = this.storeService.getCashbox();
                        }
                      );
                      this.transactionId = res.data[0].TransactionId;
                      this.openInfoModal();
                    }
                  })
          this.resetCashbox();
        }
      }
    );
  }

  openInfoModal() {
    this.storeService.getTranslation(translation => {
      this.infoModalIcon = "assets/icons/infoIcons/ok.svg";
      this.infoModalTitle = translation.PAGES.MAIN.GENERAL_DEPOSIT_FROM_CASHBOX.DEPOSIT_FROM_CASHBOX_SUCCESS;
      this.infoModalSubTitle = [`${this.depositSum} ${this.storeService.currencySymbol} ${translation.PAGES.MAIN.GENERAL_DEPOSIT_FROM_CASHBOX.WERE_DEPOSIT_FROM_CASHBOX}`];
      this.infoModalBtnTitle = translation.PAGES.MAIN.PRINT_REFERENCE;
      this.infoModalHr = true;
      this.infoModalBtnLeftText = translation.GENERAL.YES;
      this.infoModalBtnLeftClickFun = this.printReceipt;
      this.infoModalBtnRightText = translation.GENERAL.NO;
      this.infoModalBtnRightClickFun = this.navigateToHome;
      this.infoModalBackgroundClickFunction = null;

      this.modalService.open('info-modal-general-deposit-from-cashbox');
    });
  }

  navigateToHome = () => {
    this.router.navigate(['./main/home']);
  }

  printReceipt = () => {
    this.storeService.getTranslation(translation => {
      const _translation = translation.PAGES.MAIN.GENERAL_DEPOSIT_FROM_CASHBOX.PAGE_TITLE;
      this.storeService.printReceipt(this.transactionId, _translation);
      this.navigateToHome();
    });
  }

  checkoutReasonFocus() {
    this.checkoutReason.nativeElement.setAttribute('style', 'border: 2px black solid;');
    this.checkoutReasonError = false;
  }

  ngOnDestroy() {
    this.amountInputChangedSub.unsubscribe();
  }
}
