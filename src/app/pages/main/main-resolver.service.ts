import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';
import { StoreService } from 'src/app/shared/services/store.service';
import { minutesToMillis } from 'src/app/shared/utils/date';
import { LoginService } from '../login/login.service';
import { MainService } from './main.service';

@Injectable({ providedIn: 'root' })
export class MainResolverService implements Resolve<any> {
  constructor(
    private storeService: StoreService,
    private loginService: LoginService,
    private router: Router,
    private mainService: MainService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (sessionStorage.getItem(sessionKeys.cashboxLocked)) {
      const operatorId = sessionStorage.getItem(sessionKeys.operatorId);
      this.router.navigate(['/login', operatorId]);
    }
    else {
      const sessionData = JSON.parse(sessionStorage.getItem(sessionKeys.sessionData));
      if (sessionData) {
        this.storeService.updateCashBox().subscribe();
        this.mainService.getCreditValue();
        this.loginService.checkLogin().subscribe();
        const interval = minutesToMillis(15);
        setInterval(()=> {
          this.loginService.checkLogin().subscribe();
        },
        interval);

        const operatorId = sessionStorage.getItem(sessionKeys.operatorId);
        this.storeService.getOperatorConfiguration(operatorId);
      }
      else {
        this.router.navigate(['']);
      }
    }
  }
}
