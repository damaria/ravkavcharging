import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { CashboxWithoutPapers } from 'src/app/shared/models/cashboxWithoutPapers.model';
import { cashboxDataType, getValue, ServerResponse, statusCode } from '../../../shared/constants/server';
import { AmountInputService, subData } from 'src/app/shared/components/amount-input/amount-input.service';
import { HttpClient } from '@angular/common/http';
import { LoginService } from '../../login/login.service';
import { urls } from '../../../shared/constants/URLs';
import * as serverConstant from '../../../shared/constants/server';
import { StoreService } from 'src/app/shared/services/store.service';
import { getImageUrl } from 'src/app/shared/utils/cashbox';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';
import { Cashbox } from 'src/app/shared/models/cashbox.model';
import { Router } from '@angular/router';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { DepositToBankService } from './deposit-to-bank.service';

@Component({
  selector: 'app-deposit-to-bank',
  templateUrl: './deposit-to-bank.component.html',
  styleUrls: ['./deposit-to-bank.component.css']
})
export class DepositToBankComponent implements OnInit, OnDestroy {
  depositCashbox: CashboxWithoutPapers = new CashboxWithoutPapers();
  amountInputChangedSub: Subscription;

  currCashbox: Cashbox = new Cashbox();
  moreThanMax = false;
  lessThanZero = false;
  intervalTime = 3000;

  depositSum;

  infoModalBackgroundClickFunction;
  infoModalIcon;
  infoModalHr;
  infoModalTitle;
  infoModalSubTitle;
  infoModalBtnTitle;
  infoModalBtnRightText;
  infoModalBtnLeftText;
  infoModalBtnRightClickFun;
  infoModalBtnLeftClickFun;
  infoModalError;

  transactionId;
  getImageUrl = getImageUrl;
  cashboxDataType = cashboxDataType;
  @ViewChild('depositVouchers') depositVouchers: ElementRef;

  constructor(
    private amountInputService: AmountInputService,
    private http: HttpClient,
    private loginService: LoginService,
    public storeService: StoreService,
    private modalService: ModalService,
    private router: Router,
    public depositToBankService: DepositToBankService
    ) { }

  ngOnInit(): void {
    this.currCashbox = this.storeService.getCashbox();
    if (!this.currCashbox) {
      this.loginService.checkLogin().subscribe(
        connectStatus => {
          if (connectStatus == true) {
            this.storeService.updateCashBox().subscribe(
              () => {
                this.currCashbox = this.storeService.getCashbox();
              }
            );
          }
        }
      );
    }

    this.depositCashbox = new CashboxWithoutPapers();

    this.amountInputChangedSub = this.amountInputService.inputChanged.subscribe(
      (data: subData) => {
        switch(data.operation) {
          case 'increase':
            this.depositCashbox[data.type]++;
            break;
          case 'decrease':
            this.depositCashbox[data.type]--;
            break;
          case 'change':
            this.depositCashbox[data.type] = data.amount;
            break;
          case 'moreThanMax':
            this.depositCashbox[data.type] = data.amount;
            this.openPopUp('moreThanMax');
            break;
          case 'lessThanZero':
            this.openPopUp('lessThanZero');
            break;
        }
      }
    );
  }

  openPopUp(error) {
    switch(error) {
      case 'moreThanMax':
        this.moreThanMax = true;

        setTimeout(() => {
        this.moreThanMax = false;
        }, this.intervalTime);
        break;
      case 'lessThanZero':
        this.lessThanZero = true;

        setTimeout(() => {
        this.lessThanZero = false;
        }, this.intervalTime);
        break;
    }
  }

  _getImageUrl(type) {
    return getImageUrl(type);
  }

  _getValue(code) {
    return getValue(code);
  }

  resetCashbox() {
    this.depositCashbox = new CashboxWithoutPapers();
  }

  onClickDepositVouchers() {
    this.depositToBankService.depositVousher = this.depositVouchers.nativeElement.checked;
  }

  onSubmit() {
    this.depositSum = this.depositCashbox.getSum();

    this.loginService.checkLogin().subscribe(
      connectStatus => {
        if (connectStatus == true) {
          const shift = JSON.parse(sessionStorage.getItem(sessionKeys.shift));
          if (this.depositToBankService.depositVousher) {
            this.depositCashbox[cashboxDataType.studentVoucher] = this.currCashbox[cashboxDataType.studentVoucher];
          }
          let minusDepositCashbox = new Cashbox();
          minusDepositCashbox.decrease(this.depositCashbox);
          const depositData = minusDepositCashbox.convertToServerFormat();
          this.http
              .post<ServerResponse>(
                  urls.ADD_DRIVER_CASHBOX,
                  {
                    data: {
                      deviceIdCashier: sessionStorage.getItem(sessionKeys.cashboxId),
                      idNumberDriver: serverConstant.general.NULL,
                      shiftIdCashier: shift.Shift_id || shift.shiftId,
                      driverCashboxData: depositData,
                      shifts: [],
                      jsonObjRes: true,
                      comment: "",
                      actionTypeTransaction: serverConstant.actionTypeTransaction.DEPOSIT_TRANSACTION,
                      timestamp: new Date().getTime()
                    },
                    sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
                  })
                  .subscribe(res => {
                    if (res.statusCode == statusCode.OK) {
                      this.storeService.updateCashBox().subscribe(
                        () => {
                          this.currCashbox = this.storeService.getCashbox();
                        }
                      );
                      this.transactionId = res.data[0].TransactionId;
                      this.openInfoModal();
                    }
                  })
          this.resetCashbox();
        }
      }
    );
  }

  openInfoModal() {
    this.storeService.getTranslation(translation => {
      let infoModalSubTitle;
      if (this.depositSum > 0 && this.depositToBankService.depositVousher) {
        infoModalSubTitle = [
          `${this.depositSum} ${this.storeService.currencySymbol}`,
          `${translation.GENERAL.AND}${this.currCashbox[cashboxDataType.studentVoucher]} ${translation.GENERAL.VOUCHERS} ${translation.PAGES.MAIN.DEPOSIT_TO_BANK.WERE_DEPOSIT_TO_BANK}`
        ];
      }
      else if (this.depositSum == 0) {
        infoModalSubTitle = [
          `${this.currCashbox[cashboxDataType.studentVoucher]} ${translation.GENERAL.VOUCHERS} ${translation.PAGES.MAIN.DEPOSIT_TO_BANK.WERE_DEPOSIT_TO_BANK}`
        ];
      }
      else {
        infoModalSubTitle = [
          `${this.depositSum} ${this.storeService.currencySymbol} ${translation.PAGES.MAIN.DEPOSIT_TO_BANK.WERE_DEPOSIT_TO_BANK}`
        ];
      }
      this.infoModalIcon = "assets/icons/infoIcons/ok.svg";
      this.infoModalTitle = translation.PAGES.MAIN.DEPOSIT_TO_BANK.DEPOSIT_TO_BANK_SUCCESS;
      this.infoModalSubTitle = infoModalSubTitle;
      this.infoModalBtnTitle = translation.PAGES.MAIN.PRINT_REFERENCE;
      this.infoModalHr = true;
      this.infoModalBtnLeftText = translation.GENERAL.YES;
      this.infoModalBtnLeftClickFun = this.printReceipt;
      this.infoModalBtnRightText = translation.GENERAL.NO;
      this.infoModalBtnRightClickFun = this.navigateToHome;
      this.infoModalBackgroundClickFunction = null;

      this.modalService.open('info-modal-deposit-to-bank');
    });
  }

  navigateToHome = () => {
    this.depositToBankService.depositVousher = false;
    this.router.navigate(['./main/home']);
  }

  printReceipt = () => {
    this.storeService.getTranslation(translation => {
      this.storeService.printReceipt(this.transactionId, translation.PAGES.MAIN.DEPOSIT_TO_BANK.PAGE_TITLE);
      this.navigateToHome();
    });
  }

  ngOnDestroy() {
    this.amountInputChangedSub.unsubscribe();
  }
}
