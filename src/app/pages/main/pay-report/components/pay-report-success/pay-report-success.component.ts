import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { payReportResponse, PayReportResponse } from '../../pay-report-response.modal';

@Component({
  selector: 'app-pay-report-success',
  templateUrl: './pay-report-success.component.html',
  styleUrls: ['./pay-report-success.component.css']
})
export class PayReportSuccessComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    const selfWindow = window;
    const parentWindow = window.parent;
    
    if (selfWindow === parentWindow) {
      return;
    }

    this.route.queryParams
      .subscribe(params => {
        parentWindow.postMessage(
          {
            from: payReportResponse,
            result: PayReportResponse.SUCCESS,
            referenceId: params.lowprofilecode
          },
          window.location.origin
        );
        
      }
    );
  }
}
