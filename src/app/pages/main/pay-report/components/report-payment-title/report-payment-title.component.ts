import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-report-payment-title',
  templateUrl: './report-payment-title.component.html',
  styleUrls: ['./report-payment-title.component.css']
})
export class ReportPaymentTitleComponent {
  @Input('price') price;
}
