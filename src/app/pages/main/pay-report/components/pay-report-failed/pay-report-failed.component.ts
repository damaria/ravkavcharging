import { Component, OnInit } from '@angular/core';
import { payReportResponse, PayReportResponse } from '../../pay-report-response.modal';

@Component({
  selector: 'app-pay-report-failed',
  templateUrl: './pay-report-failed.component.html',
  styleUrls: ['./pay-report-failed.component.css']
})
export class PayReportFailedComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    const selfWindow = window;
    const parentWindow = window.parent;
    
    if (selfWindow === parentWindow) {
      return;
    }
    parentWindow.postMessage(
      {
        from: payReportResponse,
        result: PayReportResponse.FAILED,
      },
      window.location.origin
    );
  }
}
