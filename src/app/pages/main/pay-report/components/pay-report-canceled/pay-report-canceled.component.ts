import { Component, OnInit } from '@angular/core';
import { payReportResponse, PayReportResponse } from '../../pay-report-response.modal';

@Component({
  selector: 'app-pay-report-canceled',
  templateUrl: './pay-report-canceled.component.html',
  styleUrls: ['./pay-report-canceled.component.css']
})
export class PayReportCanceledComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    const selfWindow = window;
    const parentWindow = window.parent;
    
    if (selfWindow === parentWindow) {
      return;
    }
    parentWindow.postMessage(
      {
        from: payReportResponse,
        result: PayReportResponse.CANCELED,
      },
      window.location.origin
    );
  }

}
