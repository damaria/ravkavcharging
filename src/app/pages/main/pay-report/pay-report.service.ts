import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Moment } from "moment";
import { tap } from "rxjs/operators";
import { Card } from "src/app/shared/components/credit-card-form/Card.modal";
import { FineStatusCode, ServerResponse } from "src/app/shared/constants/server";
import { sessionKeys } from "src/app/shared/constants/sessionKey";
import { urls } from "src/app/shared/constants/URLs";
import { StoreService } from "src/app/shared/services/store.service";
import { toEncodedIsoString, toIsoString } from "src/app/shared/utils/date";
import * as serverConstant from '../../../shared/constants/server';

@Injectable({providedIn: 'root'})
export class PayReportService {
    constructor(
        private http: HttpClient,
        private storeService: StoreService) {}

    searchReport(reportId: number, reportDate: Moment) {
        const startDt = reportDate.startOf('day').valueOf();
        const endDt = reportDate.endOf('day').valueOf();
        return this.http
        .post<ServerResponse>(
            urls.GET_PASSENGER_FINE,
            {
                data: {
                    inspectorId: serverConstant.general.NULL,
                    rte: serverConstant.general.NULL,
                    passengerReviewType: serverConstant.general.NULL,
                    vehicleNumber: serverConstant.general.NULL,
                    passengerId: serverConstant.general.NULL,
                    isPaid: serverConstant.general.NULL,
                    fineStatusCodes: serverConstant.general.NULL,
                    registerFineNumber: reportId.toString(),
                    startDt: startDt.toString(),
                    endDt: endDt.toString()
                },
                sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
            })
    }

    sendCashboxTransaction(cashboxData, idNumber) {
        const shift = JSON.parse(sessionStorage.getItem(sessionKeys.shift));

        return this.http
        .post<ServerResponse>(
        urls.ADD_DRIVER_CASHBOX,
        {
            data: {
                deviceIdCashier: sessionStorage.getItem(sessionKeys.cashboxId),
                idNumberDriver: idNumber,
                shiftIdCashier: shift.Shift_id || shift.shiftId,
                driverCashboxData: cashboxData,
                shifts: [],
                jsonObjRes: true,
                comment: "",
                actionTypeTransaction: serverConstant.actionTypeTransaction.PAY_REPORT,
                timestamp: new Date().getTime()
            },
            sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
        })
        .pipe(
            tap(() => {
                this.storeService.updateCashBox().subscribe();
            })
        )
    }

    updatePassengerFine(fineId, driverCashboxTransactionId, paymentReference, paymentType) {
        return this.http
        .post<ServerResponse>(
            urls.UPDATE_PASSENGER_FINE,
            {
                data: {
                    inspectorPassengerFineId: fineId,
                    paymentType: paymentType.toString(),
                    paymentReference: paymentReference, // just on credit pay
                    driverCashboxTransactionId: driverCashboxTransactionId,
                    fineStatusCode: FineStatusCode.PAID,
                    paymentDt: new Date().getTime().toString()
                },
                sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
            })
    }

    getIframeUrl(fineId, timestamp) {
        const isoTimestamp = toIsoString(new Date(timestamp.toString()));
        return this.http
        .post<ServerResponse>(
            urls.GET_CREDIT_URL_37,
            {
                fineId: fineId,
                isoTimestamp: isoTimestamp,
                operatorId: sessionStorage.getItem(sessionKeys.operatorId),
                originUrl: window.location.origin + window.location.pathname + "#"
            })
    }

    sendCreditPay(fineId, timestamp, cardDetails: Card) {
        const isoTimestamp = toEncodedIsoString(new Date(timestamp.toString()));
        const creditCardInfo =  {
            creditCardNumber: cardDetails.cardNumber,
            ownerCivilId: cardDetails.cardName,
            validityMonth: cardDetails.expirationMonth,
            validityYear: cardDetails.expirationYear,
            securityNumber: cardDetails.cvv
        }
        const savedCreditCardInfo =  {
            creditCardNumber: '375514291003454',
            ownerCivilId: '890109629',
            validityMonth: '5',
            validityYear: '2027',
            securityNumber: '950',
        }
        return this.http
        .post<ServerResponse>(
            urls.PAY_FINE,
            {
                creditCardInfo: creditCardInfo,
                fineId: fineId,
                operatorId: sessionStorage.getItem(sessionKeys.operatorId),
                isoTimestamp: isoTimestamp
            })
    }
}
