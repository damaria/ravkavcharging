import { Component, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { cashboxDataType, FineStatusCode, getValue, statusCode } from 'src/app/shared/constants/server';
import { PayReportService } from './pay-report.service';
import { MatStepper } from '@angular/material/stepper';
import { StoreService } from 'src/app/shared/services/store.service';
import { FinePaymentWay } from '../haveRavKav/dialog-rav-kac/components/payment-to-buy-contract/paymentWay';
import { CashboxWithoutPapers } from 'src/app/shared/models/cashboxWithoutPapers.model';
import { AmountInputService, subData } from 'src/app/shared/components/amount-input/amount-input.service';
import { Subscription } from 'rxjs';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { getImageUrl } from 'src/app/shared/utils/cashbox';
import { DisplayMoneyService } from 'src/app/shared/components/display-money/display-money.service';
import { MatDialog } from '@angular/material/dialog';
import { PdfViewerComponent } from 'src/app/shared/components/dialogs/pdf-viewer/pdf-viewer.component';
import { DomSanitizer } from '@angular/platform-browser';
import { SupportCreditPayReport } from 'src/app/shared/fromServer/operatorsConfiguration';
import { CreditCardFormService } from 'src/app/shared/components/credit-card-form/credit-card-form.service';
import { Card } from 'src/app/shared/components/credit-card-form/Card.modal';
import { LoadingModalService } from 'src/app/shared/components/modal/loading-modal/loading-modal.service';
import { payReportResponse, PayReportResponse } from './pay-report-response.modal';
import { Cashbox } from 'src/app/shared/models/cashbox.model';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-pay-report',
  templateUrl: './pay-report.component.html',
  styleUrls: ['./pay-report.component.css']
})
export class PayReportComponent implements OnInit, OnDestroy {
  searchReportForm: FormGroup = new FormGroup({
    reportId: new FormControl(null),
    reportDate: new FormControl(null)
  });
  report = null;
  readRules = false;
  PaymentWay = FinePaymentWay;
  paymentWay = FinePaymentWay.CASH;

  currCashbox = new CashboxWithoutPapers();
  paymentCashbox = new CashboxWithoutPapers();
  paybackCashbox = new CashboxWithoutPapers();

  amountInputChangedSub: Subscription;

  infoModalBackgroundClickFunction;
  infoModalIcon;
  infoModalHr;
  infoModalTitle;
  infoModalSubTitle;
  infoModalBtnTitle;
  infoModalBtnRightText;
  infoModalBtnLeftText;
  infoModalBtnRightClickFun;
  infoModalBtnLeftClickFun;
  infoModalError;

  moreThanMax = false;
  lessThanZero = false;
  fineProblem = false;
  fineProblemDetails;
  mustReadRules = false;
  payFailed = false;
  payCanceled = false;
  intervalTime = 3000;

  getImageUrl = getImageUrl;
  getValue = getValue;
  parseFloat = parseFloat;
  SupportCreditPayReport = SupportCreditPayReport;

  paybackNeeded;
  hasRules;
  supportCreditPayReport: SupportCreditPayReport;
  updateFineFailed = false;
  creditPayIframeUrl = null;
  creditPayIframeUrlError = null;
  isLoadingIframe = true;
  submitCreditCardSub;
  creditCardError = "";
  creditCardReferenceId;
  driverCashboxTransactionId;
  canCloseDialog = true;
  isReset;
  maxDate = new Date();

  @ViewChild('stepper') stepper: MatStepper;

  @HostListener('window:message',['$event'])
  onMessage(e) {
    if (e.data && e.data.from && e.data.from == payReportResponse) {
      switch(e.data.result) {
        case PayReportResponse.SUCCESS:
          this.creditCardReferenceId = e.data.referenceId;
          this.sendCashboxTransaction();
          break;
        case PayReportResponse.FAILED:
          this.getIframeUrl();
          this.openPopUp("payFailed");
          break;
        case PayReportResponse.CANCELED:
          this.getIframeUrl();
          this.openPopUp("payCanceled");
          break;
      }
    }
  }

  constructor(
    private router: Router,
    private payReportService: PayReportService,
    public storeService: StoreService,
    private amountInputService: AmountInputService,
    private modalService: ModalService,
    private displayMoneyService: DisplayMoneyService,
    public dialog: MatDialog,
    private sanitizer: DomSanitizer,
    private creditCardFormService: CreditCardFormService,
    private loadingModalService: LoadingModalService,
    private translateService: TranslateService) { }

  ngOnInit(): void {
    this.paymentCashbox = new CashboxWithoutPapers();
    this.amountInputChangedSub = this.amountInputService.inputChanged.subscribe(
      (data: subData) => {
        if (data.info == 'payment') {
          switch (data.operation) {
            case 'increase':
              this.paymentCashbox[data.type]++;
              break;
            case 'decrease':
              this.paymentCashbox[data.type]--;
              break;
            case 'change':
              this.paymentCashbox[data.type] = data.amount;
              break;
          }
        }
        if (data.info == 'payback') {
          switch(data.operation) {
            case 'increase':
              this.paybackCashbox[data.type]++;
              break;
            case 'decrease':
              this.paybackCashbox[data.type]--;
              break;
            case 'change':
              this.paybackCashbox[data.type] = data.amount;
              break;
          }
        }
        switch(data.operation) {
          case 'moreThanMax':
            this.paybackCashbox[data.type] = data.amount;
            this.openPopUp('moreThanMax');
            break;
          case 'lessThanZero':
            this.openPopUp('lessThanZero');
            break;
        }
      }
    );

    this.submitCreditCardSub = this.creditCardFormService.submit.subscribe(
      (cardDetails: Card) => {
        this.creditCardError = "";
        this.payReportService.sendCreditPay(this.report.registerFineNumber, this.report.timestamp, cardDetails)
        .subscribe({
          next: (res: any) => {
            this.creditCardReferenceId = res.referenceId;
            this.sendCashboxTransaction();
          },
          error: problem => {
            this.loadingModalService.close();
            this.creditCardError = problem.error.display.HE_IL;
          }
        });
      }
    )
    this.hasRules = this.storeService.operatorConfiguration.hasRules;
    this.supportCreditPayReport = this.storeService.operatorConfiguration.supportCreditPayReport;
  }

  finishPayReport = () => {
    this.isReset = false;
    this.openInfoModalPrint();
  }

  closePayReport = () => {
    this.router.navigate(['./main/home']);
  }

  openPopUp(error, info?) {
    switch(error) {
      case 'moreThanMax':
        this.moreThanMax = true;
        setTimeout(() => {
        this.moreThanMax = false;
        }, this.intervalTime);
        break;
      case 'lessThanZero':
        this.lessThanZero = true;
        setTimeout(() => {
        this.lessThanZero = false;
        }, this.intervalTime);
        break;
      case 'fineProblem':
        this.fineProblem = true;
        switch(info) {
          case FineStatusCode.NO_EXIST:
            this.fineProblemDetails = this.translateService.stream('PAGES.MAIN.PAY_REPORT.REPORT_NO_EXIST');
            break;
          case FineStatusCode.PAID:
            this.fineProblemDetails = this.translateService.stream('PAGES.MAIN.PAY_REPORT.REPORT_PAID');
            break;
          case FineStatusCode.CANCELED:
            this.fineProblemDetails = this.translateService.stream('PAGES.MAIN.PAY_REPORT.REPORT_CANCELED');
            break;
          case FineStatusCode.OBJECTION:
            this.fineProblemDetails = this.translateService.stream('PAGES.MAIN.PAY_REPORT.REPORT_OBJECTION');
            break;
        }
        setTimeout(() => {
        this.fineProblem = false;
        }, this.intervalTime);
        break;
      case 'mustReadRules':
        this.mustReadRules = true;
        setTimeout(() => {
        this.mustReadRules = false;
        }, this.intervalTime);
        break;
      case 'payFailed':
        this.payFailed = true;
        setTimeout(() => {
        this.payFailed = false;
        }, this.intervalTime + 2000);
        break;
      case 'payCanceled':
        this.payCanceled = true;
        setTimeout(() => {
        this.payCanceled = false;
        }, this.intervalTime + 2000);
        break;
    }
  }

  invalidForm() {
    return this.searchReportForm.value.reportId == null || this.searchReportForm.value.reportDate == null;
  }

  searchReport() {
    const data = this.searchReportForm.value;
    this.payReportService.searchReport(data.reportId, data.reportDate)
    .subscribe(
      res => {
        if (res.data.length == 0) {
          this.openPopUp('fineProblem', FineStatusCode.NO_EXIST);
        }
        else {
          this.initReport(res.data[0]);
          this.creditPayIframeUrl = null;
          if (this.report.fineStatusCode != FineStatusCode.NEED_TO_PAY) {
            this.openPopUp('fineProblem', this.report.fineStatusCode);
          }
          else {
            this.stepper.next();
          }
        }
      }
    );
  }

  initReport(report){
    this.report = report;
    const mainDate = new Date(report.timestamp + "UTC");
    const time = mainDate.toLocaleDateString();
    const date = mainDate.toLocaleTimeString();
    this.report.timeString = `${date}, ${time}`;
  }

  readTheRules(read: boolean) {
    this.readRules = read;
  }

  nextFromReportDetails(allowed) {
    if (!this.hasRules || allowed) {
      if (this.supportCreditPayReport == SupportCreditPayReport.WITH_IFRAME && this.creditPayIframeUrl == null) {
        this.getIframeUrl();
      }
      this.stepper.next();
    }
    else {
      this.openPopUp('mustReadRules');
    }
  }

  getIframeUrl() {
    this.payReportService.getIframeUrl(this.report.registerFineNumber, this.report.timestamp)
    .subscribe({
      next: (res: any) => {
        this.creditPayIframeUrlError = null;
        this.creditPayIframeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(res.url);
      },
      error: problem => {
        this.loadingModalService.close();
        this.isLoadingIframe = false;
        this.creditPayIframeUrlError = problem.error.display.HE_IL;
      }
    });
  }

  payReport() {
    this.paybackNeeded = false;

    const payback = this.getPayback();

    if (parseFloat(payback) < 0) {
      this.openNoEnoughMoneyModal();
      return;
    }

    else if (parseFloat(payback) > 0) {
      this.paybackNeeded = true;
      this.currCashbox = this.storeService.getCashbox();
      this.currCashbox.increase(this.paymentCashbox);
      if (!this.hasMoneyForPayback(payback)) {
        this.openNoPaybackModal();
        return;
      }
      else {
        this.openPaybackModal();
        return;
      }
    }

    this.sendCashboxTransaction();
  }

  sendCashboxTransaction() {
    this.payReportService.sendCashboxTransaction(this.getCashboxData(), this.report.passengerId)
    .subscribe((res: any) => {
      this.driverCashboxTransactionId = res.data[0].TransactionId;
      this.payReportService.updatePassengerFine(this.report.inspectorPassengerFineId, this.driverCashboxTransactionId, this.creditCardReferenceId, this.paymentWay)
      .subscribe((res) => {
        if (res.statusCode != statusCode.OK) {
          this.updateFineFailed = true;
        }
        this.stepper.next();
        this.canCloseDialog = false;
      });
    });
  }

  openPaybackModal() {
    this.modalService.open('pay-report-payback-modal');
  }

  hasMoneyForPayback = (remaindPayback) => {
    let currCashboxCopy = this.storeService.getCashbox();

    let coinType = 10; //200 nis
    while (coinType > 0) {
      const coinValue = getValue(coinType)/100;
      while (remaindPayback >= coinValue && currCashboxCopy[coinType] > 0) {
        remaindPayback -= coinValue;
        remaindPayback = parseFloat(remaindPayback.toFixed(2));
        currCashboxCopy[coinType]--;
      }
      if (remaindPayback == 0) {
        return true;
      }
      coinType--;
    }
    return false;
  }

  openNoPaybackModal() {
    this.storeService.getTranslation(translation => {
      this.infoModalIcon = "assets/icons/infoIcons/error.svg";
      this.infoModalTitle = translation.PAGES.MAIN.PAY_REPORT.NO_EXCESS;
      this.infoModalSubTitle = null;
      this.infoModalHr = null;
      this.infoModalBtnTitle = null;
      this.infoModalBtnLeftText = translation.GENERAL.OK;
      this.infoModalBtnLeftClickFun = this.closeInfoModal;
      this.infoModalBtnRightText = null;
      this.infoModalBtnRightClickFun = null;
      this.infoModalBackgroundClickFunction = this.closeInfoModal;

      this.modalService.open('info-modal-pay-report');
    });
  }

  selectedTabChange(event) {
    if (event.index == 0) {
      this.paymentWay = FinePaymentWay.CASH;
      this.isLoadingIframe = true;
    }
    else {
      this.paymentWay = FinePaymentWay.CREDIT;
    }
  }

  openNoEnoughMoneyModal() {
    this.storeService.getTranslation(translation => {
      const _translation = translation.PAGES.MAIN.PAY_REPORT;
      this.infoModalIcon = "assets/icons/infoIcons/error.svg";
      this.infoModalTitle = _translation.NOT_ALL_BROUGHT;
      this.infoModalSubTitle = null;
      this.infoModalHr = false;
      this.infoModalBtnLeftText = _translation.RETURN_TO_COMPLETE;
      this.infoModalBtnLeftClickFun = this.closeInfoModal;
      this.infoModalBackgroundClickFunction = this.closeInfoModal;

      this.modalService.open('info-modal-pay-report');
    });
  }

  closeInfoModal = () => {
    this.modalService.close('info-modal-pay-report');
  }

  getPayback() {
    const alreadyPayed = this.paymentCashbox.getSum();
    const needToPay = parseFloat(this.report.price);
    const givenPayback = this.paybackCashbox.getSum();
    const payback = alreadyPayed - needToPay - givenPayback;
    return payback.toFixed(2);
  }

  finishPayback() {
    this.modalService.close('pay-report-payback-modal');
    this.sendCashboxTransaction();
  }

  getCashboxData() {
    switch(this.paymentWay) {
      case FinePaymentWay.CASH:
        let helperCashbox = new CashboxWithoutPapers();
        helperCashbox.increase(this.paymentCashbox);
        helperCashbox.decrease(this.paybackCashbox);
        return helperCashbox.convertToServerFormat();

      case FinePaymentWay.CREDIT:
        let helperCashbox1 = new Cashbox();
        helperCashbox1[cashboxDataType.credit] = this.getPrice() * (100 / this.storeService.CreditValue);
        return helperCashbox1.convertToServerFormat();
    }
  }

  getPrice() {
    return parseFloat(this.report.price);
  }

  resetPayReport() {
    this.currCashbox = new CashboxWithoutPapers();
    this.paybackCashbox = new CashboxWithoutPapers();
    this.paymentCashbox = new CashboxWithoutPapers();
    this.updateFineFailed = false;
    this.creditCardReferenceId = null;
    this.displayMoneyService.reset.next(null);
    this.stepper.reset();
    this.canCloseDialog = true;
    this.isReset = true;
    this.openInfoModalPrint();
  }

  openRules() {
    this.dialog.open(PdfViewerComponent);
  }

  openInfoModalPrint = () => {
    this.storeService.getTranslation(translation => {
      this.infoModalTitle = translation.PAGES.MAIN.PRINT_REFERENCE;
      if (this.isReset) {
        this.infoModalTitle += ` ${translation.PAGES.MAIN.PAY_REPORT.FOR_PREVIOUS_REPORT}`
      }
      this.infoModalIcon = "assets/icons/infoIcons/ok.svg";
      this.infoModalBtnLeftText = translation.GENERAL.YES;
      this.infoModalBtnLeftClickFun = this.printReceipt;
      this.infoModalBtnRightText = translation.GENERAL.NO;
      this.infoModalBtnRightClickFun = this.isReset ? this.closeInfoModal : this.closePayReport;
      this.infoModalBackgroundClickFunction = null;

      this.modalService.open('info-modal-pay-report');
    });
  }

  printReceipt = () => {
    this.storeService.getTranslation(translation => {
      this.storeService.printFineReceipt(this.report.inspectorPassengerFineId, translation.PAGES.MAIN.PAY_REPORT.PAGE_TITLE);
      this.isReset ? this.closeInfoModal() : this.closePayReport();
    });
  }

  ngOnDestroy(): void {
    this.amountInputChangedSub.unsubscribe();
    this.submitCreditCardSub.unsubscribe();
  }
}
