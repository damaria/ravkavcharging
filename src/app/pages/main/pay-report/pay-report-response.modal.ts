export enum PayReportResponse{
    SUCCESS,
    FAILED,
    CANCELED
}

export const payReportResponse = 'payReportResponse';