import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AmountInputService, subData } from 'src/app/shared/components/amount-input/amount-input.service';
import { getValue, ServerResponse, statusCode } from 'src/app/shared/constants/server';
import { CashboxWithoutPapers } from 'src/app/shared/models/cashboxWithoutPapers.model';
import { getImageUrl } from 'src/app/shared/utils/cashbox';
import { LoginService } from '../../login/login.service';
import { urls } from '../../../shared/constants/URLs';
import * as serverConstant from '../../../shared/constants/server';
import { StoreService } from 'src/app/shared/services/store.service';
import { Cashbox } from 'src/app/shared/models/cashbox.model';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';
import { Router } from '@angular/router';
import { ModalService } from 'src/app/shared/components/modal/modal.service';

@Component({
  selector: 'app-changing',
  templateUrl: './changing.component.html',
  styleUrls: ['./changing.component.css']
})
export class ChangingComponent implements OnInit, OnDestroy {
  currCashbox = new Cashbox();
  cashboxIn = new CashboxWithoutPapers();
  cashboxOut = new CashboxWithoutPapers();
  amountInputChangedSub: Subscription;
  moreThanMax = false;
  lessThanZero = false;
  changingNoEqual = false;
  intervalTime = 3000;

  infoModalBackgroundClickFunction;
  infoModalIcon;
  infoModalHr;
  infoModalTitle;
  infoModalSubTitle;
  infoModalBtnTitle;
  infoModalBtnRightText;
  infoModalBtnLeftText;
  infoModalBtnRightClickFun;
  infoModalBtnLeftClickFun;
  infoModalError;

  transactionId;

  constructor(
    private amountInputService: AmountInputService,
    private loginService: LoginService,
    private http: HttpClient,
    private storeService: StoreService,
    private modalService: ModalService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.currCashbox = this.storeService.getCashbox();
    if (!this.currCashbox) {
      this.loginService.checkLogin().subscribe(
        connectStatus => {
          if (connectStatus == true) {
            this.storeService.updateCashBox().subscribe(
              () => {
                this.currCashbox = this.storeService.getCashbox();
              }
            );
          }
        }
      );
    }

    this.amountInputChangedSub = this.amountInputService.inputChanged.subscribe(
      (data: subData) => {
        switch(data.operation) {
          case 'increase':
            data.payload == 'in' ?
            this.cashboxIn[data.type]++ :
            this.cashboxOut[data.type]++;
            break;
          case 'decrease':
            data.payload == 'in' ?
            this.cashboxIn[data.type]-- :
            this.cashboxOut[data.type]--;
            break;
          case 'change':
            data.payload == 'in' ?
            this.cashboxIn[data.type] = data.amount :
            this.cashboxOut[data.type] = data.amount;
            break;
          case 'moreThanMax':
            this.cashboxOut[data.type] = data.amount;
            this.openPopUp('moreThanMax');
            break;
          case 'lessThanZero':
            this.openPopUp('lessThanZero');
            break;
          case 'changingNoEqual':
            this.openPopUp('changingNoEqual');
            break;
        }
      }
    );
  }

  _getImageUrl(type) {
    return getImageUrl(type);
  }

  _getValue(code) {
    return getValue(code);
  }

  resetCashboxes() {
    this.cashboxIn = new CashboxWithoutPapers();
    this.cashboxOut = new CashboxWithoutPapers();
  }

  openPopUp(error) {
    switch(error) {
      case 'moreThanMax':
        this.moreThanMax = true;

        setTimeout(() => {
        this.moreThanMax = false;
        }, this.intervalTime);
        break;
      case 'lessThanZero':
        this.lessThanZero = true;

        setTimeout(() => {
        this.lessThanZero = false;
        }, this.intervalTime);
        break;
      case 'changingNoEqual':
        this.changingNoEqual = true;

        setTimeout(() => {
        this.changingNoEqual = false;
        }, this.intervalTime);
        break;
    }
  }

  onSubmit() {
    if (this.cashboxIn.getSum() != this.cashboxOut.getSum()) {
      this.openPopUp('changingNoEqual');
    }
    else {
      this.loginService.checkLogin().subscribe(
        connectStatus => {
          if (connectStatus == true) {
            let updatedCashbox = new CashboxWithoutPapers();
            updatedCashbox.increase(this.cashboxIn);
            updatedCashbox.decrease(this.cashboxOut);
            const cashboxData = updatedCashbox.convertToServerFormat();
            const shift = JSON.parse(sessionStorage.getItem(sessionKeys.shift));

            this.http
                .post<ServerResponse>(
                    urls.ADD_DRIVER_CASHBOX,
                    {
                      data: {
                        deviceIdCashier: sessionStorage.getItem(sessionKeys.cashboxId),
                        idNumberDriver: serverConstant.general.NULL,
                        shiftIdCashier: shift.Shift_id || shift.shiftId,
                        driverCashboxData: cashboxData,
                        shifts: [],
                        jsonObjRes: true,
                        comment: "",
                        actionTypeTransaction: serverConstant.actionTypeTransaction.FIX_CASHBOX_AMOUNT_PROACTIVELY,
                        timestamp: new Date().getTime()
                      },
                      sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
                    })
                    .subscribe(res => {
                      if (res.statusCode == statusCode.OK) {
                        this.storeService.updateCashBox().subscribe(
                          () => {
                            this.currCashbox = this.storeService.getCashbox();
                          }
                        );
                        this.transactionId = res.data[0].TransactionId;
                        this.openInfoModal();
                      }
                    })
            this.resetCashboxes();
          }
        }
      );
    }
  }

  openInfoModal() {
    this.storeService.getTranslation(translation => {
      this.infoModalIcon = "assets/icons/infoIcons/ok.svg";
      this.infoModalTitle = translation.PAGES.MAIN.CHANGING.CHANGE_SUCCESS;
      this.infoModalBtnTitle = translation.PAGES.MAIN.PRINT_REFERENCE;
      this.infoModalHr = true;
      this.infoModalBtnLeftText = translation.GENERAL.YES;
      this.infoModalBtnRightText = translation.GENERAL.NO;
      this.infoModalBtnLeftClickFun = this.printReceipt;
      this.infoModalBtnRightClickFun = this.navigateToHome;
      this.infoModalBackgroundClickFunction = null;

      this.modalService.open('info-modal-changing');
    });
  }

  navigateToHome = () => {
    this.router.navigate(['./main/home']);
  }

  printReceipt = () => {
    this.storeService.getTranslation(translation => {
      const _translation = translation.PAGES.MAIN.CHANGING.PAGE_TITLE;
      this.storeService.printReceipt(this.transactionId, _translation);
      this.navigateToHome();
    });
  }

  ngOnDestroy() {
    this.amountInputChangedSub.unsubscribe();
  }
}
