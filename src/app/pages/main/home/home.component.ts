import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { StoreService } from 'src/app/shared/services/store.service';
import { LoginService } from '../../login/login.service';
import { MainService } from '../main.service';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';
import { webSocketDefaultPort } from 'src/app/shared/constants/cardReader';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  events;
  todayString = this.datePipe.transform(new Date(), 'fullDate');
  needToUpdateEventsSub: Subscription;
  onBeforeUnloadCallback = this.onBeforeUnload.bind(this);

  infoModalIcon = "assets/icons/infoIcons/warning.svg";
  infoModalHr;
  infoModalTitle;
  infoModalSubTitle;
  infoModalBtnTitle;
  infoModalBtnRightText;
  infoModalBtnLeftText;
  infoModalBtnRightClickFun;
  infoModalBtnLeftClickFun;
  infoModalError;
  @ViewChild('port') port: ElementRef;

  constructor(
    private storeService: StoreService,
    public mainService: MainService,
    private loginService: LoginService,
    public translateService: TranslateService,
    private datePipe: DatePipe,
    private modalService: ModalService
  ) { }

  onBeforeUnload(event: BeforeUnloadEvent) {
    if (this.needToPreventNavigation()) {
      event.preventDefault();
      this.openInfoModal();
      setTimeout(() => {
        this.closeInfoModal();
      }, 10);
      const fallbackMessage = "Changes you made may not be saved.";
      event.returnValue = fallbackMessage;
      return fallbackMessage;
    }
  }

  ngOnInit(): void {
    this.storeService.getTranslation(translation => {
      this.infoModalTitle = translation.GENERAL.WARNING;
      this.infoModalBtnRightText = translation.GENERAL.OK;
      this.infoModalSubTitle = [translation.GENERAL.MIDDLE_OF_PROCESS];
    });

    this.attachBeforeUnloadListener();
    this.updateEvents();

    this.needToUpdateEventsSub = this.mainService.needToUpdateEvents.subscribe(() => {
      this.updateEvents();
    });

    this.translateService.onLangChange.subscribe(() => {
      this.todayString = this.datePipe.transform(new Date(), 'fullDate');
    });
  }

  updateEvents() {
    this.loginService.checkLogin().subscribe(
      connectStatus => {
        if (connectStatus == true) {
          this.storeService.getEventLog().subscribe(
            events => {
              this.events = events;
            }
          )
        }
      }
    );
  }

  needToPreventNavigation() {
    return this.storeService.middleOfProcess.value;
  }

  openInfoModal = () => {
    this.infoModalBtnRightClickFun = this.closeInfoModal;
    this.modalService.open('info-modal-home');
  }

  closeInfoModal = () => {
    this.modalService.close('info-modal-home');
  }

  attachBeforeUnloadListener() {
    window.addEventListener("beforeunload", this.onBeforeUnloadCallback);
  }

  dettachBeforeUnloadListener() {
    window.removeEventListener("beforeunload", this.onBeforeUnloadCallback);
  }

  ngAfterViewInit (): void {
    let port = localStorage.getItem(sessionKeys.port);

    if (!port) {
      port = webSocketDefaultPort;
      localStorage.setItem(sessionKeys.port, port);
    }
    
    this.port.nativeElement.value = port;
    this.mainService.enterToHomePage.next(port);

    this.listenToPortInput();
  }

  listenToPortInput() {
    this.port.nativeElement.addEventListener('change', () => {
      const updatedPort = this.port.nativeElement.value;
      localStorage.setItem(sessionKeys.port, updatedPort);
    });
  }

  ngOnDestroy(): void {
    this.needToUpdateEventsSub.unsubscribe();
    this.dettachBeforeUnloadListener();
  }
}
