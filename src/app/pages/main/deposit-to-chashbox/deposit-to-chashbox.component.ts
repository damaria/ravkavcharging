import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { CashboxWithoutPapers } from 'src/app/shared/models/cashboxWithoutPapers.model';
import { getValue, ServerResponse, statusCode } from '../../../shared/constants/server';
import { AmountInputService, subData } from 'src/app/shared/components/amount-input/amount-input.service';
import { HttpClient } from '@angular/common/http';
import { LoginService } from '../../login/login.service';
import { urls } from '../../../shared/constants/URLs';
import * as serverConstant from '../../../shared/constants/server';
import { StoreService } from 'src/app/shared/services/store.service';
import { getImageUrl } from 'src/app/shared/utils/cashbox';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-deposit-to-chashbox',
  templateUrl: './deposit-to-chashbox.component.html',
  styleUrls: ['./deposit-to-chashbox.component.css']
})
export class DepositToChashboxComponent implements OnInit, OnDestroy {
  depositCashbox: CashboxWithoutPapers;
  amountInputChangedSub: Subscription;
  depositSum;

  infoModalBackgroundClickFunction;
  infoModalIcon;
  infoModalHr;
  infoModalTitle;
  infoModalSubTitle;
  infoModalBtnTitle;
  infoModalBtnRightText;
  infoModalBtnLeftText;
  infoModalBtnRightClickFun;
  infoModalBtnLeftClickFun;
  infoModalError;

  transactionId;
  reasonOfDepositToCashbox;

  @ViewChild('checkoutReason') checkoutReason: ElementRef;
  @ViewChild('f') f: NgForm;

  constructor(
    private amountInputService: AmountInputService,
    private http: HttpClient,
    private loginService: LoginService,
    private storeService: StoreService,
    private modalService: ModalService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.reasonOfDepositToCashbox = this.storeService.operatorConfiguration.reasonOfDepositToCashbox;
    this.depositCashbox = new CashboxWithoutPapers();

    this.amountInputChangedSub = this.amountInputService.inputChanged.subscribe(
      (data: subData) => {
        switch(data.operation) {
          case 'increase':
            this.depositCashbox[data.type]++;
            break;
          case 'decrease':
            this.depositCashbox[data.type]--;
            break;
          case 'change':
            this.depositCashbox[data.type] = data.amount;
            break;
        }
      }
    );
  }

  _getImageUrl(type) {
    return getImageUrl(type);
  }

  _getValue(code) {
    return getValue(code);
  }

  resetCashbox() {
    this.depositCashbox = new CashboxWithoutPapers();
  }

  onSubmit() {
    this.depositSum = this.depositCashbox.getSum();

    this.loginService.checkLogin().subscribe(
      connectStatus => {
        if (connectStatus == true) {
          const shift = JSON.parse(sessionStorage.getItem(sessionKeys.shift));
          const depositData = this.depositCashbox.convertToServerFormat();
          this.http
              .post<ServerResponse>(
                  urls.ADD_DRIVER_CASHBOX,
                  {
                    data: {
                      deviceIdCashier: sessionStorage.getItem(sessionKeys.cashboxId),
                      idNumberDriver: serverConstant.general.NULL,
                      shiftIdCashier: shift.Shift_id || shift.shiftId,
                      driverCashboxData: depositData,
                      shifts: [],
                      jsonObjRes: true,
                      comment: this.f.form.value.checkoutReason || '',
                      actionTypeTransaction: serverConstant.actionTypeTransaction.ENTER_MONEY_PROACTIVELY,
                      timestamp: new Date().getTime()
                    },
                    sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
                  })
                  .subscribe(res => {
                    if (res.statusCode == statusCode.OK) {
                      this.storeService.updateCashBox().subscribe();
                      this.transactionId = res.data[0].TransactionId;
                      this.openInfoModal();
                    }
                  })
          this.resetCashbox();
        }
      }
    );
  }

  openInfoModal() {
    this.storeService.getTranslation(translation => {
      this.infoModalIcon = "assets/icons/infoIcons/ok.svg";
      this.infoModalTitle = translation.PAGES.MAIN.DEPOSIT_TO_CASHBOX.DEPOSIT_TO_CASHBOX_SUCCESS;
      this.infoModalSubTitle = [`${this.depositSum} ${this.storeService.currencySymbol} ${translation.PAGES.MAIN.DEPOSIT_TO_CASHBOX.WERE_DEPOSIT_TO_CASHBOX}`];
      this.infoModalBtnTitle = translation.PAGES.MAIN.PRINT_REFERENCE;
      this.infoModalHr = true;
      this.infoModalBtnLeftText = translation.GENERAL.YES;
      this.infoModalBtnLeftClickFun = this.printReceipt;
      this.infoModalBtnRightText = translation.GENERAL.NO;
      this.infoModalBtnRightClickFun = this.navigateToHome;
      this.infoModalBackgroundClickFunction = null;

      this.modalService.open('info-modal-deposit-to-cashbox');
    });
  }

  navigateToHome = () => {
    this.router.navigate(['./main/home']);
  }

  printReceipt = () => {
    this.storeService.getTranslation(translation => {
      this.storeService.printReceipt(this.transactionId, translation.PAGES.MAIN.DEPOSIT_TO_CASHBOX.PAGE_TITLE);
      this.navigateToHome();
    });
  }

  ngOnDestroy() {
    this.amountInputChangedSub.unsubscribe();
  }
}
