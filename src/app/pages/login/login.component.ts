import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { of, Subscription } from 'rxjs';
import { StocktakingStatus } from 'src/app/shared/constants/driverBox';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';
import { StoreService } from 'src/app/shared/services/store.service';
import { ModalService } from '../../shared/components/modal/modal.service';
import { LoginPopUpService } from './components/login-pop-up/login-pop-up.service';
import { SelectCashboxModalService } from './components/select-cashbox-modal/select-cashbox-modal.service';
import { LoginService } from './login.service';
import { DescriptionNoOk, LoginResponse, LoginResponseType } from './loginResponse';
import * as serverConstant from '../../shared/constants/server';
import { cashboxStatus } from '../../shared/constants/server';
import { devMode } from 'src/app/shared/constants/URLs';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  @ViewChild('loginForm') loginForm: NgForm;
  @ViewChild('loginAdminForm') loginAdminForm: NgForm;

  isLoading;
  isLoadingAdmin;
  error;
  errorAdmin;
  devices;
  userOpenSessions;
  otherOpenSessions;
  dayClosed;
  noClusterId
  cashboxLocked;
  adminLogin;
  adminLoginData;
  cashboxSelected = new Subscription();
  popUpClosed = new Subscription();

  sessionStorage = sessionStorage;
  sessionKeys = sessionKeys;

  constructor(
    private modalService: ModalService,
    private selectCashboxModalService: SelectCashboxModalService,
    public loginService: LoginService,
    public loginPopUpService: LoginPopUpService,
    private route: ActivatedRoute,
    private router: Router,
    private storeService: StoreService,
    private translateService: TranslateService
    ) { }

  ngOnInit(): void {
    this.isLoading = false;
    this.resetValues();

    this.route.params.subscribe(
      (params: Params) => {
        const operatorId = params['operatorId'];
        this.sessionStorage.setItem(sessionKeys.operatorId, operatorId);
        this.storeService.getOperatorConfiguration(operatorId);
      }
    );
    this.cashboxSelected = this.selectCashboxModalService.cashboxSelected.subscribe(
      (cashbox: any) => {
        this.isLoading = false;
        this.loginPopUpService.closed.next(null);
        if (cashbox.Status_id == cashboxStatus.LOCKED) {
          this.cashboxLocked = true;
        }
        else if (!cashbox.clusterId) {
          this.noClusterId = true;
        }
        else {
          this.onSelectCashbox(cashbox);
        }
      }
    );

    this.popUpClosed = this.loginPopUpService.closed.subscribe(
      () => {
        this.userOpenSessions = null;
        this.otherOpenSessions = null;
        this.dayClosed = null;
        this.cashboxLocked = null;
        this.noClusterId = null;
      }
    );

    if (this.route.snapshot.queryParamMap.get('sessionNoValid')) {
      this.error = this.translateService.stream('PAGES.LOGIN.LOGIN_AGAIN');
    }
    this.removeQueryParams();
    sessionStorage.removeItem(sessionKeys.cashboxLocked);
  }

  checkIfReturnFromBreak() {
    this.error = null;
    this.isLoading = true;

    this.loginService.isReturnFromBreak(this.loginForm.value.email, this.loginForm.value.password)
    .subscribe({
      next: (res: LoginResponse) => this.loginSubscribeHandler(res),
      error: error => this.loginSubscribeErrorHandler(error)
    });
  }

  login() {
    this.isLoading = true;
    this.loginService.login(this.loginForm.value.email, this.loginForm.value.password)
    .subscribe({
      next: (res: LoginResponse) => this.loginSubscribeHandler(res),
      error: error => this.loginSubscribeErrorHandler(error)
    });
  }

  returnFromBreak() {
    this.isLoading = true;
    this.loginService.addEventLog(serverConstant.eventLogCodes.RETURN_FROM_BREAK).subscribe();
    this.enterToCashbox();
  }

  loginAdmin() {
    this.errorAdmin = null;
    this.isLoadingAdmin = true;
      this.loginService.adminLogin(this.loginAdminForm.value.email, this.loginAdminForm.value.password)
      .subscribe({
        next: (res: LoginResponse) => this.loginSubscribeHandler(res),
        error: error => this.loginSubscribeErrorHandler(error)
      });
  }

  onSelectCashbox(cashboxDetails) {
    this.loginService.checkOpenSessions(cashboxDetails)
    .subscribe({
      next: (res: LoginResponse) => this.loginSubscribeHandler(res),
      error: (error) => this.loginSubscribeErrorHandler(error)
    });
  }

  getIntoCheckBox() {
    this.isLoading = true;
    this.resetValues();

    this.loginService.getIntoCheckBox()
    .subscribe({
      next: (res: LoginResponse) => this.loginSubscribeHandler(res),
      error: (error) => this.loginSubscribeErrorHandler(error)
    });
  }

  logoutFromSessions(openSessions) {
    this.isLoading = true;
    this.modalService.close('select-box-modal');
    this.resetValues();

    openSessions.forEach(session => {
      this.loginService.logoutFromSession(session)
      .subscribe(
        (res: LoginResponse) => {
          this.loginSubscribeHandler(res),
          error => this.loginSubscribeErrorHandler(error)
        }
      );
    });
  }

  enterToCashbox() {
    const stocktakingStatus = sessionStorage.getItem(sessionKeys.stocktakingStatus);
    if (stocktakingStatus == StocktakingStatus.START_SHIFT) {
      //if the app in test mode OR the operator allow to skip - can skip stocktaking
      if (devMode || this.storeService.operatorConfiguration.canSkipStocktakingOnStartShift) {
        this.sessionStorage.setItem(sessionKeys.stocktakingStatus, StocktakingStatus.MIDDLE_SHIFT);
      }
      this.router.navigate(['/main/stocktaking']);
    }
    else {
      this.storeService.updateCashBox().subscribe(
        () => {
          this.router.navigate(['/main']);
        }
      );
    }
  }

  closeLoginPopUp() {
    this.loginPopUpService.closed.next(null);
  }

  loginSubscribeHandler (res: LoginResponse) {
    if (res) {
      this.isLoading = false;
      this.isLoadingAdmin = false;

      switch(res.type) {
        case LoginResponseType.logoutFromSessionSuccessfully:
          this.getIntoCheckBox();
          break;
        case LoginResponseType.noOperatorId:
          this.error = this.translateService.stream('PAGES.LOGIN.NO_OPERATOR_ID')
          break;
        case LoginResponseType.descriptionNoOk:
          switch(res.data) {
            case DescriptionNoOk.login:
              this.error = this.translateService.stream('PAGES.LOGIN.USER_NAME_OR_PASSWORD_INCORRECT')
              break;
            case DescriptionNoOk.loginAdmin:
                this.errorAdmin = this.translateService.stream('PAGES.LOGIN.USER_NAME_OR_PASSWORD_INCORRECT')
                break;
            case DescriptionNoOk.getDevice:
              this.error = this.translateService.stream('PAGES.LOGIN.NO_DEVICE')
              break;
            case DescriptionNoOk.getOpenLoginSession:
              this.error = this.translateService.stream('PAGES.LOGIN.CANT_CHECK_OPEN_SESSIONS')
              break;
            case DescriptionNoOk.getShiftId:
              this.error = this.translateService.stream('PAGES.LOGIN.CANT_GET_SHIFT_ID')
              break;
            case DescriptionNoOk.endShift:
              this.error = this.translateService.stream('PAGES.LOGIN.ERROR_CLOSE_PREVIOUS_SHIFT')
              break;
            case DescriptionNoOk.startShift:
              this.error = this.translateService.stream('PAGES.LOGIN.ERROR_OPEN_SHIFT')
              break;
            case DescriptionNoOk.logoutFromSession:
              this.error = this.translateService.stream('PAGES.LOGIN.ERROR_CLOSE_SESSIONS')
            case DescriptionNoOk.getRepZ:
              this.error = this.translateService.stream('PAGES.LOGIN.ERROR_Z_REPORT')
            case DescriptionNoOk.closeRepZ:
              this.translateService.stream('PAGES.LOGIN.ERROR_DAY_CLOSING').subscribe(ERROR_DAY_CLOSING => {
                this.error = of(`${ERROR_DAY_CLOSING} ${sessionStorage.getItem(sessionKeys.cashboxName)}`);
              })
          }
          break;
        case LoginResponseType.selectCashbox:
          this.devices = res.data;
          this.modalService.open('select-box-modal');
          break;
        case LoginResponseType.openInDifferentSession:
          this.userOpenSessions = res.data;
          break;
        case LoginResponseType.openByOtherUser:
          this.otherOpenSessions = res.data;
          break;
        case LoginResponseType.canEnterToCashbox:
          this.enterToCashbox();
          break;
        case LoginResponseType.userNoAdmin:
          this.errorAdmin = this.translateService.stream('PAGES.LOGIN.USER_NO_ADMIN');
          break;
        case LoginResponseType.adminLoginSuccesfully:
          this.logoutFromSessions(this.otherOpenSessions);
          break;
        case LoginResponseType.alreadyClosedDay:
          this.dayClosed = true;
          break;
        case LoginResponseType.noReturnFromBreak:
          this.login();
          break;
        case LoginResponseType.returnFromBreak:
          this.returnFromBreak();
          break;
      }
    }
  }

  loginSubscribeErrorHandler (error) {
    this.isLoading = false;
    console.error(error);
    this.error = error.message;
  }

  resetValues() {
    this.error = null;
    this.errorAdmin = null;
    this.userOpenSessions = null;
    this.otherOpenSessions = null;
    this.adminLoginData = null;
  }

  removeQueryParams() {
    this.router.navigate([], {
      queryParams: {
        'sessionNoValid': null
      },
      queryParamsHandling: 'merge'
    });
  }

  ngOnDestroy() {
    this.cashboxSelected.unsubscribe();
    this.popUpClosed.unsubscribe();
  }

  getOperatorId() {
    const queryParams = this.route.snapshot.queryParams;
    const operatorId = Object.keys(queryParams)[0];
    sessionStorage.setItem(sessionKeys.operatorId, operatorId);
    return operatorId;
  }
}
