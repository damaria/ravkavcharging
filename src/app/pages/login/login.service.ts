import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { map, switchMap } from 'rxjs/operators';
import { Observable, of } from "rxjs";

import { androidAppVersion, cashboxDataType, EventCode, ServerResponse, statusCode } from "../../shared/constants/server";
import * as serverConstant from '../../shared/constants/server';
import { DescriptionNoOk, LoginResponse, LoginResponseType } from './loginResponse';
import { isToday } from "src/app/shared/utils/date";
import { SelectCashboxModalService } from "./components/select-cashbox-modal/select-cashbox-modal.service";
import { urls } from "../../shared/constants/URLs";
import { sessionKeys } from "src/app/shared/constants/sessionKey";
import { StocktakingStatus } from 'src/app/shared/constants/driverBox';
import { StoreService } from "src/app/shared/services/store.service";
import { actionTypeTransaction } from '../../shared/constants/server';

@Injectable({providedIn: 'root'})
export class LoginService {
    response;

    constructor(
        private http: HttpClient,
        private router: Router,
        private selectCashboxModalService: SelectCashboxModalService,
        private storeService: StoreService
        ) { }

    login(email, password) {
        if (!sessionStorage.getItem(sessionKeys.operatorId)) {
            return of(new LoginResponse(LoginResponseType.noOperatorId));
        }

        return this.http
            .post<ServerResponse>(
                urls.LOGIN,
                {
                    email: email,
                    password: password,
                    operator_id: sessionStorage.getItem(sessionKeys.operatorId),
                    loginSource: serverConstant.loginSource.BROWSER,
                    withUserData: "1"
                })
                .pipe(
                    switchMap(res => {
                        if (res.statusCode == statusCode.OK) {
                            const data = res.data;
                            sessionStorage.setItem(sessionKeys.sessionData , JSON.stringify(data.sessionData));

                            sessionStorage.setItem(sessionKeys.username , data.userData.firstName + " " + data.userData.lastName);
                            sessionStorage.setItem(sessionKeys.userId , data.userData.idNumber);
                            sessionStorage.setItem(sessionKeys.userEmail , email);

                            return this.getDevice();
                        }
                        else {
                            return of(new LoginResponse(LoginResponseType.descriptionNoOk, DescriptionNoOk.login));
                        }
                    })
                )
    }

    isReturnFromBreak(email, password) {
        let onBreakList: any = JSON.parse(sessionStorage.getItem(sessionKeys.onBreak));

        if (onBreakList != null) { //if someone on break no from today - drop him from on break list
            for (let i = 0; i < onBreakList.length; i++) {
                const onBreak = onBreakList[i];
                if (!isToday(onBreak.date)) {
                    onBreakList.splice(i,1);
                }
            }
            sessionStorage.setItem(sessionKeys.onBreak, JSON.stringify(onBreakList));
        }
        if (onBreakList == null ||
            !onBreakList.find(onBreak => onBreak.userEmail == email)) {
            return of(new LoginResponse(LoginResponseType.noReturnFromBreak));
        }
        else { // get back in from break
          //update onBreakList
          for (let i = 0; i < onBreakList.length; i++) {
            const onBreak = onBreakList[i];
            if (onBreak.userEmail == email) {
                onBreakList.splice(i,1);
                break;
            }
          }
          sessionStorage.setItem(sessionKeys.onBreak, JSON.stringify(onBreakList));

          return of(new LoginResponse(LoginResponseType.returnFromBreak));
        }
    }

    adminLogin(email, password) {
        return this.http
            .post<ServerResponse>(
                urls.LOGIN,
                {
                    email: email,
                    password: password,
                    operator_id: sessionStorage.getItem(sessionKeys.operatorId),
                    loginSource: serverConstant.loginSource.BROWSER,
                    withUserData: "1"
                })
                .pipe(
                    switchMap(res => {
                        if (res.statusCode != statusCode.OK) {
                            return of(new LoginResponse(LoginResponseType.descriptionNoOk, DescriptionNoOk.loginAdmin));
                        }
                        else {
                            const roles = res.data.userData.typeRoleId;
                            if (!roles.includes(serverConstant.typeRoleId.ADMIN)) {
                                return of(new LoginResponse(LoginResponseType.userNoAdmin));
                            }
                            else {
                                return this.logoutFromSession(res.data.sessionData)
                                .pipe(
                                    switchMap((res: LoginResponse) => {
                                        switch(res.type) {
                                            case LoginResponseType.descriptionNoOk:
                                                return of(new LoginResponse(LoginResponseType.descriptionNoOk, DescriptionNoOk.logoutFromSession));
                                            case LoginResponseType.logoutFromSessionSuccessfully:
                                                return of(new LoginResponse(LoginResponseType.adminLoginSuccesfully));
                                        }
                                    })
                                );
                            }
                        }
                    })
                )
    }

    disconnect() {
        return this.endShift(
            JSON.parse(sessionStorage.getItem(sessionKeys.shift)),
            serverConstant.endShiftStatus.MANUAL
            )
        .pipe(
            switchMap((res: LoginResponse) => {
                switch(res.type) {
                    case LoginResponseType.descriptionNoOk:
                        return of(new LoginResponse(LoginResponseType.descriptionNoOk, DescriptionNoOk.endShift));
                    case LoginResponseType.endShiftSuccessfully:
                        return this.logoutFromSession(JSON.parse(sessionStorage.getItem(sessionKeys.sessionData)));
                }
            })
        )
    }

    goToBreak() {
        return this.addEventLog(serverConstant.eventLogCodes.GO_TO_BREAK)
            .pipe(
                switchMap(res => {
                    if (res.statusCode != statusCode.OK) {
                        return of(new LoginResponse(LoginResponseType.descriptionNoOk, DescriptionNoOk.goToBreak));
                    }
                    else {
                        let onBreakList: any = JSON.parse(sessionStorage.getItem(sessionKeys.onBreak));
                        const userEmail = sessionStorage.getItem(sessionKeys.userEmail);
                        const onBreakNewMember = {
                            userEmail: userEmail,
                            date: new Date().getTime()
                        }

                        if (onBreakList == null) {
                            onBreakList = [onBreakNewMember];
                        }
                        else {
                            onBreakList.push(onBreakNewMember);
                        }
                        sessionStorage.setItem(sessionKeys.onBreak, JSON.stringify(onBreakList));

                        return of(new LoginResponse(LoginResponseType.goToBreakSuccessfully));
                    }
                })
            );
    }

    getDevice() {
        return this.http
            .post<ServerResponse>(
                urls.GET_DEVICE,
                {
                    data: {
                        active: "1",
                        status_id: serverConstant.general.NULL,
                        deviceType: serverConstant.deviceType.RAV_KAV_CHARGING,
                        idNumber: sessionStorage.getItem(sessionKeys.userId)
                    },
                    sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
                })
                .pipe(
                    switchMap(res => {
                        const cashboxes = res.data;
                        if (res.statusCode != statusCode.OK || cashboxes.length == 0) {
                            return of(new LoginResponse(LoginResponseType.descriptionNoOk, DescriptionNoOk.getDevice));
                        }
                        else {
                            if (cashboxes.length == 1) {
                                this.selectCashboxModalService.cashboxSelected.next(cashboxes[0]);
                                return of(null);
                            }
                            else {
                                return of(new LoginResponse(LoginResponseType.selectCashbox, cashboxes));
                            }
                        }
                    })
                )
    }

    checkOpenSessions(cashboxDetails) {
        sessionStorage.setItem(sessionKeys.cashboxId, cashboxDetails.Device_id);
        sessionStorage.setItem(sessionKeys.cashboxName, cashboxDetails.DeviceDescr);
        sessionStorage.setItem(sessionKeys.cashboxClusterId, cashboxDetails.clusterId);
        sessionStorage.setItem(sessionKeys.cashboxDeviceNum, cashboxDetails.Device_Num);
        sessionStorage.setItem(sessionKeys.adminCashbox, cashboxDetails.isManagement);

        return this.http
            .post<ServerResponse>(
                urls.GET_OPEN_LOGIN_SESSION,
                {
                    data: {
                        idNumber: serverConstant.general.NULL,
                        deviceId: sessionStorage.getItem(sessionKeys.cashboxId),
                        deviceType: serverConstant.deviceType.RAV_KAV_CHARGING,
                        employeeType: serverConstant.general.NULL,
                        loginSource: serverConstant.loginSource.BROWSER,
                        shiftType: serverConstant.general.NULL,
                        closeShift: "0"
                    },
                    sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
                })
                .pipe(
                    switchMap(res => {
                        if (res.statusCode != statusCode.OK) {
                            this.selectCashboxModalService.cancelCashboxSelected.next(null);
                            return of(new LoginResponse(LoginResponseType.descriptionNoOk, DescriptionNoOk.getOpenLoginSession));
                        }
                        else {
                            const sessionData = JSON.parse(sessionStorage.getItem(sessionKeys.sessionData));
                            const openSessions = res.data.filter(item => item.session !== sessionData.session)
                            //no else session connected to this cashbox
                            if (openSessions.length == 0) {
                                return this.getIntoCheckBox();
                            }
                            //else session connected to this cashbox
                            else {
                                //same user connected
                                this.selectCashboxModalService.cancelCashboxSelected.next(null);
                                if (openSessions.every(session =>
                                    session.FB_uid == sessionData.uid
                                ))
                                {
                                    return of(new LoginResponse(LoginResponseType.openInDifferentSession, openSessions));
                                }
                                //other user connected
                                else {
                                    return of(new LoginResponse(LoginResponseType.openByOtherUser, openSessions));
                                }
                            }
                        }
                    })
                )
    }

    logoutFromSession(sessionInfo) {
        return this.addEventLog(serverConstant.eventLogCodes.LOGOUT)
            .pipe(
                switchMap(() => {
                    return this.http
                    .post<ServerResponse>(
                        urls.LOGOUT,
                        sessionInfo.FB_uid ?
                        {
                            uid: sessionInfo.FB_uid,
                            session: sessionInfo.session,
                            operator_id: sessionInfo.Operator_ID
                        } :
                        {
                            uid: sessionInfo.uid,
                            session: sessionInfo.session,
                            operator_id: sessionInfo.operator_id
                        })
                        .pipe(
                            switchMap(res => {
                                if (res.statusCode != statusCode.OK) {
                                    return of(new LoginResponse(LoginResponseType.descriptionNoOk, DescriptionNoOk.logoutFromSession));
                                }
                                else {
                                  const thisSessionData = JSON.parse(sessionStorage.getItem(sessionKeys.sessionData));
                                  if (sessionInfo.session == thisSessionData.session) {
                                    sessionStorage.removeItem(sessionKeys.sessionData);
                                  }
                                  return of(new LoginResponse(LoginResponseType.logoutFromSessionSuccessfully));
                                }
                            })
                        )
                        })
                    );
    }

    getIntoCheckBox() {
      return this.getRepZ();
    }

    getShiftId() {
        return this.addEventLog(serverConstant.eventLogCodes.LOGIN)
            .pipe(
                switchMap(() => {
                    return this.http
                    .post<ServerResponse>(
                        urls.GET_SHIFT_ID,
                        {
                            data: {
                                idNumber: sessionStorage.getItem(sessionKeys.userId),
                                device_ID: sessionStorage.getItem(sessionKeys.cashboxId),
                                shiftType:  serverConstant.shiftType.RAV_KAV_CHARGING
                            },
                            sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
                        })
                        .pipe(
                            switchMap(res => {
                                if (res.statusCode != statusCode.OK) {
                                    return of(new LoginResponse(LoginResponseType.descriptionNoOk, DescriptionNoOk.getShiftId));
                                }
                                else {
                                    const shift = res.data[0];
                                    sessionStorage.setItem(sessionKeys.shift, JSON.stringify(shift));
                                    if (shift.StatusCode == serverConstant.StatusCode.NEW_SHIFT) {
                                        return this.startShift();
                                    }
                                    else { //open shift
                                        return this.storeService.getEventLog()
                                        .pipe(
                                            switchMap(events => {
                                                return this.storeService.updateCashBox()
                                                .pipe(
                                                    switchMap(() => {
                                                        const cashboxSum = this.storeService.getCashBoxSum();
                                                        const adminCashbox = JSON.parse(sessionStorage.getItem(sessionKeys.adminCashbox));
                                                        //if adminCashbox OR stocktaking already done OR no money in cashbox - skip stocktaking
                                                        if (adminCashbox || events.find(x => x.EventCode == EventCode.stocktaking) || cashboxSum == 0) {
                                                          sessionStorage.setItem(sessionKeys.stocktakingStatus, StocktakingStatus.MIDDLE_SHIFT);
                                                        }
                                                        else {
                                                          sessionStorage.setItem(sessionKeys.stocktakingStatus, StocktakingStatus.START_SHIFT);
                                                        }
                                                        if (isToday(shift.Shift_Start)) {
                                                            return of(new LoginResponse(LoginResponseType.canEnterToCashbox));
                                                        }
                                                        else {
                                                            return this.endShift(shift, serverConstant.endShiftStatus.AUTO);
                                                        }
                                                    })
                                                )
                                            })
                                        )
                                    }
                                }
                            })
                        )
                        })
                    )
    }

    getRepZ() {
        return this.http
        .post<ServerResponse>(
            urls.GET_REP_Z,
            {
                data: {
                    deviceId: sessionStorage.getItem(sessionKeys.cashboxId),
                    startDt:  serverConstant.general.NULL,
                    endDt: serverConstant.general.NULL,
                    lastOpenCashbox: "1"
                },
                sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
            })
            .pipe(
                switchMap(res => {
                    if (res.statusCode != statusCode.OK) {
                        return of(new LoginResponse(LoginResponseType.descriptionNoOk, DescriptionNoOk.getRepZ));
                    }
                    else {
                        const repZ = res.data[0];
                        if (repZ == null) {
                            return this.addRepZ();
                        }
                        if (!repZ.closeCashboxDt) {
                            if (isToday(repZ.openCashboxDt)) { //middle of the day
                                return  this.getShiftId();
                            }
                            else  { //repZ from the past no closed, need to close and search repZ again
                                return this.closeRepZ(repZ);
                            }
                        }
                        else {
                            if (!isToday(repZ.closeCashboxDt)) { //no repZ for today, need to open a new one
                                return this.addRepZ();
                            }
                            if (isToday(repZ.openCashboxDt)) { //after end of the day
                                // return this.getShiftId();
                                this.selectCashboxModalService.cancelCashboxSelected.next(null);
                                return of(new LoginResponse(LoginResponseType.alreadyClosedDay));
                            }
                            else  { //repZ open in the past and closed today
                                return this.addRepZ();
                            }
                        }
                    }
                })
            );
    }

    addRepZ() {
        return this.http.
        post<ServerResponse>(
            urls.ADD_REP_Z,
            {
                data: {
                    deviceId: sessionStorage.getItem(sessionKeys.cashboxId),
                    openCashboxDt: new Date().getTime()
                },
                sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
            }
        )
        .pipe(
            switchMap(res => {
                if (res.statusCode != statusCode.OK) {
                    return of(new LoginResponse(LoginResponseType.descriptionNoOk, DescriptionNoOk.getRepZ));
                }
                else {
                    return  this.getShiftId();
                }
            })
        );
    }

    closeRepZ(repZ) {
        return this.http
        .post<ServerResponse>(
            urls.CLOSE_CASHBOX,
            {
                data: {
                    repZId: repZ.repZId,
                    closeCashboxDt: new Date().getTime()
                },
                sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
            }
        )
        .pipe(
            switchMap(res => {
                if (res.statusCode != statusCode.OK) {
                    return of(new LoginResponse(LoginResponseType.descriptionNoOk, DescriptionNoOk.closeRepZ));
                }
                else {
                    return this.getRepZ();
                }
            })
        );
    }

    closingDay() {
        return this.endShift(
            JSON.parse(sessionStorage.getItem(sessionKeys.shift)),
            serverConstant.endShiftStatus.MANUAL
            )
        .pipe(
            switchMap(res => {
                switch(res.type) {
                    case LoginResponseType.descriptionNoOk:
                        return of(new LoginResponse(LoginResponseType.descriptionNoOk, DescriptionNoOk.endShift));
                    case LoginResponseType.endShiftSuccessfully:
                        return this.http
                            .post<ServerResponse>(
                                urls.GET_REP_Z,
                                {
                                    data: {
                                        deviceId: sessionStorage.getItem(sessionKeys.cashboxId),
                                        startDt:  serverConstant.general.NULL,
                                        endDt: serverConstant.general.NULL,
                                        lastOpenCashbox: "1"
                                    },
                                    sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
                                })
                                .pipe(
                                    switchMap(res => {
                                        if (res.statusCode != statusCode.OK) {
                                            return of(new LoginResponse(LoginResponseType.descriptionNoOk, DescriptionNoOk.getRepZ));
                                        }
                                        else {
                                            const repZId = res.data[0].repZId;

                                            return this.http
                                            .post<ServerResponse>(
                                                urls.CLOSE_CASHBOX,
                                                {
                                                    data: {
                                                        repZId: repZId,
                                                        closeCashboxDt: new Date().getTime()
                                                    },
                                                    sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
                                                }
                                            )
                                            .pipe(
                                                switchMap(res => {
                                                    if (res.statusCode != statusCode.OK) {
                                                        return of(new LoginResponse(LoginResponseType.descriptionNoOk, DescriptionNoOk.closeRepZ));
                                                    }
                                                    else {
                                                        this.storeService.printRepZReceipt(repZId);
                                                        return this.logoutFromSession(JSON.parse(sessionStorage.getItem(sessionKeys.sessionData)));
                                                    }
                                                })
                                            );
                                        }
                                    })
                                );
                }
            })
        )

    }

    startShift() {
        const now = new Date();

        const shiftId = (JSON.parse(sessionStorage.getItem(sessionKeys.shift))).Shift_id;
        const routeSystem = sessionStorage.getItem(sessionKeys.cashboxClusterId) ? sessionStorage.getItem(sessionKeys.cashboxClusterId) : '0';

        const shift = {
            androidAppVersion: androidAppVersion["1.4.4"],
            closeShiftForce: false,
            deviceId: sessionStorage.getItem(sessionKeys.cashboxId),
            driverId: sessionStorage.getItem(sessionKeys.userId),
            Operator_id: sessionStorage.getItem(sessionKeys.operatorId),
            operator: sessionStorage.getItem(sessionKeys.operatorId),
            routeSystem: routeSystem,
            serialNumberDevice: "aaaaaaaaaa",
            shiftId: shiftId,
            Shift_Start: new Date().toUTCString(),
            startShift: {
                date: {
                    day: now.getUTCDate(),
                    month: now.getUTCMonth() + 1,
                    year: now.getUTCFullYear()
                },
                time: {
                    hour: now.getUTCHours(),
                    minute: now.getMinutes(),
                    nano: 0,
                    second: now.getSeconds()
                }
            },
            status: 1,
            subContractor: 0,
            shiftType: serverConstant.shiftType.RAV_KAV_CHARGING
        }

        sessionStorage.setItem(sessionKeys.shift, JSON.stringify(shift));

        return this.http
            .post<ServerResponse>(
                urls.START_SHIFT,
                    {
                        shift: JSON.stringify(shift),
                        sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
                    }
                )
                .pipe(
                    switchMap(res => {
                        if (res.statusCode != statusCode.OK) {
                            return of(new LoginResponse(LoginResponseType.descriptionNoOk, DescriptionNoOk.startShift));
                        }
                        else {
                            const cashboxSum = this.storeService.getCashBoxSum();
                            const adminCashbox = JSON.parse(sessionStorage.getItem(sessionKeys.adminCashbox));
                            //if adminCashbox OR no money in cashbox OR is test mode OR this operator can skip - skip stocktaking
                            if (adminCashbox || cashboxSum == 0) {
                                sessionStorage.setItem(sessionKeys.stocktakingStatus, StocktakingStatus.MIDDLE_SHIFT);
                            }
                            else {
                                sessionStorage.setItem(sessionKeys.stocktakingStatus, StocktakingStatus.START_SHIFT);
                            }

                            return of(new LoginResponse(LoginResponseType.canEnterToCashbox));
                        }
                    })
                )
    }

    endShift(shift, status: serverConstant.endShiftStatus): Observable<any>{
        const shiftStartMillis = new Date(shift.Shift_Start).getTime();

        return this.http
        .post<ServerResponse>(
            urls.GET_DRIVER_CASHBOX,
            {
                data: {
                    driverCashboxTransactionId: serverConstant.general.NULL,
                    idNumber: sessionStorage.getItem(sessionKeys.userId),
                    startDt: shiftStartMillis,
                    endDt: serverConstant.general.NULL,
                    actionTypeTransaction: actionTypeTransaction.LOAD_CONTRACT + "," + actionTypeTransaction.CANCEL_CONTRACT,
                    device_ID: sessionStorage.getItem(sessionKeys.cashboxId),
                    onlySum: false
                },
                sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
            })
            .pipe(
                switchMap(res => {
                    let cashAmount = 0;
                    let cashSum = 0;
                    let vouchersAmount = 0;
                    let creditAmount = 0;

                    let transactions = res.data;
                    transactions = transactions.filter(transaction => transaction.ActionTypeTransaction);
                    while (transactions.length != 0) {
                        const currTransaction = transactions[0];
                        const currTransactionId = currTransaction.DriverCashboxTransactionId;
                        const currCashboxDataType = parseInt(currTransaction.CashboxDataType_ID);

                        switch(currCashboxDataType) {
                            case cashboxDataType.credit:
                                switch (currTransaction.ActionTypeTransaction) {
                                    case actionTypeTransaction.LOAD_CONTRACT:
                                        creditAmount++;
                                        break;
                                    case actionTypeTransaction.CANCEL_CONTRACT:
                                        creditAmount--;
                                }
                                break;
                            case cashboxDataType.studentVoucher:
                                switch (currTransaction.ActionTypeTransaction) {
                                    case actionTypeTransaction.LOAD_CONTRACT:
                                        vouchersAmount++;
                                        break;
                                    case actionTypeTransaction.CANCEL_CONTRACT:
                                        vouchersAmount--;
                                }
                                break;
                            default: //cash
                                switch (currTransaction.ActionTypeTransaction) {
                                    case actionTypeTransaction.LOAD_CONTRACT:
                                        cashAmount++;
                                        break;
                                    case actionTypeTransaction.CANCEL_CONTRACT:
                                        cashAmount--;
                                }
                                const relavantTransactions = transactions.filter(transaction => transaction.DriverCashboxTransactionId == currTransactionId);
                                relavantTransactions.forEach(transaction => {
                                    cashSum += parseInt(transaction.sumCash);
                                });
                                break;
                        }
                        transactions = transactions.filter(transaction => transaction.DriverCashboxTransactionId != currTransactionId);
                    }

                    const shiftStart = new Date(shift.Shift_Start + ' UTC');
                    const routeSystem = sessionStorage.getItem(sessionKeys.cashboxClusterId) ? sessionStorage.getItem(sessionKeys.cashboxClusterId) : '0';
                    const now = new Date();

                    const data = {
                        shift: JSON.stringify({
                            androidAppVersion: androidAppVersion["1.4.4"],
                            closeShiftForce: false,
                            deviceId: sessionStorage.getItem(sessionKeys.cashboxId),
                            driverId: shift.Driver_id,
                            endShift: {
                                date: {
                                    day: now.getUTCDate(),
                                    month: now.getUTCMonth() + 1,
                                    year: now.getUTCFullYear()
                                },
                                time: {
                                    hour: now.getUTCHours(),
                                    minute: now.getMinutes(),
                                    nano: 0,
                                    second: now.getSeconds()
                                }
                            },
                            operator: shift.Operator_id,
                            routeSystem: routeSystem,
                            serialNumberDevice: 'aaaaaaaaaa',
                            shiftId: shift.Shift_id | shift.shiftId,
                            startShift: {
                                date: {
                                    day: shiftStart.getUTCDate(),
                                    month: shiftStart.getUTCMonth() + 1,
                                    year: shiftStart.getUTCFullYear()
                                },
                                time: {
                                    hour: shiftStart.getUTCHours(),
                                    minute: shiftStart.getMinutes(),
                                    nano: 0,
                                    second: shiftStart.getSeconds()
                                }
                            },
                            status: 1,
                            subContractor: 0,
                            shiftType: serverConstant.shiftType.RAV_KAV_CHARGING,
                            sumShiftMoney: 0,
                            closeShiftType: status,
                            numberOfTicketsSoldinCredit: creditAmount.toString(),
                            numberOfTicketsSoldinCash: cashAmount.toString(),
                            numberOfValidations: vouchersAmount.toString(), // need to be number of vouchers
                            // numOfIDFVouchers: vouchersAmount.toString(),
                            cashRevenue: cashSum.toString(),
                        }),
                        sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
                    }
                    return this.http
                    .post<ServerResponse>(
                        urls.END_SHIFT,
                        JSON.stringify(data)
                        )
                        .pipe(
                            switchMap(res => {
                                if (res.statusCode != statusCode.OK) {
                                    return of(new LoginResponse(LoginResponseType.descriptionNoOk, DescriptionNoOk.endShift));
                                }
                                else {
                                    switch (status) {
                                        case serverConstant.endShiftStatus.AUTO:
                                            return  this.getShiftId();
                                        case serverConstant.endShiftStatus.MANUAL:
                                            sessionStorage.removeItem(sessionKeys.shift);
                                            return of(new LoginResponse(LoginResponseType.endShiftSuccessfully));
                                    }
                                }
                            })
                        )
                })
            )
    }

    checkLogin() {
        const sessionData = JSON.parse(sessionStorage.getItem(sessionKeys.sessionData));

        if (!sessionData) {
          this.navigateToLoginPage();
          return of(false);
        }

        return this.http
        .post<ServerResponse>(
            urls.CHECK_LOGIN,
            {
                uid: sessionData.uid,
                session: sessionData.session,
                operator_id: sessionData.operator_id
            })
            .pipe(
                map(res => {
                    if (res.statusCode != statusCode.OK) {
                        sessionStorage.removeItem(sessionKeys.sessionData);
                        this.navigateToLoginPage();
                        return false;
                    }
                    return true;
                })
            )
    }

    navigateToLoginPage() {
      const operatorId = sessionStorage.getItem(sessionKeys.operatorId);
      this.router.navigate(['/login', operatorId], { queryParams: { sessionNoValid: true } });
    }



    addEventLog(eventCode) {
        return this.http
        .post<ServerResponse>(
            urls.ADD_EVENT_LOG,
            {
                data : {
                idNumber: sessionStorage.getItem(sessionKeys.userId),
                device_ID: sessionStorage.getItem(sessionKeys.cashboxId),
                eventCode: eventCode,
                jsonObjRes: true
                },
                sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
            })
    }
}
