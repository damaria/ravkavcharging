import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchInTable'
})
export class SearchInTablePipe implements PipeTransform {

  transform(value: any, ...filterValue: any): any {
    let result = [];

    value.forEach(contract => {
      let values = [];

      for (let key in contract) {
        values.push(contract[key]);
      }
      if (values.find(col => col.includes(filterValue)) != undefined) {
        result.push(contract);
      }
    });

    if (result.length > 0) {
      return result;
    }

    return null;
  }
}
