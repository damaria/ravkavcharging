import { Pipe, PipeTransform } from "@angular/core";
import { cashboxDataType } from "../constants/server";
import { StoreService } from "../services/store.service";

@Pipe({name: 'cashboxKeys'})
export class CashboxKeysPipe implements PipeTransform {
  constructor(private storeService: StoreService) {}

  transform(value): any {
    if (typeof(value) == 'undefined') {
      return;
    }
    let keys = [];

    for (let key in value) {
      if (+key < 11) {
        keys.push({key: key, value: value[key]});
      }
    }
    keys.reverse();

    for (let key in value) {
      if (+key >= 11 && +key != cashboxDataType.credit && +key != cashboxDataType.idfVoucher) {
        if (this.checkFilterVouchers(+key)) {
          keys.push({key: key, value: value[key]});
        }
      }
    }
    
    return keys;
  }

  checkFilterVouchers(key) {
    const isSupportVouchers = this.storeService.operatorConfiguration.supportVouchers;
    if (isSupportVouchers || !isSupportVouchers && key != cashboxDataType.studentVoucher) {
      return true;
    }
    return false;
  }
}