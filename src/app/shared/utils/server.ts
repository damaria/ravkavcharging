import { ErrorStatus } from "../constants/server";

export function getErrorStatusTranslationPath(errorStatus: ErrorStatus) : string[] {
  const translationPath = ['SHARED','UTILS','SERVER'];
  switch(errorStatus.code) {
    case 2:
      translationPath.push('CANT_MORE_THEN_THOUSAND');
      break;
    case 5:
      translationPath.push('NO_PLACE');
      break;
    case 6:
      translationPath.push('ANONYMOUS_CANT');
      break;
    case 10:
      translationPath.push('CONTRACT_NO_VALID');
      break;
    case 11:
      translationPath.push('CONTRACT_EXIST');
      break;
    case 12:
      translationPath.push('CARD_BLACKLISTED');
      break;
    case 13:
      translationPath.push('START_TIME_INCORRECT');
      break;
    case 14:
      translationPath.push('NOT_MATCH_PROFILES');
      break;
    case 15:
      translationPath.push('PROFILE_DATE_EXPIRED');
      break;
    default:
      translationPath.push('SERVER_CANT_LOAD');
  }
  return translationPath;
}
