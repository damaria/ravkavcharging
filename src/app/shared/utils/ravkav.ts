import { ContractPriority, GOLD_KAV_PROFILE, TariffContractNameCode } from "../constants/ravKav";
import { isAfterToday } from "./date";

export function isAnonymousCard(ravKavDetails) {
    return (
      ravKavDetails.environment.holdersNationalid == 0
      // && ravKavDetails.environment.holderProfile1Code == 0
      // && ravKavDetails.environment.holderProfile2Code == 0
    );
}

export function getProfileToOverride(environment, newProfile?) {
  // if get the new profile - return it
  if (newProfile) {
    if (environment.holderProfile1Code == newProfile) {
      return 1;
    }
    if (environment.holderProfile2Code == newProfile) {
      return 2;
    }
  }

  // if get special not valid - returm it
  if(
    environment.holderProfile1Code != 0 &&
    environment.holderProfile1Code != 1 &&
    new Date(environment.holderProfile1Date) < new Date()
  ) {
    return 1;
  }
  if(
    environment.holderProfile2Code != 0 &&
    environment.holderProfile2Code != 1 &&
    new Date(environment.holderProfile2Date) < new Date()
  ) {
    return 2;
  }

  // if get regular not valid - returm it
  if(
    (environment.holderProfile1Code == 0 ||
    environment.holderProfile1Code == 1) &&
    new Date(environment.holderProfile1Date) < new Date()
  ) {
    return 1;
  }
  if(
    (environment.holderProfile2Code == 0 ||
    environment.holderProfile2Code == 1) &&
    new Date(environment.holderProfile2Date) < new Date()
  ) {
    return 2;
  }

  // if get regular valid - returm it
  if(
    (environment.holderProfile1Code == 0 ||
    environment.holderProfile1Code == 1) &&
    new Date(environment.holderProfile1Date) > new Date()
  ) {
    return 1;
  }
  if(
    (environment.holderProfile2Code == 0 ||
    environment.holderProfile2Code == 1) &&
    new Date(environment.holderProfile2Date) > new Date()
  ) {
    return 2;
  }
  return null;
}

export function isValidPriority(priority: ContractPriority) {
  return priority != ContractPriority.INVALID && priority != ContractPriority.ERASABLE && priority != ContractPriority.ABSENT;
}

export function hasGoldkavFreeCertificateContract(card) {
  let isHasGoldkavFreeCertificateContract = false;
  card.contracts.every((contract, i) => {
    if (contract && isContractGoldkavFreeCertificate(contract, i, card.events[0])) {
      isHasGoldkavFreeCertificateContract = true;
      return false;
    }
    return true;
  })
  return isHasGoldkavFreeCertificateContract;
}

function isContractGoldkavFreeCertificate(contract, i, lastEvent) {
  return isGoldkav(contract) && isFC(contract) && isValid(contract) && isExist(i, lastEvent);
}

function isGoldkav(contract) {
  return contract.contractCustomerProfile == GOLD_KAV_PROFILE;
}

function isFC(contract) {
  return contract.tariffContractNameCode == TariffContractNameCode.FREE_CERTIFICATE;
}

function isValid(contract) {
  return (!contract.contractValidityEndDate || isAfterToday(contract.contractValidityEndDate));
}

function isExist(i, lastEvent) {
  const index = i + 1; //start from one, no from zero
  return lastEvent['eventBestContractPriority' + index] == ContractPriority.CURRENT_SP_OR_FC;
}
