import { StoreService } from "../services/store.service";

export function isToday(date) {
    const pureDate = new Date(date).setHours(0,0,0,0);
    const today = new Date().setHours(0,0,0,0);
    return pureDate == today;
}

export function isAfterToday(date) {
  const pureDate = new Date(date).setHours(0,0,0,0);
  const today = new Date().setHours(0,0,0,0);
  return pureDate > today;
}

export function isFreeMonthly(contract) {
    return contract.ettCode == 20;
}

export function isFreeRide(contract) {
  return (contract.ettCode)[0] == "4" && contract.profileType == "0";
}

export function minutesToMillis(minutes) {
  const seconds = minutes * 60;
  return seconds * 1000;
}

export function toIsoString(date) {
  var tzo = -date.getTimezoneOffset(),
      dif = tzo >= 0 ? '+' : '-',
      pad = function(num) {
          return (num < 10 ? '0' : '') + num;
      };

  return date.getFullYear() +
      '-' + pad(date.getMonth() + 1) +
      '-' + pad(date.getDate()) +
      'T' + pad(date.getHours()) +
      ':' + pad(date.getMinutes()) +
      ':' + pad(date.getSeconds()) +
      dif + pad(Math.floor(Math.abs(tzo) / 60)) +
      ':' + pad(Math.abs(tzo) % 60);
}

export function toEncodedIsoString(date) {
  const IsoString = toIsoString(date);
  return encodeURIComponent(IsoString);
}
