import { Component, Input, OnInit } from '@angular/core';
import { filter } from 'rxjs';
import { MainService } from 'src/app/pages/main/main.service';
import { environment } from 'src/environments/environment';
import { statusCode } from '../../constants/server';
import { sessionKeys } from '../../constants/sessionKey';
import { devMode, testMode } from '../../constants/URLs';
import { StoreService } from '../../services/store.service';

@Component({
  selector: 'app-upper-strap',
  templateUrl: './upper-strap.component.html',
  styleUrls: ['./upper-strap.component.css']
})
export class UpperStrapComponent implements OnInit {
  operatorDescr;
  versionNum = environment.versionNum;
  testMode = testMode;
  devMode = devMode;
  @Input('userName') userName: string;
  adminCashbox: boolean;

  constructor(private mainService: MainService, private storeService: StoreService) { }

  ngOnInit(): void {
    this.getOperatorDescr();
    this.adminCashbox = JSON.parse(sessionStorage.getItem(sessionKeys.adminCashbox)) == 1;
  }

  getOperatorDescr(): void {
    const currOperator = sessionStorage.getItem(sessionKeys.operatorId);
    const operatorsFromStorage = sessionStorage.getItem(sessionKeys.serverOperators);

    if (operatorsFromStorage) {
      const operators = JSON.parse(operatorsFromStorage);
      this.operatorDescr = operators.find(op => op.Operator_id == currOperator).Descr;
    }
    else if (sessionStorage.getItem(sessionKeys.sessionData)) {
      this.mainService.getGeneralDataFromServer().pipe(
        filter(res => res.statusCode == statusCode.OK))
        .subscribe(
          res => {
            const operators = res.data.operators.data;
            this.operatorDescr = operators.find(op => op.Operator_id == currOperator).Descr;
          }
        );
    }
  }
}
