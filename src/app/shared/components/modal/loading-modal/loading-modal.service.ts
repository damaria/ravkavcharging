import { Injectable } from "@angular/core";
import { ModalService } from "../modal.service";

@Injectable({ providedIn: "root" })
export class LoadingModalService {
    constructor(private modalService: ModalService) { }

    open() {
      this.modalService.open("loading-modal");
    }

    close() {
      this.modalService.close("loading-modal");
    }
}
