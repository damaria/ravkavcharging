import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ModalService } from '../modal.service';

@Injectable({
  providedIn: 'root'
})
export class ErrorModalService {
  errorSubject = new Subject<ErrorInfo>();

  constructor(private modalService: ModalService) { }

  sendError(event) {
    const response = event.body;
    this.errorSubject.next({
      statusCode: response.statusCode,
      description: response.description
    });
    this.modalService.open('error-modal');
  }

  closeError() {
    this.modalService.close('error-modal');
  }
}

export interface ErrorInfo {
  statusCode: number,
  description: string
}
