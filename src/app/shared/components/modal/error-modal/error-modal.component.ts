import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { StoreService } from 'src/app/shared/services/store.service';
import { ErrorInfo, ErrorModalService } from './error-modal-service.service';

@Component({
  selector: 'app-error-modal',
  templateUrl: './error-modal.component.html',
  styleUrls: ['./error-modal.component.css']
})
export class ErrorModalComponent implements OnInit, OnDestroy {
  id;
  backgroundClickFunction;

  infoModalIcon = "assets/icons/infoIcons/error.svg";
  infoModalTitle;
  infoModalSubTitle;
  infoModalHr = true;
  infoModalBtnLeftText;
  errorInfo: ErrorInfo;

  errorSubscription: Subscription;

  CHECK_INTERNET_CONNECTION;

  constructor(
    private errorModalService: ErrorModalService,
    private storeService: StoreService) { }

  ngOnInit(): void {
    this.errorSubscription = this.errorModalService.errorSubject.subscribe(data => {
      this.errorInfo = {
        statusCode: data.statusCode,
        description: data.description
      }
    });

    this.storeService.getTranslation(translation => {
      this.infoModalTitle = translation.GENERAL.ERROR;
      this.infoModalSubTitle  = [translation.GENERAL.ERROR_SORRY];
      this.infoModalBtnLeftText = translation.GENERAL.CLOSE;
      // const ERROR = translation.SHARED.COMPONENTS.DIALOGS.ERROR;
      // this.CHECK_INTERNET_CONNECTION = `${ERROR.LOADING_ERROR}, ${ERROR.CHECK_INTERNET_CONNECTION}`;
      // this.infoModalSubTitle = [this.CHECK_INTERNET_CONNECTION];
    });
  }

  closeInfoModal = () => {
    this.errorModalService.closeError();
  }

  ngOnDestroy(): void {
    this.errorSubscription.unsubscribe();
  }
}
