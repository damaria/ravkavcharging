import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-info-modal',
  templateUrl: './info-modal.component.html',
  styleUrls: ['./info-modal.component.css']
})
export class InfoModalComponent {
  @Input('id') id;
  @Input('backgroundClickFunction') backgroundClickFunction;
  @Input('icon') icon;
  @Input('title') title;
  @Input('subTitle') subTitle;
  @Input('hr') hr;
  @Input('btnTitle') btnTitle;
  @Input('btnRightText') btnRightText;
  @Input('btnRightClickFun') btnRightClickFun;
  @Input('btnLeftText') btnLeftText;
  @Input('btnLeftClickFun') btnLeftClickFun;
  @Input('warning') warning;
  @Input('error') error;
  @Input('btnRightDisabled') btnRightDisabled;
  @Input('btnLeftDisabled') btnLeftDisabled;
}
