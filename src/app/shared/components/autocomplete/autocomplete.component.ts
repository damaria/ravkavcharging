import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, ValidatorFn } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.css']
})
export class AutocompleteComponent implements OnInit {

  option = new FormControl('')

  filteredOptions: Observable<any[]>;
  _options: any[]
  _value: any
  _icons = []
  isAutocompeleteOpened: boolean = false

  @ViewChild('input') input: ElementRef;

  @Input() marginBottom: string
  @Input() autoSetFirstValue: boolean = false
  @Input() width: string = "100"
  @Input() appearance: string = "outline"
  @Input() label: string
  @Input('validators')
  set validators(validators: ValidatorFn[]) {
    this.option.setValidators(validators);
    this.option.updateValueAndValidity()
  }
  @Input('options')
  set options(inputArr: any[]) {
    if (!inputArr) return

    // if inputArr different than _options -> set and emit empty value
    if (this._options && JSON.stringify(this._options) !== JSON.stringify(inputArr) && this._options !== inputArr) this.clearAndEmitValue()

    // if (inputArr.length) inputArr = this.sortData(inputArr)
    this._options = inputArr

    this.filteredOptions = this.option.valueChanges
      .pipe(
        startWith(''),
        map(value => typeof value == 'string' || 'undefined' ? value : value[this.displayBy]),
        map(name => name ? this._filter(name) : this._options)
      )

    this.setValue()
  }

  // sortData(inputArr){
  //   if (this.type == 'line') return inputArr.sort((a, b) => this.fixDataSort(a) > this.fixDataSort(b) ? 1 : this.fixDataSort(b) > this.fixDataSort(a) ? -1 : 0)
  //   if (this.type == 'sortByReturnBy') return inputArr.sort((a, b) => Number(a[this.returnBy]) > Number(b[this.returnBy]) ? 1 : Number(b[this.returnBy]) > Number(a[this.returnBy]) ? -1 : 0)
  //   return inputArr.sort((a, b) => a[this.displayBy] > b[this.displayBy] ? 1 : b[this.displayBy] > a[this.displayBy] ? -1 : 0)
  // }

  fixDataSort(line: string): number{
    return Number(line.replace("א", ".5"))
  }

  @Input('icons')
  set icons(icons: any[]) {
    this._icons = icons
  }
  @Input() displayBy: string
  @Input() type: string
  @Input() returnBy: string
  @Input('value')
  set value(value: any) {
    if (typeof value === 'string' || value instanceof String) {
      if (JSON.stringify(this._value) === JSON.stringify(value)) return;

      this._value = value;

      if (this._value == '') {
        this.option.setValue('');
      }
      else if (this._options) {
        this.setValue()
      }
    }
  }

  @Output() getValue = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void { }

  private _filter(name: string): any[] {
    const filterValue = typeof name == 'string' ? name.trim().toLowerCase() : name;

    return this._options.filter(option => (this.displayBy ? option[this.displayBy] : option).trim().toLowerCase().includes(filterValue));
  }

  displayFn(selectedOption: any): string {
    if (!selectedOption) return ''

    if (this.displayBy && selectedOption[this.displayBy]) return selectedOption[this.displayBy]
    else return selectedOption
  }

  private setValue() {
    if (this.returnBy) {
      if (this._value && (this.option.value == undefined || this._value != this.option.value[this.returnBy])){
        this.option.setValue(this._options.find(option => option[this.returnBy] === this._value))
      }
      else if (this.autoSetFirstValue && this._options.length === 1 && (this._value === '' || this._value != this.option.value[this.returnBy])) {
        this._value = this._options[0]
        this.option.setValue(this._options[0])
        this.getValue.emit(this.option.value[this.returnBy])
      }
    } else {
      if (this._value && this._value != this.option.value)
        this.option.setValue(this._value)
      else if (this.autoSetFirstValue && this._options.length === 1 && (this._value === '' || this._value != this.option.value)) {
        this._value = this._options[0]
        this.option.setValue(this._options[0])
        // this.getValue.emit(this.option.value)
      }
    }
  }

  public clearAndEmitValue() {
    this.option.setValue('');
    // this.getValue.emit('');
  }

  onIconClicked = (icon: { name: string, tooltip?: string, callback: () => void }): void => {
    if (icon.callback) icon.callback()
  }

  inputCahnge() {
    this.input.nativeElement.value = "";
    this.getValue.emit(null);
  }
}
