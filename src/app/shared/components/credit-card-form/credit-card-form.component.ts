import { Component, OnInit } from '@angular/core';
import { CardLabel, FormLabel } from 'ngx-interactive-paycard';
import { StoreService } from '../../services/store.service';
import { Card } from './Card.modal';
import { CreditCardFormService } from './credit-card-form.service';

@Component({
  selector: 'app-credit-card-form',
  templateUrl: './credit-card-form.component.html',
  styleUrls: ['./credit-card-form.component.css']
})
export class CreditCardFormComponent implements OnInit {
  error;
  cardNumberFormat;
  cardNumberMask;
  //Optional
  cardLabel: CardLabel;
  //Optional
  formLabel: FormLabel;
  PLEASE_COMPLITE;
  AT_LEAST_6;

  constructor(private creditCardFormService: CreditCardFormService, private storeService: StoreService) { }

  ngOnInit(): void {
    this.storeService.getTranslation(translation => {
      const _translation = translation.SHARED.COMPONENTS.CREDIT_CARD_FORM;
      this.error = null;
      this.cardNumberFormat = "#### #### #### ####";
      this.cardNumberMask = "#### #### #### ####";
      this.cardLabel = {
        expires: _translation.EXPIRES,
        cardHolder: _translation.CARD_HOLDER,
        fullName: _translation.ID,
        mm: 'MM',
        yy: 'YY',
      };
      this.formLabel = {
        cardNumber: _translation.CARD_NUMBER,
        cardHolderName: _translation.CARD_HOLDER_ID,
        expirationDate: _translation.EXPIRATION_DATE,
        expirationMonth: _translation.EXPIRATION_MONTH,
        expirationYear: _translation.EXPIRATION_YEAR,
        cvv: 'CVV',
        submitButton: _translation.SUBMIT_BUTTON,
      };
      this.PLEASE_COMPLITE = _translation.PLEASE_COMPLITE;
      this.AT_LEAST_6 = _translation.AT_LEAST_6;
    });
  }

  onSubmit(cardDetails: Card) {
    const errorCardDetails = this.errorCardDetails(cardDetails);
    if (errorCardDetails) {
      this.error = errorCardDetails;
    }
    else {
      this.creditCardFormService.submit.next(
        {
          ...cardDetails,
          cardNumber: cardDetails.cardNumber.replaceAll(" ", "")
        }
      );
    }
  }

  errorCardDetails(cardDetails: Card) {
    let errorString = null;
    if (
    cardDetails.cardName == "" ||
    cardDetails.cardNumber == "" ||
    cardDetails.cvv == "" ||
    cardDetails.expirationMonth == "" ||
    cardDetails.expirationYear == ""
    ) {
      errorString = this.PLEASE_COMPLITE;
    }
    else if (cardDetails.cardNumber.replace(" ","").length < 6) {
      errorString = this.AT_LEAST_6;
    }
    return errorString;
  }

  formChanged($event) {
    this.error = null;
  }

  // showChangesCardNumber($event) {
  //   // any changes on card number
  //   console.log($event);
  // }
}
