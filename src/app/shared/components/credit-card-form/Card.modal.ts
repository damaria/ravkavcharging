export interface Card {
    cardName: string,
    cardNumber: string,
    cvv: string,
    expirationMonth: string,
    expirationYear: string,
}