import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import * as moment from 'moment';

@Component({
  selector: 'app-select-month-free-monthly',
  templateUrl: './select-month-free-monthly.component.html',
  styleUrls: ['./select-month-free-monthly.component.css']
})
export class SelectMonthFreeMonthlyComponent implements OnInit {
  minDate =  new Date("8/1/2022"); // 1 at august - beginning reform
  maxDate = new Date("12/31/2040");
  selectedDate: moment.Moment;
  dateNoValid = false;

  constructor(public dialogRef: MatDialogRef<SelectMonthFreeMonthlyComponent>) { }

  ngOnInit(): void { }

  checkValidity(date) {
    if (date.value.valueOf() < this.minDate.getTime()) {
      this.dateNoValid = true;
    }
    else {
      this.dateNoValid = false;
    }
  }

  pressContinue(dateString) {
    if (!this.dateNoValid) {
      this.selectedDate = moment(dateString, "DD/MM/YYYY");
      this.dialogRef.close(this.selectedDate);
    }
  }
}
