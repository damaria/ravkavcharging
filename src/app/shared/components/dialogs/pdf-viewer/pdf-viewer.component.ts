import { Component, OnInit } from '@angular/core';
import { PDFProgressData } from 'ng2-pdf-viewer';

@Component({
  selector: 'app-pdf-viewer',
  templateUrl: './pdf-viewer.component.html',
  styleUrls: ['./pdf-viewer.component.css']
})
export class PdfViewerComponent implements OnInit {
  rulesSrc;
  loadingPercent;
  
  constructor() { }

  ngOnInit(): void {
    this.loadingPercent = 0;

    this.rulesSrc = window.location.pathname + "assets/pdf/op_37.pdf";
  }

  onProgress(progressData: PDFProgressData) {
    if (this.loadingPercent != 100) {
      this.loadingPercent = parseInt((progressData.total / progressData.loaded * 100).toString());
    }
  }
}
