import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import * as moment from 'moment';

@Component({
  selector: 'app-select-expiration-free-ride',
  templateUrl: './select-expiration-free-ride.component.html',
  styleUrls: ['./select-expiration-free-ride.component.css']
})
export class SelectExpirationFreeRideComponent implements OnInit {
  minDate =  new Date();
  maxDate = new Date("12/31/2039"); //expiration of contracts
  selectedDate: moment.Moment;
  dateNoValid = false;

  constructor(public dialogRef: MatDialogRef<SelectExpirationFreeRideComponent>) { }

  ngOnInit(): void { }

  checkValidity(date) {
    if (date.value.valueOf() < this.minDate.setHours(0,0,0,0)) {
      this.dateNoValid = true;
    }
    else {
      this.dateNoValid = false;
    }
  }

  pressContinue(dateString) {
    if (!this.dateNoValid) {
      this.selectedDate = moment(dateString, "DD/MM/YYYY");
      this.dialogRef.close(this.selectedDate);
    }
  }
}
