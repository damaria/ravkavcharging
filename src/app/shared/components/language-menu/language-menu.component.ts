import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { StoreService } from '../../services/store.service';

@Component({
  selector: 'app-language-menu',
  templateUrl: './language-menu.component.html',
  styleUrls: ['./language-menu.component.css']
})
export class LanguageMenuComponent implements OnInit {
  langs: {
    [lang: string]: {
      [langLabel: string]: string
    }
  } = {};

  constructor(public translateService: TranslateService, private storeService: StoreService) { }

  ngOnInit(): void {
    this.initMenu();
  }

  initMenu() {
    const allLangs = this.translateService.getLangs();
    allLangs.forEach(lang => {
      this.storeService.getTranslation(translation => {
        this.langs[lang] = {
          langLabel: translation.GENERAL.LANG_LABEL,
          langImg: translation.GENERAL.LANG_IMG
        }
      }, lang);
    });
  }

  useLanguage(lang: string): void {
    this.translateService.use(lang);
  }
}
