import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-error-message',
  templateUrl: './error-message.component.html',
  styleUrls: ['./error-message.component.css']
})
export class ErrorMessageComponent {
  @Input() fontSize: string;
  @Input() margin: string;
  color = "#B53535";
  border = "solid 1px #B53535";
  backgroundColor = "#FFF5F5";

  iconSrc = "assets/icons/infoIcons/error.svg";
  @Input() set status(newStatus) {
    if (newStatus == "warning") {
      this.iconSrc = "assets/icons/infoIcons/warning.svg";
      this.color = "#906c02";
      this.border = "solid 1px #906c02";
      this.backgroundColor = "#fffef5";
    }
  }
}
