import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { MatButton } from '@angular/material/button';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {
  cursor;
  styleExpression;

  @Input() text: string;
  @Input() color: string = "white";
  @Input() width: string;
  @Input() backGroundColor: string = "white";

  @Input() disabled;

  @Output() clickOnButton = new EventEmitter<any>();
  @ViewChild("button") button: MatButton;

  constructor() { }

  ngOnInit(): void {
    if (this.disabled) {
      this.color = 'grey';
      this.cursor ='not-allowed';
      this.styleExpression = 'border: 1px solid grey;'
    }
  }

  clicked(){
    this.clickOnButton.emit();
  }
}
