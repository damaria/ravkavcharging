import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

@Injectable({providedIn: 'root'})
export class DisplayMoneyService {
  textChanged = new Subject();
  reset = new Subject();
}

export enum TextChangedStatus {
  CONTINUE,
  ERROR
}

