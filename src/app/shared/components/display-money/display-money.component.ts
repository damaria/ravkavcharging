import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { CashboxWithoutPapers } from 'src/app/shared/models/cashboxWithoutPapers.model';
import { getImageUrl } from 'src/app/shared/utils/cashbox';
import { getValue } from 'src/app/shared/constants/server';
import { Subscription } from 'rxjs';
import { AmountInputService, subData } from 'src/app/shared/components/amount-input/amount-input.service';
import { DisplayMoneyService, TextChangedStatus } from './display-money.service';
import { MatButton } from '@angular/material/button';
import { StoreService } from '../../services/store.service';

@Component({
  selector: 'app-display-money',
  templateUrl: './display-money.component.html',
  styleUrls: ['./display-money.component.css']
})
export class DisplayMoneyComponent implements OnInit {
  paymentCashbox: CashboxWithoutPapers;
  amountInputChangedSub: Subscription;
  textChangedSub: Subscription;
  resetSub: Subscription;
  @Input() text: string;
  @Input() sum: number;
  @Input() disabled: boolean;
  @Input() type: string;
  @Output() action = new EventEmitter<any>();
  @ViewChild("submitButton") submitButton: MatButton;

  constructor(
    private amountInputService: AmountInputService,
    private displayMoneyService: DisplayMoneyService,
    private storeService: StoreService
    ) { }

  ngOnInit(): void {
    this.paymentCashbox = new CashboxWithoutPapers();
    this.amountInputChangedSub = this.amountInputService.inputChanged.subscribe(
      (data: subData) => {
        if (data.info == 'payment') {
          switch (data.operation) {
            case 'increase':
              this.paymentCashbox[data.type]++;
              break;
            case 'decrease':
              this.paymentCashbox[data.type]--;
              break;
            case 'change':
              this.paymentCashbox[data.type] = data.amount;
              break;
          }
        }
      }
    );

    this.textChangedSub = this.displayMoneyService.textChanged.subscribe(
      (status: TextChangedStatus) => {
        this.storeService.getTranslation(translation => {
          switch(status) {
            case TextChangedStatus.CONTINUE:
              this.text = `${translation.GENERAL.PLEASE_WAIT}...`;
              if (this.submitButton) {
                this.submitButton._elementRef.nativeElement.disabled = true;
                this.submitButton._elementRef.nativeElement.style.cursor = "not-allowed";
              }
              break;
            case TextChangedStatus.ERROR:
              this.text = translation.GENERAL.PURCHASE;
              if (this.submitButton) {
                this.submitButton._elementRef.nativeElement.disabled = false;
                this.submitButton._elementRef.nativeElement.style.cursor = "pointer";
              }
              break;
          }
        });
      }
    )

    this.resetSub = this.displayMoneyService.reset.subscribe(
      () => {
        this.paymentCashbox = new CashboxWithoutPapers();
      }
    )
  }

  clicked() {
    this.action.emit(this.paymentCashbox);
  }

  _getImageUrl(type) {
    return getImageUrl(type);
  }

  _getValue(code) {
    return getValue(code);
  }

  ngOnDestroy() {
    this.amountInputChangedSub.unsubscribe();
    this.textChangedSub.unsubscribe();
    this.resetSub.unsubscribe();
  }
}
