import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { AmountInputService } from './amount-input.service';

@Component({
  selector: 'app-amount-input',
  templateUrl: './amount-input.component.html',
  styleUrls: ['./amount-input.component.css']
})
export class AmountInputComponent implements OnInit {
  @Input('type') type;
  @Input('payload') payload;
  @Input('info') info;
  @Input('maxValue') maxValue;
  amount;
  oldAmount;

  @ViewChild('input') input: ElementRef;

  constructor(private amountInputService: AmountInputService) { }

  ngOnInit(): void {
    this.amount = 0;
  }

  onPlus = () => {
    if (this.payload == "out" && this.amount == this.maxValue) {
      this.amount = this.maxValue;
      this.amountInputService.inputChanged.next({
        amount: this.amount,
        operation: 'moreThanMax',
        type: this.type,
        maxValue: this.maxValue
      });
    }
    else {
      this.amount++;
      this.amountInputService.inputChanged.next({
        operation: 'increase',
        type: this.type,
        payload: this.payload,
        info: this.info,
        maxValue: this.maxValue
      });
    }
  }

  onMinus = () => {
    if (this.amount == null || this.amount == 0) {
      this.amountInputService.inputChanged.next({
        operation: 'lessThanZero',
        type: this.type
      });
    }
    else {
      this.amount--;
      this.amountInputService.inputChanged.next({
        operation: 'decrease',
        type: this.type,
        payload: this.payload,
        info: this.info
      })
    }
  }

  onInputChange = (newValue) => {
    if (newValue < 0) {
      this.amount = 0;
      this.amountInputService.inputChanged.next({
        operation: 'lessThanZero',
        type: this.type
      });
    }
    else if (this.payload == "out" && newValue > this.maxValue) {
      this.amount = this.maxValue;
      this.input.nativeElement.value = this.maxValue;
      this.amountInputService.inputChanged.next({
        amount: this.amount,
        operation: 'moreThanMax',
        type: this.type,
        maxValue: this.maxValue
      });
      return;
    }
    else {
      this.amount = newValue;
      // this.amount = newValue ? newValue : 0;
      this.input.nativeElement.value = this.amount;
      this.amountInputService.inputChanged.next({
        operation: 'change',
        type: this.type,
        amount: this.amount,
        payload: this.payload,
        info: this.info,
        maxValue: this.maxValue
      })
    }
  }

  onBlurInput() {
    if (this.amount == null) {
      this.amount = 0;
    }
  }

  onKeydown(event) { //avoids 'e', '-', '+', '.' ... all characters that are not numbers !
    javascript: return event.keyCode === 8 || event.keyCode === 9 ||
    event.keyCode === 46 ? true : !isNaN(Number(event.key))
  }
}