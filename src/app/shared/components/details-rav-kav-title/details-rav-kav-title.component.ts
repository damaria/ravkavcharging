import { Component, Input, OnInit } from '@angular/core';
import { StoreService } from '../../services/store.service';
import { isFreeMonthly } from '../../utils/date';

@Component({
  selector: 'app-details-rav-kav-title',
  templateUrl: './details-rav-kav-title.component.html',
  styleUrls: ['./details-rav-kav-title.component.css']
})
export class DetailsRavKavTitleComponent implements OnInit {
  isFreeMonthly = isFreeMonthly;
  price;
  priceLabel;

  @Input('contractDetails') contractDetails;

  constructor(public storeService: StoreService) {}

  ngOnInit(): void {
    this.storeService.getTranslation(translation => {
      this.priceLabel = translation.GENERAL.PRICE;
      this.price = this.contractDetails.contract.exactPrice;
    });
  }
}
