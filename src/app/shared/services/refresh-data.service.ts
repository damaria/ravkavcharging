import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/internal/Subject';

export const DataMessages = {
  REFRESH_DATA: "REFRESH_DATA"
}

export interface IHandler {
  type: string,
  payload?: any,
  additionalPayload?: any
}

@Injectable({providedIn: 'root'})
export class RefreshDataService {
  public emitChangeSource = new Subject<IHandler>();
  changeEmitted$ = this.emitChangeSource.asObservable();
}
