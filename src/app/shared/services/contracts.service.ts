import { Injectable } from '@angular/core';
import { StoreService } from 'src/app/shared/services/store.service';
import { sessionKeys } from '../constants/sessionKey';

@Injectable({
  providedIn: 'root'
})
export class ContractsService {
  dataOfContract;
  currentDataOfContract;
  ettCodes;
  area: string;
  profile;

  constructor() {
    this.initParams();
  }

  initDataOfContract(isAnonymousCard) {
    this.dataOfContract = JSON.parse(sessionStorage.getItem(sessionKeys.profilesContractsFromServer));

    // if no data from the server - init dataOfContract with empty array
    if(!this.dataOfContract) {
      this.dataOfContract = [];
    }

    if (isAnonymousCard) {
      for (let i = 0; i < this.dataOfContract.length; i++) {
        let shareCode = this.dataOfContract[i];
        for (let j = 0; j < shareCode.details.length; j++) {
          let contract = shareCode.details[j];
          if (this.isFreeMoreThanWeek(contract.ettCode)) {
            shareCode.details.splice(j, 1);
            j--;
            if (shareCode.details.length == 0) {
              this.dataOfContract.splice(i, 1);
              i--;
            }
          }
        }
      }
    }
  }

  initParams() {
    this.ettCodes = ["-999"];
    this.area = "";
    this.profile = ["-999"];
  }

  setEttCode(ettCode: any[]): void {
    this.ettCodes = ettCode;
  }

  setArea(area: string): void {
    this.area = area;
  }

  setProfile(profile: string): void {
    this.profile[0] = profile;
  }

  setProfiles(profile: any[]): void {
    this.profile = profile;
  }

  getContrct(isAnonymousCard) {
    this.initDataOfContract(isAnonymousCard);
    this.currentDataOfContract = this.dataOfContract;
  }

  getContractAccordingFilter() {
    let contractsAfterFilter = [];

    if (this.dataOfContract) {
      this.currentDataOfContract = this.dataOfContract.filter(contract => contract.motContractDescr.includes(this.area) || this.area == "כל הארץ");
      this.currentDataOfContract.forEach(shareCodeContract => {
        let tmpShareCodeContract = shareCodeContract.details.filter(contract => {
          return (this.ettCodes[0] === "-999" || (this.ettCodes.indexOf(contract.ettCode) != -1)) &&
          (this.profile[0] === "-999" || ((this.profile.indexOf(contract.profileType) != -1)))
        })

        if (tmpShareCodeContract.length > 0) {
          let updatedShareCodeContract = Object.assign({}, shareCodeContract);
          updatedShareCodeContract.details = tmpShareCodeContract;
          contractsAfterFilter.push(updatedShareCodeContract);
        }
      });
    }

    return contractsAfterFilter;
  }

  isFreeMoreThanWeek(ettCode) {
    return (ettCode == 20 || ettCode == 23 || ettCode == 24 || ettCode == 25);
  }
}
