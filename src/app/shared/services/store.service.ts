import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { tap, map, filter } from 'rxjs/operators';
import { urls } from "../../shared/constants/URLs";
import * as serverConstant from "../../shared/constants/server";
import { Cashbox } from "../models/cashbox.model";
import { BehaviorSubject, Subject } from "rxjs";
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { sessionKeys } from "../constants/sessionKey";
import { OperatorConfiguration, operatorsConfiguration } from "../fromServer/operatorsConfiguration";
import * as FileSaver from 'file-saver';
import { EventCode, statusCode } from "../../shared/constants/server";
import { LoadingModalService } from "../components/modal/loading-modal/loading-modal.service";
import { MatSnackBar } from "@angular/material/snack-bar";
import { getCurrencySymbol, getLocaleCurrencyCode } from '@angular/common';
import { TranslateService } from "@ngx-translate/core";
import { Title } from "@angular/platform-browser";
import * as languages from 'src/assets/i18n/all-languages';
import { DateAdapter } from "@angular/material/core";
import moment, { Moment } from "moment";
import { LocaleService } from "./locale/locale.service";

@Injectable({ providedIn: 'root' })
export class StoreService {
    private cashBox: Cashbox;
    private cashBoxSum;
    data: any;
    operatorConfiguration: OperatorConfiguration;
    cashBoxChanged = new Subject();
    CreditValue: number;
    localeCurrencyCode;
    currencySymbol;
    REFERENCE;
    CLOSE_DAY_REEPORT_FOR;
    middleOfProcess = new BehaviorSubject<boolean>(false);

    constructor(
      private http: HttpClient,
      private router: Router,
      public dialog: MatDialog,
      private loadingModalService: LoadingModalService,
      private snackBar: MatSnackBar,
      public translateService: TranslateService,
      private titleService: Title,
      private dateAdapter: DateAdapter<Moment>,
      private localeService: LocaleService)
    {
      this.cashBox = new Cashbox();
      this.initCurrencySymbol('he'); //TODO: think if needed to be selected
      this.initTranslate();
    }

    initTranslate() {
      const allLangs = Object.keys(languages);
      this.translateService.addLangs(allLangs);
      this.translateService.use(this.getFavoriteLang());
      this.initLang();

      this.translateService.onLangChange.subscribe(() => {
        this.initLang();
      });
    }

    initLang(): void {
      this.getTranslation(translation => {
        const currentLang = this.translateService.currentLang;
        this.localeAdjustment(currentLang);
        this.documentAdjustment(translation);
        this.titleAdjustment(translation);
        this.dateAdjustment(currentLang);
        this.specificTranslations(translation);
        sessionStorage.setItem(sessionKeys.lang, currentLang);
      });
    }

    initCurrencySymbol(lang) {
      this.localeCurrencyCode = getLocaleCurrencyCode(lang);
      this.currencySymbol = getCurrencySymbol(this.localeCurrencyCode, "wide");
    }

    documentAdjustment(translation) {
      document.documentElement.lang = translation.LANG;
      document.body.style.direction = translation.DIRECTION;
    }

    titleAdjustment(translation) {
      this.titleService.setTitle(translation.APP_TITLE);
    }

    dateAdjustment(lang) {
      this.dateAdapter.setLocale(lang);
      // this.dateNamesAdjustment();
    }

    dateNamesAdjustment() {
      const date = moment();
      const d = this.dateAdapter.getDayOfWeekNames("long");
      const d2 = this.dateAdapter.getDayOfWeekNames("short");
      const g = this.dateAdapter.getMonthNames("long");
      const g2 = this.dateAdapter.getMonthNames("short");
    }

    localeAdjustment(lang) {
      this.localeService.setLocale(lang);
    }

    specificTranslations(translation) {
      this.REFERENCE = translation.GENERAL.REFERENCE;
      this.CLOSE_DAY_REEPORT_FOR = `${translation.SHARED.SERVICES.STORE.CLOSE_DAY_REEPORT} ${translation.SHARED.SERVICES.STORE.FOR}`;
    }

    getFavoriteLang(): string {
      const sessionStorageLang = sessionStorage.getItem(sessionKeys.lang);
      if (sessionStorageLang) {
        return sessionStorageLang;
      }
      const browserLang = this.translateService.getBrowserLang();
      sessionStorage.setItem(sessionKeys.lang, browserLang);
      return browserLang;
    }

    navigateToLoginPage(): void {
        const operatorId = sessionStorage.getItem(sessionKeys.operatorId);
        this.router.navigate(['/login', operatorId]);
    }

    setParams(payload: string) {
        let data = {};
        if (payload.length > 1)
            data = JSON.parse(payload);
        const query = {
            data,
            sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
        }
        return query;
    }

    setParamsToTransaction(payload: string) {
        let transaction = {};
        if (payload.length > 1)
            transaction = JSON.parse(payload);
        const query = {
            transaction,
            sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
        }
        return query;
    }

    updateCashBox() {
        return this.http
            .post(
                urls.GET_DRIVER_CASHBOX,
                {
                    data: {
                        driverCashboxTransactionId: serverConstant.general.NULL,
                        idNumber: serverConstant.general.NULL,
                        startDT: serverConstant.general.NULL,
                        endDT: serverConstant.general.NULL,
                        device_ID: sessionStorage.getItem(sessionKeys.cashboxId),
                        onlySum: true
                    },
                    sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
                }
            )
            .pipe(
              filter((res: any) => res.statusCode == statusCode.OK),
              tap(res => {
                  this.cashBox = new Cashbox();
                  let sum = 0;
                  res.data.forEach(item => {
                      if (item.CashboxDataType_ID != serverConstant.cashboxDataType.credit) {
                          let type = item.CashboxDataType_ID;
                          const count = +item.CashboxDataCount;
                          this.cashBox[type] = count;
                          sum += +item.sumCash;
                      }
                  });
                  this.cashBoxSum = sum / 100;

                  this.cashBoxChanged.next({
                      cashBox: this.cashBox,
                      cashBoxSum: this.cashBoxSum
                  });
              })
          )
    }

    getCashbox(): Cashbox {
        return Object.assign(Object.create(Object.getPrototypeOf(this.cashBox)), this.cashBox);
    }

    getCashBoxSum() {
        return this.cashBoxSum;
    }

    getOperatorConfiguration(operatorId) {
      this.operatorConfiguration = operatorsConfiguration[operatorId];
      return this.operatorConfiguration;
    }

    printReceipt(transactionId, filename) {
        return this.http
        .post(
            urls.ISSUANCE_RECEIPT,
            {
                data: {
                    driverCashboxTransactionId: transactionId,
                    overrideFile: false
                },
                sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
            },
            { responseType: 'blob'})
            .subscribe(res => {
                if (res.type == "application/octet-stream") {
                    //save file
                    FileSaver.saveAs(res, `${this.REFERENCE} - ${filename}.pdf`);
                    //print file
                    var blob = new Blob([res], { type: 'application/pdf' });
                    const blobUrl = URL.createObjectURL(blob);
                    const iframe = document.createElement('iframe');
                    iframe.style.display = 'none';
                    iframe.src = blobUrl;
                    document.body.appendChild(iframe);
                    iframe.contentWindow.print();
                    //wait until print screen open
                    setTimeout(() => {
                        this.loadingModalService.close();
                    }, 2500);
                }
                else {
                    this.loadingModalService.close();
                }
                this.middleOfProcess.next(false);
            })
    }

    printFineReceipt(fineId, filename) {
        return this.http
        .post(
            urls.ISSUANCE_INSPECTOR_FINE,
            {
                data: {
                    inspectorPassengerFineId: fineId,
                    overrideFile: false
                },
                sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
            },
            { responseType: 'blob'}
            )
            .subscribe(res => {
                if (res.type == "application/octet-stream") {
                    //save file
                    FileSaver.saveAs(res, `${this.REFERENCE} - ${filename}.pdf`);
                    //print file
                    var blob = new Blob([res], { type: 'application/pdf' });
                    const blobUrl = URL.createObjectURL(blob);
                    const iframe = document.createElement('iframe');
                    iframe.style.display = 'none';
                    iframe.src = blobUrl;
                    document.body.appendChild(iframe);
                    iframe.contentWindow.print();
                    //wait until print screen open
                    setTimeout(() => {
                        this.loadingModalService.close();
                    }, 2500);
                }
                else {
                    this.loadingModalService.close();
                    //this.dataHandling(res);
                }
            })
    }

    printRepZReceipt(repZId) {
        return this.http
            .post(
                urls.ISSUANCE_REP_Z,
                {
                    data: {
                        repZId: repZId,
                        overrideFile: false
                    },
                    sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
                },
                { responseType: 'blob'}
            )
            .subscribe(res => {
                if (res.type == "application/octet-stream") {
                    const today = new Date().toLocaleDateString();
                    //save file
                    FileSaver.saveAs(res, `${this.CLOSE_DAY_REEPORT_FOR} ${today}.pdf`);
                    //print file
                    var blob = new Blob([res], { type: 'application/pdf' });
                    const blobUrl = URL.createObjectURL(blob);
                    const iframe = document.createElement('iframe');
                    iframe.style.display = 'none';
                    iframe.src = blobUrl;
                    document.body.appendChild(iframe);
                    iframe.contentWindow.print();
                }
                else {
                    //this.dataHandling(res);
                }
            })
    }

    getEventLog() {
        const time = new Date();
        time.setHours(0, 0, 0, 0);
        let start = time.getTime().toString(); //Start from this day in last midnight
        time.setHours(24, 0, 0, 0);
        let end = time.getTime().toString(); //End from this day in next midnight

        return this.http
            .post(
                urls.GET_EVENT_LOG,
                {
                    data: {
                        deviceId: sessionStorage.getItem(sessionKeys.cashboxId),
                        endDt: end,
                        idNumber: sessionStorage.getItem(sessionKeys.userId),
                        jsonObjRes: true,
                        startDt: start
                    },
                    sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
                }
            )
            .pipe(
                map((res: any) => {
                  const events = res.data;
                  return this.filterEvents(events);
                }),
                map(filteredEvents => {
                  let utcEvents = [];
                  filteredEvents.forEach(event => {
                    utcEvents.push({
                      ...event,
                      EntryDt: new Date(`${event.EntryDt} UTC`)
                    })
                  });
                  return utcEvents;
                })
            )
    }

    filterEvents = (events) => {
        let filteredEvents = events;
        for (let index = 0; index < filteredEvents.length; index++) {
          if (filteredEvents[index].EventCode == EventCode.OpenShift) {
            if (index != 0) {
              filteredEvents.splice(0, index);
              filteredEvents = this.filterEvents(filteredEvents);
            }
          }
        }
        return filteredEvents;
    }

    openSnackBar(message: string, action: string, duration: number) {
      this.snackBar.open(message, action, {
        duration: duration,
      });
    }

    getTranslation(callbackFunc, lang?: string) {
      const choosenLang = lang ? lang : this.translateService.currentLang;
      const langsFromStorage = JSON.parse(sessionStorage.getItem(sessionKeys.i18n));
      if (langsFromStorage && langsFromStorage[choosenLang]) {
        callbackFunc(langsFromStorage[choosenLang]);
      }
      else {
        this.translateService.getTranslation(choosenLang).subscribe(translation => {
          callbackFunc(translation);
        });
      }
    }
}
