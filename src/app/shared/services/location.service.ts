import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class LocationService {
  latitude: number = 0;
  longitude: number = 0;

  constructor() {
    navigator.geolocation.getCurrentPosition(position => {
      this.latitude = position.coords.latitude;
      this.longitude = position.coords.longitude;
    },
    null,
    { enableHighAccuracy: true });
  }
}
