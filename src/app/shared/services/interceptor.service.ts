import { HttpEventType, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { from, of } from "rxjs";
import { concatMap, filter, tap } from "rxjs/operators";
import { ErrorModalService } from "../components/modal/error-modal/error-modal-service.service";
import { LoadingModalService } from "../components/modal/loading-modal/loading-modal.service";
import { statusCode } from "../constants/server";
import { sessionKeys } from "../constants/sessionKey";
import { urls } from "../constants/URLs";

export class InterceptorService implements HttpInterceptor {
  constructor(private loadingModalService: LoadingModalService, private errorModalService: ErrorModalService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    if (this.needToOpenLoadingModal(req.url)) {
      this.loadingModalService.open();
    }
    return next.handle(req).pipe(
      filter(event => event.type == HttpEventType.Response),
      concatMap(event => {
        if (this.blobError(event)) {
          return from(this.convertBodyEventToJson(event));
        }
        return of(event);
      }),
      tap((event: any) => {
        if (this.needToCloseLoadingModal(event)) {
          this.loadingModalService.close();
        }
        if (this.isI18nRes(event)) {
          //Occurs once for each language
          this.insertLangToSessionStorage(event as HttpResponse<any>);
        }
        else if (this.needToOpenErrorModal(event)) {
          this.errorModalService.sendError(event);
        }
      })
    )
  }

  insertLangToSessionStorage(event: HttpResponse<any>) {
    const receivedNowLang = {[event.body.LANG]: event.body};
    const existingLangs = JSON.parse(sessionStorage.getItem(sessionKeys.i18n));
    const allLangsMerged = Object.assign(receivedNowLang, existingLangs);
    sessionStorage.setItem(sessionKeys.i18n, JSON.stringify(allLangsMerged));
  }

  needToOpenLoadingModal(url) : boolean {
    return (url != urls.CHECK_LOGIN && url != urls.GET_EVENT_LOG && !url.includes(urls.I18N))
  }

  needToCloseLoadingModal(event) : boolean {
    return this.needToOpenLoadingModal(event.url) && !(event.body instanceof Blob);
  }

  needToOpenErrorModal(event) : boolean {
    return this.blobError(event) || (this.generalError(event) && event.url != urls.CHECK_LOGIN);
  }

  isI18nRes(event) : boolean {
    return event.url.includes(urls.I18N);
  }

  generalError(event) {
    return (!(event.body instanceof Blob) && event.body.statusCode != statusCode.OK && !this.exceptionallyNoError(event));
  }

  blobError(event) {
    return (event.body instanceof Blob && event.body.type != "application/octet-stream");
  }

  async convertBodyEventToJson(event): Promise<Object> {
    return {
      ...event,
      body: JSON.parse(await event.body.text())
    }
  }

  exceptionallyNoError(event) {
    return event.body.statusCode == statusCode.CASHBOX_LOCK ||
    event.body.statusCode == statusCode.RESULTSTATUSLOGOUT ||
    event.url == urls.UPDATE_PASSENGER_FINE ||
    event.url == urls.ADD_PERSONALIZATION ||
    event.url == urls.ADD_LOAD_CONTRACT_TRANSACTION ||
    event.url == urls.CAN_LOAD_CONTRACT_ON_CARD;
  }
}
