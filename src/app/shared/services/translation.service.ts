import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {BehaviorSubject, concatMap, delayWhen, firstValueFrom, lastValueFrom, Observable, retryWhen, timer} from 'rxjs';
import {filter, first, map} from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class TranslationService {

  private static translateServiceSubject = new BehaviorSubject<TranslateService | null>(null);

  constructor(private translateService: TranslateService) {
    TranslationService.translateServiceSubject.next(translateService);
  }

  /* the following static methods are should be used sparingly and ONLY when the service cannot be injected */

  public static staticGet(key: string | Array<string>, interpolateParams?: any): Observable<string | any> {
    return TranslationService.getReadyTranslationService()
      .pipe(concatMap(_translateService => _translateService.stream(key, interpolateParams)));
  }

  public static staticGetPromise(key: string | Array<string>, interpolateParams?: any): Promise<string | any> {
    return lastValueFrom(
      TranslationService.getReadyTranslationService()
        .pipe(concatMap(_translateService => _translateService.get(key, interpolateParams)), first())
    );
  }

  // the base method of this service, used to obtain the underline service
  private static getReadyTranslationService(): Observable<TranslateService> {
    return TranslationService.translateServiceSubject.pipe(
      filter(_translateService => _translateService != null),
      map(_translateService => _translateService as TranslateService),
      map(_translateService => {
        const langs = _translateService.getLangs();
        if (langs != null && langs.length > 0) {
          return _translateService;
        } else {
          throw new Error();
        }
      }),
      retryWhen(erroredStream => erroredStream.pipe(delayWhen(() => timer(100)))
      )
    );
  }

  /* methods for fetching translations */

  public get(key: string | Array<string>, interpolateParams?: any): Observable<string | any> {
    return TranslationService.getReadyTranslationService()
      .pipe(concatMap(_translateService => _translateService.stream(key, interpolateParams)));
  }

  public getSingle(key: string | Array<string>, interpolateParams?: any): Observable<string | any> {
    return this.get(key, interpolateParams).pipe(first());
  }

  public getPromise(key: string | Array<string>, interpolateParams?: any): Promise<string | any> {
    return lastValueFrom(this.getSingle(key, interpolateParams));
  }

  public getNow(key: string | Array<string>, interpolateParams?: any): string | any {
    return this.translateService.stream(key, interpolateParams);
  }

  /* methods for setting and fetching the default language */

  public setDefaultLang(defaultLanguage: string): void {
    this.translateService.setDefaultLang(defaultLanguage);
  }

  public getDefaultLang(): string {
    return this.translateService.getDefaultLang();
  }

  /* methods for setting and fetching the current language */

  public getCurrentLang(): string {
    return this.translateService.currentLang;
  }

  public setCurrentLang(language: string): string {
    return this.translateService.currentLang = language;
  }

  /* methods for getting the available languages */

  public getLanguages(): string[] {
    return this.translateService.getLangs();
  }

  public getLanguagesWhenReady(): Promise<string[]> {
    return firstValueFrom(TranslationService.getReadyTranslationService()
      .pipe(map(_translateService => _translateService.getLangs())));
  }

}
