export enum SupportCreditPayReport {
    NO,
    WITH_IFRAME,
    WITHOUT_IFRAME
}

export enum ExperationDateOfOperatorProfile {
  ONE_YEAR_FROM_NOW,
  END_OF_YEAR
}

export const
operatorsConfiguration: OperatorsConfiguration = {
  4: { // eged-taabura
      supportCredit: false,
      supportVouchers: true,
      supportMultiMonthFreeMonthly: false,
      operatorProfile: 58,
      experationDateOfOperatorProfile: ExperationDateOfOperatorProfile.ONE_YEAR_FROM_NOW,
      canSkipStocktakingOnStartShift: false,
      hasRules: false,
      supportCreditPayReport: SupportCreditPayReport.NO,
      needSendToServerAddingProfile: true,
      supportGeneralDepositFromCashbox: true,
      reasonOfDepositToCashbox: true,
      supportEditProfile: true
  },
  8: { // GB-tours
      supportCredit: true,
      supportVouchers: false,
      supportMultiMonthFreeMonthly: true,
      operatorProfile: 13,
      experationDateOfOperatorProfile: ExperationDateOfOperatorProfile.ONE_YEAR_FROM_NOW,
      canSkipStocktakingOnStartShift: true,
      hasRules: false,
      supportCreditPayReport: SupportCreditPayReport.NO,
      needSendToServerAddingProfile: false,
      supportGeneralDepositFromCashbox: false,
      reasonOfDepositToCashbox: true,
      supportEditProfile: true
  },
  15: { // metropolin
    supportCredit: false,
    supportVouchers: false,
    supportMultiMonthFreeMonthly: false,
    operatorProfile: 13,
    experationDateOfOperatorProfile: ExperationDateOfOperatorProfile.ONE_YEAR_FROM_NOW,
    canSkipStocktakingOnStartShift: true,
    hasRules: false,
    supportCreditPayReport: SupportCreditPayReport.NO,
    needSendToServerAddingProfile: true,
    supportGeneralDepositFromCashbox: true,
    reasonOfDepositToCashbox: true,
    supportEditProfile: false
  },
  34: { // tnufa
      supportCredit: false,
      supportVouchers: false,
      supportMultiMonthFreeMonthly: false,
      operatorProfile: 13,
      experationDateOfOperatorProfile: ExperationDateOfOperatorProfile.ONE_YEAR_FROM_NOW,
      canSkipStocktakingOnStartShift: false,
      hasRules: false,
      supportCreditPayReport: SupportCreditPayReport.WITHOUT_IFRAME,
      needSendToServerAddingProfile: false,
      supportGeneralDepositFromCashbox: false,
      reasonOfDepositToCashbox: true,
      supportEditProfile: true
  },
  37: { //extra
      supportCredit: true,
      supportVouchers: true,
      supportMultiMonthFreeMonthly: false,
      operatorProfile: 13,
      experationDateOfOperatorProfile: ExperationDateOfOperatorProfile.END_OF_YEAR,
      canSkipStocktakingOnStartShift: true,
      hasRules: true,
      supportCreditPayReport: SupportCreditPayReport.WITH_IFRAME,
      needSendToServerAddingProfile: false,
      supportGeneralDepositFromCashbox: false,
      reasonOfDepositToCashbox: true,
      supportEditProfile: true
  },
  38: { //extra Jerusalem
    supportCredit: true,
    supportVouchers: true,
    supportMultiMonthFreeMonthly: false,
    operatorProfile: 13,
    experationDateOfOperatorProfile: ExperationDateOfOperatorProfile.END_OF_YEAR,
    canSkipStocktakingOnStartShift: true,
    hasRules: true,
    supportCreditPayReport: SupportCreditPayReport.WITH_IFRAME,
    needSendToServerAddingProfile: false,
    supportGeneralDepositFromCashbox: false,
    reasonOfDepositToCashbox: true,
    supportEditProfile: true
  },
  24: { //clalit-golan
    supportCredit: false,
    supportVouchers: false,
    supportMultiMonthFreeMonthly: false,
    operatorProfile: 13,
    experationDateOfOperatorProfile: ExperationDateOfOperatorProfile.ONE_YEAR_FROM_NOW,
    canSkipStocktakingOnStartShift: true,
    hasRules: false,
    supportCreditPayReport: SupportCreditPayReport.NO,
    needSendToServerAddingProfile: false,
    supportGeneralDepositFromCashbox: false,
    reasonOfDepositToCashbox: true,
    supportEditProfile: true
  }
}

interface OperatorsConfiguration {
    [operatorId: number]: OperatorConfiguration;
}

export interface OperatorConfiguration {
    supportCredit: boolean,
    supportVouchers: boolean,
    supportMultiMonthFreeMonthly: boolean,
    operatorProfile: number,
    experationDateOfOperatorProfile: ExperationDateOfOperatorProfile,
    canSkipStocktakingOnStartShift: boolean,
    hasRules: boolean,
    supportCreditPayReport: SupportCreditPayReport,
    needSendToServerAddingProfile: boolean,
    supportGeneralDepositFromCashbox: boolean,
    reasonOfDepositToCashbox: boolean,
    supportEditProfile: boolean
}
