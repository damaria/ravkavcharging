import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Observable, of } from 'rxjs';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';
import { StoreService } from 'src/app/shared/services/store.service';

@Injectable({providedIn: 'root'})
export class LoginGuard implements CanActivate, CanLoad {
  public constructor(public storeService: StoreService, public router: Router) {}

  public canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> {
    return this.getActivationCondition();
  }

  public canLoad(route: Route, segments: UrlSegment[]): Observable<boolean | UrlTree> {
    return this.getActivationCondition();
  }

  private getActivationCondition(): Observable<boolean | UrlTree> {
    const sessionData = sessionStorage.getItem(sessionKeys.sessionData);
    //if has sessionData
    if (!!sessionData) {
      return of(true);
    }
    //else - navigate to login page
    const operatorId = sessionStorage.getItem(sessionKeys.operatorId);
    this.router.navigate(['/login', operatorId]);
    return of(false);
  }
}
