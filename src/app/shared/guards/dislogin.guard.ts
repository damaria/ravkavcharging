import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Observable, of } from 'rxjs';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';
import { StoreService } from 'src/app/shared/services/store.service';

@Injectable({providedIn: 'root'})
export class DisloginGuard implements CanActivate, CanLoad {
  public constructor(public storeService: StoreService, public router: Router) {}

  public canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> {
    return this.getActivationCondition();
  }

  public canLoad(route: Route, segments: UrlSegment[]): Observable<boolean | UrlTree> {
    return this.getActivationCondition();
  }

  private getActivationCondition(): Observable<boolean | UrlTree> {
    const sessionData = sessionStorage.getItem(sessionKeys.sessionData);
    // if user in a break or no connected
    const onBreakList: any = JSON.parse(sessionStorage.getItem(sessionKeys.onBreak));
    const userEmail = sessionStorage.getItem(sessionKeys.userEmail);
    if (!sessionData) {
      return of(true);
    }
    if (onBreakList && onBreakList.some(element => element.userEmail = userEmail)) {
      return of(true);
    }
    this.router.navigate(['/main']);
    return of(false);
  }
}
