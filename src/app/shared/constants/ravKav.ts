export enum EnvCountryID {
    ISRAEL = 886
}

export enum ContractPriority {
    CURRENT_SP_OR_FC = 0,
    NEXT_SP = 1,
    CURRENT_RESTRICTED_SP = 2,
    NEXT_RESTRICTED_SP = 3,
    CURRENT_T_OR_SV = 4,
    NEXT_T_OR_SV = 5,
    CONTRACT_NOT_FOR_TRANSPORT = 12,
    INVALID = 13,
    ERASABLE = 14,
    ABSENT = 15
}

export enum TariffContractNameCode {
  ONE_TIME_OR_MULTI_RIDE_TICKET = 1,
  SEASON_PASS = 2,
  TRANSFER_TICKET = 3,
  FREE_CERTIFICATE = 4,
  PARK_AND_RIDE_SP = 5,
  STORED_VALUE = 6,
  ONE_TIME_OR_MULTI_RIDE_TICKET_EXTENTION = 7,
  SLIDING_SEASON_PASS = 8,
}

export const GOLD_KAV_PROFILE = "45";
export const STUDENT_PROFILE = "3";
