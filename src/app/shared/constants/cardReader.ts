export const webSocketUrl = "ws://localhost";
export const webSocketDefaultPort = "80";

export enum Commands {
    GET_READERS = "getReaders",
    READER = "reader",
    READ_RAVKAV = "readRavKav",
    WRITE_RAVKAV = "writeRavKavMultipleEvents",
    WRITE_RAVKAV_OBJECTS = "writeRavKavObjects",
    CONNECT_TO_READERS = "connectToReaders",
    LOG = "log"
}

export enum ReaderStatus {
    FAILED = "Failed",
    DISCONNECTED = "Disconnected",
    SAM_NOT_EXISTS = "SAM Not Exists",
    CONNECTED = "Connected",
    READY = "Ready"
}
