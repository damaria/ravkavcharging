export enum loginSource {
    BROWSER = '4'
}

export enum deviceType {
    CASHBOX = '2',
    RAV_KAV_CHARGING = '4'
}

export enum shiftType {
    CASHBOX = '2',
    RAV_KAV_CHARGING = '4'
}
export enum StatusCode {
    OPEN_SHIFT = '1',
    NEW_SHIFT = '2'
}

export enum androidAppVersion {
    '1.4.4' = '1.4.4'
}

export enum general {
    NULL = '-999'
}

export enum endShiftStatus {
    MANUAL = '2',
    AUTO = '4'
}

export enum typeRoleId {
    USER = '1',
    ADMIN = '3',
}

export enum cashboxStatus {
    OPEN = '1',
    LOCKED = '2'
}

export enum alsoOpenShifts {
    TRUE = '1',
    FALSE = '2'
}

export enum deposited {
    WITHOUT = '0',
    JUST = '1',
    ALSO = '',
}

export enum detailsSumCode {
    DETAILS = '1',
    SUM = '2',
    ALL = ''
}

export enum statusCode {
    OK = 1,
    RESULTSTATUSLINE = 2,
    SERVER_ERROR = 3,
    DB_ERROR = 4,
    RESULTSTATUSLOGOUT = 5,
    RESULTSTATUSPERMISSION = 6,
    RESULTSTATUSSESSIONORUIDNOTEXISTS = 7,
    ERRORFROMTRANSACTIONINSERT = 8,
    ERRORFROMANDROIDLOGINDRIVER = 9,
    ERRORIDNOTFOUND = 10,
    ERROREMPLOYEENOTFOUNDFORTHISOPERATOR = 11,
    ERRORNOTACTIVE = 12,
    ERRORCODENOTMATCH = 13,
    ERRORINSERTEMPLOYEE = 14,
    ERRORIDNOTDRIVER = 15,
    ERRORINSERTPARAMS = 16,
    ERRORADDTYPE_ID = 17,
    ERRORSAVETRIP = 18,
    ERRORSAVESHIFT = 19,
    ERRORSIGNUP = 20,
    ERRORDELETEEMPLOYEE = 21,
    ERRORDELETETYPEEMPLOYEE = 22,
    ERRORUPDATEEMPLOYEE = 23,
    ERROREMPLOYEENOTFOUND = 24,
    ERRORUPDATEEMPLOYEEPASSWORD = 25,
    ERRORLOGIN = 26,
    ERRORTRYFIREBASE = 27,
    ERRORFROMFIREBASE = 28,
    ERRORINSERTEXSISTSEMPLOYEE = 29,
    ERRORDELETEEMPLOYEEFROMFIREBASE = 30,
    DUPLICATE = 31,
    ITEMNOTFOUND = 32,
    ERRORUPDATE = 33,
    ERROROPENFOLDER = 34,
    ERRORUPLOADFILEZILLA = 35,
    ERROREMAILEXSITSFIREBASE = 36,
    ERROREMAILFIREBASE = 37,
    ERROREPASSWORDLENGTHFIREBASE = 38,
    HAVENOFBUID = 39,
    ERRORIDNOTMATCHTYPE = 40,
    ERRORMAILNOTFIND = 41,
    ERRORMISSINGFB_UID = 42,
    MULTYOPERATORTOEMPLOYEE = 43,
    ERRORINSERTEXISTSEMPLOYEETOTHISOPERATOR = 44,
    ERRORFINISHSIGNUPFBUID = 45,
    ERROREMPLOYEENOTEXISTSINANOTHEROPERATOR = 46,
    RESULTEMPLOYEEEXISTSJUSTINANOTHEROPERATOR = 47,
    ERROROPERATORNAMENOTEXISTS = 48,
    ERRORBUFFEREDWRITERNOTFOUND = 49,
    RESULTSTATUSNEWTOKEN = 50,
    ERRORWRITETOFILE = 51,
    RESULTDRIVERCASHBOXNOTEXISTS = 52,
    OPTION_NOT_EXISTS = 53,
    SETTING_NOT_FOUND = 54,
    TOKEN_EXPIRED = 55,
    NO_PHONE_IN_VERIFICATION = 56,
    CARD_IN_USE = 57,
    CARD_RECOVERY_NOT_FOUND = 58,
    CARD_NOT_CORRECT = 59,
    CASHBOX_LOCK = 60,
    CARD_NOT_ACTIVE = 61,
    TOO_MATCH_ITEMS = 62,
    LOADING_CONTRACT = 63,
    CANCELING_CONTRACT = 64,
    PERSONALIZATION = 65,
    SERVER_BUSY = 66,
    INTERFACES_INFO = 67,
    INCORRECT_PINCODE = 68,
    ERROR_CREATE_FILE = 69,
}

export enum ContractAction {
  LOAD = '1',
  CANCEL = '2',
}

export enum ContractActionType {
  LOAD = '1',
  CANCEL = '2',
  CANCEL_FOR_LOAD = '4',
  LOAD_FOR_CANCEL = '5'
}

export enum EventCircumstances {
    UNSPECIFIED = '0',
    ENTRY = '1',
    EXIT = '2',
    PASSAGE = '3',
    ON_BOARD_CONTROL = '4',
    TEST = '5',
    INTERCHANGE_ENTRY = '6',
    INTERCHANGE_EXIT = '7',
    OTHER_USAGE = '8',
    CANCELLETION = '9',
    CONTRACT_LOADING_WITH_IMMEDIATE_FIRST_USE = '12',
    CONTRACT_LOADING = '13',
    APPLICATION_ISSUING = '14',
    INVALIDATION = '15'
}

export enum ActionCode {
    LOAD_OR_CANCEL_CONTRACT = '13',
    ADD_PERSONALIZATION = '26'
}

export enum cashboxDataType {
    _10 = 1,
    _50 = 2,
    _100 = 3,
    _200 = 4,
    _500 = 5,
    _1000 = 6,
    _2000 = 7,
    _5000 = 8,
    _10000 = 9,
    _20000 = 10,
    cancel = 11,
    idfVoucher = 12,
    credit = 13,
    studentVoucher = 14,
    voucherSum = 15
}

export enum actionTypeTransaction {
    ADD_TRANSACTION = "1",
    REMOVE_TRANSACTION = "2",
    UPDATE_TRANSACTION = "3",
    TAKE_OUT_MONEY_PROACTIVELY = "4",
    ENTER_MONEY_PROACTIVELY = "5",
    DEPOSIT_TRANSACTION = "6",
    FIX_CASHBOX_AMOUNT_PROACTIVELY = "7",
    REJECTION_FIX_CASHBOX = "8",
    DEBT_REPAYMENT = "9",
    LOAD_CONTRACT = "10",
    CANCEL_CONTRACT = "11",
    PAY_REPORT = "16"
}

export enum eventLogCodes {
    GO_TO_BREAK = "4",
    RETURN_FROM_BREAK = "6",
    LOGIN = "9",
    LOGOUT = "10"
}

export function getValue(code) {
    if (code > 10) {
        return 0;
    }
    return +cashboxDataType[code].slice(1);
}

interface dataClass {
    firebase: {},
    sessionData: {},
    userData: {}
}

export enum EventCode {
    OpenShift = "1",
    stocktaking = "2",
}

export interface ServerResponse {
    data: dataClass | any,
    dateTimeMillis?: number,
    dateTime: string,
    description: string,
    descriptionForUserHE: string,
    newToken: boolean,
    statusCode: number
}

export interface ErrorStatus {
    code: number,
    description: string
}

export enum FineStatusCode {
    NO_EXIST = "-1", // no on server!
    NEED_TO_PAY = "1",
    PAID = "2",
    OBJECTION = "3",
    CANCELED = "4"
}

export enum PaymentType {
    CREDIT = "1",
    CASH = "2"
}

export enum SmartIdType {
  RAV_KAV = "1"
}

export const CirtusMahar = {
  YES: true,
  NO: false,
}

export const CardDataModelVersion = "2.06.1";
