export let baseUrl;
export const devMode = window.location.origin.includes('localhost');
export const testMode = window.location.href.includes('Test');
export const prodMode = !devMode && !testMode;

if (devMode) {
    baseUrl = 'https://isrticketing.isrcorp.co.il/isrTicketingTest/v1';
}
else {
    baseUrl = window.location.origin + (testMode ? '/isrTicketingTest/v1': '/isrTicketing/v1');
}

export const urls = {
    LOGIN: `${baseUrl}/driverCashbox/signIn`,
    LOGOUT: `${baseUrl}/driverCashbox/logout`,
    GET_DEVICE: `${baseUrl}/devices/getDevice`,
    GET_OPEN_LOGIN_SESSION: `${baseUrl}/driverCashbox/getOpenLoginSession`,
    GET_SHIFT_ID: `${baseUrl}/driverCashbox/getShiftId`,
    END_SHIFT: `${baseUrl}/driverCashbox/endShift`,
    START_SHIFT: `${baseUrl}/driverCashbox/startShift`,
    CHECK_LOGIN: `${baseUrl}/login/checkLogin`,
    GET_REP_Z: `${baseUrl}/driverCashbox/getRepZ`,
    ADD_REP_Z: `${baseUrl}/driverCashbox/addRepZ`,
    CLOSE_CASHBOX: `${baseUrl}/driverCashbox/closeCashbox`,
    GET_DRIVER_CASHBOX: `${baseUrl}/driverCashbox/getDriverCashboxTransaction`,
    ADD_DRIVER_CASHBOX: `${baseUrl}/driverCashbox/addDriverCashboxTransaction`,
    STOCKTAKING_WITH_LOCK: `${baseUrl}/driverCashbox/stocktakingWithLock`,
    STOCKTAKING: `${baseUrl}/driverCashbox/stocktaking`,
    GET_OWE_DRIVERS_REPORT: `${baseUrl}/driverCashbox/getOweDriversReport`,
    ADD_EVENT_LOG: `${baseUrl}/driverCashbox/addEventLog`,
    GET_CONTRACTS: `${baseUrl}/contract/getContract`,
    ISSUANCE_RECEIPT: `${baseUrl}/driverCashbox/issuanceReceipt`,
    ISSUANCE_INSPECTOR_FINE: `${baseUrl}/driverCashbox/issuanceInspectorFine`,
    ISSUANCE_REP_Z: `${baseUrl}/driverCashbox/issuanceRepZ`,
    DATA_RECORDS: `${baseUrl}/dataRecords`,
    GET_EVENT_LOG: `${baseUrl}/driverCashbox/getEventLog`,
    CAN_LOAD_CONTRACT_ON_CARD: `${baseUrl}/contract/canLoadContractOnCard`,
    GET_LOAD_CONTRACT_DUMP: `${baseUrl}/contract/getLoadContractDump`,
    CAN_CANCEL_CONTRACT_ON_CARD: `${baseUrl}/contract/canCancelContractOnCard`,
    GET_CANCELLATION_LOAD_CONTRACT: `${baseUrl}/contract/getCancellationLoadContract`,
    GET_LOAD_PERSONALIZATION_DUMP: `${baseUrl}/contract/getLoadPersonalizationDump`,
    ADD_PERSONALIZATION: `${baseUrl}/contract/addPersonalization`,
    GET_PASSENGER_FINE: `${baseUrl}/inspectorReport/getPassengerFine`,
    UPDATE_PASSENGER_FINE: `${baseUrl}/inspectorReport/updatePassengerFine`,
    GET_CREDIT_URL_37: devMode || testMode ?
    `https://isrticketing.isrcorp.co.il/public-transport-payment-server-staging/v1/generate/specific-payment-url` :
    `https://isrticketing.isrcorp.co.il/public-transport-payment-server/v1/generate/specific-payment-url`,
    PAY_FINE: devMode || testMode ?
    `https://isrticketing.isrcorp.co.il/public-transport-payment-server-staging/v1/fine/credit-card` :
    `https://isrticketing.isrcorp.co.il/public-transport-payment-server/v1/fine/credit-card`,
    GET_DRIVER_CASHBOX_DATA_TYPE: `${baseUrl}/driverCashbox/getDriverCashboxDataType`,
    ADD_FREE_EMPLOYEE_CARD: `${baseUrl}/employee/addFreeEmployeeCard`,
    ADD_LOAD_CONTRACT_TRANSACTION: `${baseUrl}/contract/addTransaction`,
    GET_STORED_VALUE_BY_DISCOUNT: `${baseUrl}/contract/getStoredValueByDiscount`,
    GET_MOT_ZONE: `${baseUrl}/geography/getMOTZone`,
    I18N: `/assets/i18n/`
}
