import { EnvCountryID } from "./ravKav";
import { MainService } from '../../pages/main/main.service';
import { Commands, ReaderStatus } from "./cardReader";
import { EventCircumstances } from "./server";
import { sessionKeys } from "./sessionKey";

let connectionOfWS;
const requiredReader = "ACS ACR";
const requiredVesionNumber = 0;

export function sendGetReadersCommand(connection) {
    connectionOfWS = connection;
    var msg = {
        command: Commands.GET_READERS
    };
    connectionOfWS.send(JSON.stringify(msg));
}

export function tryConnectToReaders(data, mainService: MainService) {
    if (data.status == ReaderStatus.FAILED) {
        mainService.noCardReader();
    }
    else {
        let cardReaderIndex = -1;
        let samReaderIndex = -1;

        data.cardReaders.forEach(cardReader =>
            cardReaderIndex = (cardReader.name.includes(requiredReader) && mainService.equalToSuffix(cardReader)) ? cardReader.index : cardReaderIndex
        );
        data.samReaders.forEach(samReader =>
            samReaderIndex = (samReader.name.includes(requiredReader) && mainService.equalToSuffix(samReader)) ? samReader.index : samReaderIndex
        );

        var msg;

        if (cardReaderIndex != -1 && samReaderIndex != -1) {
            msg = {
                command: Commands.CONNECT_TO_READERS,
                cardReaderIndex: cardReaderIndex,
                samReaderIndex: samReaderIndex
            };
            connectionOfWS.send(JSON.stringify(msg));
        }
        else {
            if (mainService.currReaderSuffix == 0) {
                mainService.currReaderSuffix = 1;
                msg = {
                    command: Commands.GET_READERS
                };
                connectionOfWS.send(JSON.stringify(msg));
            }
            else {
                mainService.noCardReader();
            }
        }
    }
}

export function handleReaderStatus(data, mainService: MainService) {
    switch (data.status) {
        case ReaderStatus.DISCONNECTED:
            if (!data.cardReaderName) {
                mainService.noCardReader();
            }
            break;
        case ReaderStatus.SAM_NOT_EXISTS:
            if (mainService.currReaderSuffix == 0) {
                mainService.currReaderSuffix = 1;
                var msg = {
                    command: Commands.GET_READERS
                };
                connectionOfWS.send(JSON.stringify(msg));
            }
            else {
                mainService.noCardReader();
            }
            break;
        case ReaderStatus.CONNECTED:
            mainService.cardReaderConnected();
            break;
        case ReaderStatus.READY:
            mainService.cardReaderConnected();
            sessionStorage.setItem(sessionKeys.cardReaderSamSerialNumber, data.samSerialNum);
            var msg = {
                command: Commands.READ_RAVKAV
            };
            connectionOfWS.send(JSON.stringify(msg));
            break;
    }
}

export function handleReadRavKav(data, mainService: MainService) {
    if (data.status == ReaderStatus.FAILED) {
        mainService.failedToReadRavkav();
        return false;
    }

    const validDateOfRavKav = new Date(data.environment.envendDate).getTime();

    if (data.environment.envCountryID !== EnvCountryID.ISRAEL) {
        mainService.noIsraeliCard();
        return false;
    }

    if (validDateOfRavKav < new Date().setUTCHours(0,0,0,0)) {
        mainService.cardNoValid();
        return false;
    }

    const thisCardVersionsError = cardVersionsError(data);
    if (thisCardVersionsError.hasError) {
        mainService.cardVersionsError(thisCardVersionsError);
        return false;
    }

    const lastEvent = data.events[0];
    if (lastEvent.eventCircumstances == EventCircumstances.INVALIDATION) {
        mainService.burnedCard();
        return false;
    }

    return true;
}

function cardVersionsError(cardData) {
    let errorStatus = {
        hasError: false,
        contracts: false,
        environment: false,
        events: false,
        specialEvents: false
    };
    //contracts
    cardData.contracts.forEach(contract => {
        if (contract && contract.contractVersionNumber != requiredVesionNumber) {
            errorStatus.contracts = true;
            errorStatus.hasError = true;
        }
    });
    //environment
    if (cardData.environment.envApplicationVersionNumber != requiredVesionNumber) {
        errorStatus.environment = true;
        errorStatus.hasError = true;
    }
    //events
    cardData.events.forEach(event => {
        if (event && event.eventVersionNumber != requiredVesionNumber) {
            errorStatus.events = true;
            errorStatus.hasError = true;
        }
    });
    //specialEvents
    cardData.specialEvents.forEach(specialEvent => {
        if (specialEvent && specialEvent.eventVersionNumber != requiredVesionNumber) {
            errorStatus.specialEvents = true;
            errorStatus.hasError = true;
        }
    });

    return errorStatus;
}

export function writeToCard(data) {
  const events = data.events;
  const specialEvents = data.specialEventsUpdated;

  let msg = {
    command: Commands.WRITE_RAVKAV,
    ...data.environment && // just if has enviroment
    {
      environment: {
        "dump": data.environment
      }
    },
    ...data.contract && // just if has contract
    {
      contract: {
        "dump": data.contract,
        "position": data.position
      }
    },
    ...data.counter && // just if has counter
    {
      counter: {
        "dump": data.counter
      }
    },
    ...events && // just if has events
    {
      events: convertEventsToCardReader(events)
    },
    ...specialEvents?.length == 1 && // if has just one specialEvent
    {
      specialEvent: {
        "dump": specialEvents[0].specialEvent,
        "position": specialEvents[0].position
      }
    },
  };

  connectionOfWS.send(JSON.stringify(msg));

  if (specialEvents.length > 1) {
    for (let specialEventsIndex = specialEvents.length - 1; specialEventsIndex > -1; specialEventsIndex--) {
      let msg = {
        command: Commands.WRITE_RAVKAV,
        specialEvent: {
          "dump": specialEvents[specialEventsIndex].specialEvent,
          "position": specialEvents[specialEventsIndex].position
        }
      };

      connectionOfWS.send(JSON.stringify(msg));
    }
  }
}

function convertEventsToCardReader(events: string[]) : {}[] {
  let eventsToCardReader = [];
  events.forEach(event => {
    eventsToCardReader.push({
      "dump": event
    });
  });
  return eventsToCardReader.reverse();
}

export function writeToLogger(message: string) {
  const msg = {
    command: Commands.LOG,
    message: message
  };

  connectionOfWS.send(JSON.stringify(msg));
}
