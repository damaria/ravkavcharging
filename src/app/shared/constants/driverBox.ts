export enum CompareResult {
    IDENTICAL,
    DIFFERENT_COIN_COMPOSITION,
    DIFFERENT_SUM,
}

export enum StocktakingStatus {
    START_SHIFT = '0',
    END_SHIFT = '1',
    MIDDLE_SHIFT = '2',
    END_DAY = '3'
}

export enum From {
    DISPLAY_DETAILS,
    DISPLAY_CONTRACT
}