import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DEFAULT_CURRENCY_CODE, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { MainComponent } from './pages/main/main.component';
import { HomeComponent } from './pages/main/home/home.component';
import { StocktakingComponent } from './pages/main/stocktaking/stocktaking.component';
import { DepositToChashboxComponent } from './pages/main/deposit-to-chashbox/deposit-to-chashbox.component';
import { DepositToBankComponent } from './pages/main/deposit-to-bank/deposit-to-bank.component';
import { ChangingComponent } from './pages/main/changing/changing.component';
import { MoneyHeaderComponent } from './pages/main/components/money-header/money-header.component';
import { MoneyHeaderComponentComponent } from './pages/main/components/money-header/money-header-component/money-header-component.component';
import { KeysPipe } from './shared/pipes/keys.pipe';
import { CashboxKeysPipe } from './shared/pipes/cashboxKeys.pipe';
import { SearchInTablePipe } from './shared/pipes/search-in-table.pipe';
import { MenuHeaderComponent } from './pages/main/components/menu-header/menu-header.component';
import { ToPromisePipe } from './shared/pipes/toPromise.pipe';
import { AmountInputComponent } from './shared/components/amount-input/amount-input.component';
import { ModalComponent } from './shared/components/modal/modal.component';
import { SelectCashboxModalComponent } from './pages/login/components/select-cashbox-modal/select-cashbox-modal.component';
import { LoginPopUpComponent } from './pages/login/components/login-pop-up/login-pop-up.component';
import { PopUpComponent } from './shared/components/pop-up/pop-up.component';
import { InfoModalComponent } from './shared/components/modal/info-modal/info-modal.component';
import { SpinnerComponent } from './shared/components/spinner/spinner.component';
// import { MatSliderModule } from '@angular/material/slider';
import { DisplayDetailsOfRavKavComponent } from './pages/main/haveRavKav/dialog-rav-kac/components/display-details-of-rav-kav/display-details-of-rav-kav.component';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTabsModule } from '@angular/material/tabs';
import { MatButtonModule } from '@angular/material/button';
import { DisplayContractComponent } from './pages/main/haveRavKav/dialog-rav-kac/components/display-contract/display-contract.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
// import { AgmCoreModule } from '@agm/core';
import { DisplayMoneyComponent } from './shared/components/display-money/display-money.component';
import { PaymentToBuyContractComponent } from './pages/main/haveRavKav/dialog-rav-kac/components/payment-to-buy-contract/payment-to-buy-contract.component';
import { ButtonComponent } from './shared/components/button/button.component';
import { DialogRavKacComponent } from './pages/main/haveRavKav/dialog-rav-kac/dialog-rav-kac.component';
import { DetailsRavKavTitleComponent } from './shared/components/details-rav-kav-title/details-rav-kav-title.component';
import { CancelationContractComponent } from './pages/main/haveRavKav/dialog-rav-kac/components/cancelation-contract/cancelation-contract.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { AutocompleteComponent } from './shared/components/autocomplete/autocomplete.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ErrorModalComponent } from './shared/components/modal/error-modal/error-modal.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {DateAdapter, MatNativeDateModule, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { EditProfileComponent } from './pages/main/haveRavKav/dialog-rav-kac/components/edit-profile/edit-profile.component';
import { LoadingModalComponent } from './shared/components/modal/loading-modal/loading-modal.component';
import { LoadingSpinnerComponent } from './shared/components/loading-spinner/loading-spinner.component';
import { InterceptorService } from './shared/services/interceptor.service';
import { LoadingModalService } from './shared/components/modal/loading-modal/loading-modal.service';
import { PayReportComponent } from './pages/main/pay-report/pay-report.component';
// import { AgmCoreModule } from '@agm/core';
import {MatStepperModule} from '@angular/material/stepper';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { ReportPaymentTitleComponent } from './pages/main/pay-report/components/report-payment-title/report-payment-title.component';
import { SelectMonthFreeMonthlyComponent } from './shared/components/dialogs/select-month-free-monthly/select-month-free-monthly.component';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { PdfViewerComponent } from './shared/components/dialogs/pdf-viewer/pdf-viewer.component';
import { CreditCardFormComponent } from './shared/components/credit-card-form/credit-card-form.component';
import { InteractivePaycardModule } from 'ngx-interactive-paycard';
import { PayReportSuccessComponent } from './pages/main/pay-report/components/pay-report-success/pay-report-success.component';
import { PayReportFailedComponent } from './pages/main/pay-report/components/pay-report-failed/pay-report-failed.component';
import { PayReportCanceledComponent } from './pages/main/pay-report/components/pay-report-canceled/pay-report-canceled.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { StoreService } from './shared/services/store.service';
import { DatePipe, registerLocaleData } from '@angular/common';
import localeHe from '@angular/common/locales/he';
import { GeneralDepositFromCashboxComponent } from './pages/main/general-deposit-from-cashbox/general-deposit-from-cashbox.component';
import { MissingTranslationHandler, MissingTranslationHandlerParams, TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MatMenuModule} from '@angular/material/menu';
import { LanguageMenuComponent } from './shared/components/language-menu/language-menu.component';
import { UpperStrapComponent } from './shared/components/upper-strap/upper-strap.component';
import { LocaleProvider } from './shared/services/locale/locale.provider';
import { ErrorMessageComponent } from './shared/components/error-message/error-message.component';
import { ErrorModalService } from './shared/components/modal/error-modal/error-modal-service.service';
import { SelectExpirationFreeRideComponent } from './shared/components/dialogs/select-experation-free-ride/select-expiration-free-ride.component';

registerLocaleData(localeHe);

export class MyMissingTranslationHandler implements MissingTranslationHandler {
  handle(params: MissingTranslationHandlerParams) {
    if (!params.translateService.translations[params.translateService.store.currentLang || params.translateService.store.defaultLang]) {
      return null;
    }
    // return params.translateService.translations[params.translateService.store.currentLang || params.translateService.store.defaultLang].MISSING_TRANSLATION;
    return null;
  }
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    HomeComponent,
    MenuHeaderComponent,
    StocktakingComponent,
    DepositToChashboxComponent,
    DepositToBankComponent,
    ChangingComponent,
    MoneyHeaderComponent,
    MoneyHeaderComponentComponent,
    KeysPipe,
    CashboxKeysPipe,
    ToPromisePipe,
    SearchInTablePipe,
    AmountInputComponent,
    ModalComponent,
    SelectCashboxModalComponent,
    LoginPopUpComponent,
    PopUpComponent,
    InfoModalComponent,
    SpinnerComponent,
    DisplayDetailsOfRavKavComponent,
    DisplayContractComponent,
    DisplayMoneyComponent,
    PaymentToBuyContractComponent,
    ButtonComponent,
    DialogRavKacComponent,
    DetailsRavKavTitleComponent,
    CancelationContractComponent,
    AutocompleteComponent,
    ErrorModalComponent,
    EditProfileComponent,
    LoadingModalComponent,
    LoadingSpinnerComponent,
    PayReportComponent,
    ReportPaymentTitleComponent,
    SelectMonthFreeMonthlyComponent,
    SelectExpirationFreeRideComponent,
    PdfViewerComponent,
    CreditCardFormComponent,
    PayReportSuccessComponent,
    PayReportFailedComponent,
    PayReportCanceledComponent,
    GeneralDepositFromCashboxComponent,
    LanguageMenuComponent,
    UpperStrapComponent,
    ErrorMessageComponent
  ],
  imports: [
    // AgmCoreModule.forRoot({
    //   apiKey: 'AIzaSyCwTDITyB0LBCC3jVICSoE0vM86ijY9uoQ'
    // }),
    HttpClientModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MatDialogModule,
    MatTabsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    ScrollingModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatTooltipModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    // MatSliderModule,
    MatStepperModule,
    MatCheckboxModule,
    PdfViewerModule,
    InteractivePaycardModule,
    MatSnackBarModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => new TranslateHttpLoader(http, './assets/i18n/', '.json'),
        deps: [HttpClient]
      },
      missingTranslationHandler: {
        provide: MissingTranslationHandler,
        useClass: MyMissingTranslationHandler
      },
    }),
    MatMenuModule
  ],
  providers: [
    MatDatepickerModule,
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [
        MAT_DATE_LOCALE,
        MAT_MOMENT_DATE_ADAPTER_OPTIONS
      ],
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: MAT_MOMENT_DATE_FORMATS
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true,
      deps: [
        LoadingModalService,
        ErrorModalService
      ]
    },
    LocaleProvider,
    {
      provide: DEFAULT_CURRENCY_CODE,
      useFactory: (storeService: StoreService) => {
        return storeService.localeCurrencyCode;
      },
      deps: [StoreService]
    },
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
