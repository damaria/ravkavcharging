const fs = require('fs')
const path = require('path')
const {EOL} = require('os')

const fileResultName = "all-languages.ts";
const languageFilesPath = path.join(__dirname, "src", "assets", "i18n")
const fileResultPath = path.join(languageFilesPath, fileResultName)

const finalFileResult = fs.readdirSync(languageFilesPath)
  .filter(fullFileName => fullFileName.endsWith(".json"))
  .map(jsonFileName => `export { default as ${jsonFileName.split('.')[0]} } from './${jsonFileName}';`)
  .join(EOL)
fs.writeFileSync(fileResultPath, finalFileResult)
